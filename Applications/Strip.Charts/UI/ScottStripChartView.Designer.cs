using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Strip.Charts
{
    public partial class ScottStripChartView
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ScottStripChartView));
            _ToolStrip = new ToolStrip();
            _SignalComboBoxLabel = new ToolStripLabel();
            __ChartComboBox = new ToolStripComboBox();
            __ChartComboBox.SelectedIndexChanged += new EventHandler(ChartComboBox_SelectedIndexChanged);
            __StartStopButton = new ToolStripButton();
            __StartStopButton.Click += new EventHandler(StartStopButton_Click);
            _PointCountNumericLabel = new ToolStripLabel();
            __PointCountNumeric = new Core.Controls.ToolStripNumericUpDown();
            __PointCountNumeric.ValueChanged += new EventHandler<EventArgs>(PointCountNumeric_ValueChanged);
            _RefreshRateNumberLabel = new ToolStripLabel();
            __RefreshRateNumeric = new Core.Controls.ToolStripNumericUpDown();
            __RefreshRateNumeric.ValueChanged += new EventHandler<EventArgs>(RefreshRateNumeric_ValueChanged);
            _SamplingRateNumericLabel = new ToolStripLabel();
            __SamplingRateNumeric = new Core.Controls.ToolStripNumericUpDown();
            __SamplingRateNumeric.ValueChanged += new EventHandler<EventArgs>(SamplingRateNumeric_ValueChanged);
            _FrequencyNumericLabel = new ToolStripLabel();
            __FrequencyNumeric = new Core.Controls.ToolStripNumericUpDown();
            __FrequencyNumeric.ValueChanged += new EventHandler<EventArgs>(FrequencyNumeric_ValueChanged);
            _ScottPlot = new ScottPlot.FormsPlot();
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _SignalComboBoxLabel, __ChartComboBox, __StartStopButton, _PointCountNumericLabel, __PointCountNumeric, _RefreshRateNumberLabel, __RefreshRateNumeric, _SamplingRateNumericLabel, __SamplingRateNumeric, _FrequencyNumericLabel, __FrequencyNumeric});
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new Size(846, 26);
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "Tool Strip";
            // 
            // _SignalComboBoxLabel
            // 
            _SignalComboBoxLabel.Name = "_SignalComboBoxLabel";
            _SignalComboBoxLabel.Size = new Size(41, 23);
            _SignalComboBoxLabel.Text = "Select:";
            // 
            // _ChartComboBox
            // 
            __ChartComboBox.Items.AddRange(new object[] { "Select chart:", "Sine wave phase", "Singe wave chart" });
            __ChartComboBox.Name = "__ChartComboBox";
            __ChartComboBox.Size = new Size(121, 26);
            __ChartComboBox.ToolTipText = "Selects chart type";
            // 
            // _StartStopButton
            // 
            __StartStopButton.Alignment = ToolStripItemAlignment.Right;
            __StartStopButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __StartStopButton.Image = (Image)resources.GetObject("_StartStopButton.Image");
            __StartStopButton.ImageTransparentColor = Color.Magenta;
            __StartStopButton.Name = "__StartStopButton";
            __StartStopButton.Size = new Size(35, 23);
            __StartStopButton.Text = "Start";
            __StartStopButton.ToolTipText = "Start or stop";
            // 
            // _PointCountNumericLabel
            // 
            _PointCountNumericLabel.Margin = new Padding(4, 1, 0, 2);
            _PointCountNumericLabel.Name = "_PointCountNumericLabel";
            _PointCountNumericLabel.Size = new Size(43, 23);
            _PointCountNumericLabel.Text = "Points:";
            // 
            // _PointCountNumeric
            // 
            __PointCountNumeric.AutoSize = false;
            __PointCountNumeric.Name = "__PointCountNumeric";
            __PointCountNumeric.Size = new Size(54, 23);
            __PointCountNumeric.Text = "100";
            __PointCountNumeric.Value = new decimal(new int[] { 100, 0, 0, 0 });
            // 
            // _RefreshRateNumberLabel
            // 
            _RefreshRateNumberLabel.Margin = new Padding(4, 1, 0, 2);
            _RefreshRateNumberLabel.Name = "_RefreshRateNumberLabel";
            _RefreshRateNumberLabel.Size = new Size(75, 23);
            _RefreshRateNumberLabel.Text = "Refresh Rate:";
            // 
            // _RefreshRateNumeric
            // 
            __RefreshRateNumeric.AutoSize = false;
            __RefreshRateNumeric.Name = "__RefreshRateNumeric";
            __RefreshRateNumeric.Size = new Size(64, 23);
            __RefreshRateNumeric.Text = "100";
            __RefreshRateNumeric.Value = new decimal(new int[] { 100, 0, 0, 0 });
            // 
            // _SamplingRateNumericLabel
            // 
            _SamplingRateNumericLabel.Margin = new Padding(4, 1, 0, 2);
            _SamplingRateNumericLabel.Name = "_SamplingRateNumericLabel";
            _SamplingRateNumericLabel.Size = new Size(86, 23);
            _SamplingRateNumericLabel.Text = "Sampling Rate:";
            // 
            // _SamplingRateNumeric
            // 
            __SamplingRateNumeric.AutoSize = false;
            __SamplingRateNumeric.Name = "__SamplingRateNumeric";
            __SamplingRateNumeric.Size = new Size(64, 23);
            __SamplingRateNumeric.Text = "100";
            __SamplingRateNumeric.Value = new decimal(new int[] { 100, 0, 0, 0 });
            // 
            // _FrequencyNumericLabel
            // 
            _FrequencyNumericLabel.Margin = new Padding(5, 1, 0, 2);
            _FrequencyNumericLabel.Name = "_FrequencyNumericLabel";
            _FrequencyNumericLabel.Size = new Size(54, 23);
            _FrequencyNumericLabel.Text = "Cycles/s:";
            // 
            // _FrequencyNumeric
            // 
            __FrequencyNumeric.Name = "__FrequencyNumeric";
            __FrequencyNumeric.Size = new Size(41, 23);
            __FrequencyNumeric.Text = "3";
            __FrequencyNumeric.ToolTipText = "Sine wave frequency";
            __FrequencyNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _ScottPlot
            // 
            _ScottPlot.Dock = DockStyle.Fill;
            _ScottPlot.Location = new Point(0, 26);
            _ScottPlot.Margin = new Padding(3, 4, 3, 4);
            _ScottPlot.Name = "_ScottPlot";
            _ScottPlot.Size = new Size(846, 359);
            _ScottPlot.TabIndex = 1;
            // 
            // ScottStripChartView
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ScottPlot);
            Controls.Add(_ToolStrip);
            Name = "ScottStripChartView";
            Size = new Size(846, 385);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        internal ToolStrip _ToolStrip;
        private ScottPlot.FormsPlot _ScottPlot;
        private ToolStripLabel _SignalComboBoxLabel;
        private ToolStripComboBox __ChartComboBox;

        private ToolStripComboBox _ChartComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ChartComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ChartComboBox != null)
                {
                    __ChartComboBox.SelectedIndexChanged -= ChartComboBox_SelectedIndexChanged;
                }

                __ChartComboBox = value;
                if (__ChartComboBox != null)
                {
                    __ChartComboBox.SelectedIndexChanged += ChartComboBox_SelectedIndexChanged;
                }
            }
        }

        private ToolStripButton __StartStopButton;

        private ToolStripButton _StartStopButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartStopButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartStopButton != null)
                {
                    __StartStopButton.Click -= StartStopButton_Click;
                }

                __StartStopButton = value;
                if (__StartStopButton != null)
                {
                    __StartStopButton.Click += StartStopButton_Click;
                }
            }
        }

        private ToolStripLabel _PointCountNumericLabel;
        private Core.Controls.ToolStripNumericUpDown __PointCountNumeric;

        private Core.Controls.ToolStripNumericUpDown _PointCountNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PointCountNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PointCountNumeric != null)
                {
                    __PointCountNumeric.ValueChanged -= PointCountNumeric_ValueChanged;
                }

                __PointCountNumeric = value;
                if (__PointCountNumeric != null)
                {
                    __PointCountNumeric.ValueChanged += PointCountNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown __RefreshRateNumeric;

        private Core.Controls.ToolStripNumericUpDown _RefreshRateNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshRateNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshRateNumeric != null)
                {
                    __RefreshRateNumeric.ValueChanged -= RefreshRateNumeric_ValueChanged;
                }

                __RefreshRateNumeric = value;
                if (__RefreshRateNumeric != null)
                {
                    __RefreshRateNumeric.ValueChanged += RefreshRateNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown __SamplingRateNumeric;

        private Core.Controls.ToolStripNumericUpDown _SamplingRateNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SamplingRateNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SamplingRateNumeric != null)
                {
                    __SamplingRateNumeric.ValueChanged -= SamplingRateNumeric_ValueChanged;
                }

                __SamplingRateNumeric = value;
                if (__SamplingRateNumeric != null)
                {
                    __SamplingRateNumeric.ValueChanged += SamplingRateNumeric_ValueChanged;
                }
            }
        }

        private ToolStripLabel _RefreshRateNumberLabel;
        private ToolStripLabel _SamplingRateNumericLabel;
        private ToolStripLabel _FrequencyNumericLabel;
        private Core.Controls.ToolStripNumericUpDown __FrequencyNumeric;

        private Core.Controls.ToolStripNumericUpDown _FrequencyNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FrequencyNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FrequencyNumeric != null)
                {
                    __FrequencyNumeric.ValueChanged -= FrequencyNumeric_ValueChanged;
                }

                __FrequencyNumeric = value;
                if (__FrequencyNumeric != null)
                {
                    __FrequencyNumeric.ValueChanged += FrequencyNumeric_ValueChanged;
                }
            }
        }

    }
}
