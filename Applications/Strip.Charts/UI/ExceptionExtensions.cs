using System;

namespace isr.Strip.Charts.ExceptionExtensions
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <license>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static class ProjectMethods
    {

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static bool AddExceptionData( Exception exception )
        {

            // this project adds no project-specific extensions.
            return false;
        }
    }

    /// <summary> Builds the full blown exception report. </summary>
    /// <license>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static class Methods
    {

        /// <summary> Adds an exception data. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception exception )
        {
            return ProjectMethods.AddExceptionData( exception ); 
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        public static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToFullBlownString( this Exception value, int level )
        {
            return Core.Services.ExceptionExtensions.Methods.ToFullBlownString( value, level, AddExceptionData );
        }
    }
}
