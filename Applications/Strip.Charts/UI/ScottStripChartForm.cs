using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using isr.Strip.Charts.ExceptionExtensions;

namespace StripCharts
{
    /// <summary>   Form for viewing the Scott strip chart. </summary>
    /// <remarks>   David, 2020-09-12. </remarks>
    internal partial class ScottStripChartForm : Form
    {

        #region " CONSTRUCTION AND CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-12. </remarks>
        public ScottStripChartForm()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (this.components != null) )
            {
                this.components.Dispose();
            }
            base.Dispose( disposing );
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="e">    A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains
        ///                     the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = "loading";
            try
            {
                this.Cursor = Cursors.WaitCursor;
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.ToFullBlownString(), $"Exception {activity}", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = "showing";
            try
            {
              
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.ToFullBlownString(), $"Exception {activity}", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = "showing";
            try
            {
                this.Text = "Scott Plot Strip Chart Demo";
                this.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.ToFullBlownString(), $"Exception {activity}", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

    }
}
