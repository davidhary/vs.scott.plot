namespace StripCharts
{
    partial class ScottStripChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._ScottStripChartView = new isr.Strip.Charts.ScottStripChartView();
            this.SuspendLayout();
            // 
            // _ScottStripChartView
            // 
            this._ScottStripChartView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ScottStripChartView.Frequency = 3D;
            this._ScottStripChartView.Location = new System.Drawing.Point(0, 0);
            this._ScottStripChartView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._ScottStripChartView.Name = "_ScottStripChartView";
            this._ScottStripChartView.PointCount = 101;
            this._ScottStripChartView.RefreshRate = 60;
            this._ScottStripChartView.SamplingRate = 100;
            this._ScottStripChartView.Size = new System.Drawing.Size(1057, 475);
            this._ScottStripChartView.TabIndex = 0;
            this._ScottStripChartView.TimerEnabled = false;
            // 
            // ScottStripChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 475);
            this.Controls.Add(this._ScottStripChartView);
            this.Name = "ScottStripChartForm";
            this.Text = "ScottStripChartForm";
            this.ResumeLayout(false);

        }

        #endregion

        private isr.Strip.Charts.ScottStripChartView _ScottStripChartView;
    }
}

