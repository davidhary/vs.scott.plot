## ISR Strip Charts<sub>&trade;</sub>
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History

*4.0.7545 08/28/20*  
Created using Scott Plot 4.0.

**Contact Information**

support[at]IntegratedScientificResources[.]com

**Copyrights and Trademarks**

The ISR logo is a trademark of Integrated Scientific Resources. Windows
and .NET Framework are trademarks of Microsoft Corporation. Other
trademarks are property of their owners.

\(C\) 2020 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/arebis.typedunits)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Scott Plot Libraries](https://bitbucket.org/davidhary/vs.scott.plot)  
[Scott Plot](https://github.com/swharden/ScottPlot.git)  
[Visualization Charting Libraries](https://bitbucket.org/davidhary/vs.Visuals.Charting)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)
