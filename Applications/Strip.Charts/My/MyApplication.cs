
#pragma warning disable IDE1006 // Naming Styles
namespace isr.Strip.Charts.My
#pragma warning restore IDE1006 // Naming Styles
{
    internal class MyApplication
    {

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 101;
        public const string AssemblyTitle = "Strip Charts Demo";
        public const string AssemblyDescription = "Strip Charts Demo";
        public const string AssemblyProduct = "Strip.Charts";

        public MyApplication()
        {
        }

        public static MyApplication Default { get; } = new MyApplication();
    }

}
