using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Strip.Charts.ExceptionExtensions;

namespace isr.Strip.Charts
{
    /// <summary> A sealed class the parses the command line and provides the command line values. </summary>
    /// <license> (c) 2011 Integrated Scientific Resources, Inc.<para>
    /// Licensed under The MIT License. </para><para>
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </para> </license>
    /// <history date="02/02/2011" by="David" revision="x.x.4050.x"> Created. </history>
    public sealed class CommandLineInfo
    {

        /// <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
        private CommandLineInfo() : base()
        {
        }

        /// <summary> Gets the no-operation command line option. </summary>
        /// <value> The no operation option. </value>
        public static string NoOperationOption => "/nop";

        /// <summary> Gets the Device-Enabled option. </summary>
        /// <value> The Device enabled option. </value>
        public static string DevicesEnabledOption => "/d:";

        /// <summary> Gets the display name option. </summary>
        /// <value> The display name option. </value>
        public static string DisplayNameOption => "/ui:";

        /// <summary> Validate the command line. </summary>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        /// not handled. </exception>
        /// <param name="commandLineArguments"> The command line arguments. </param>
        public static void ValidateCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArguments )
        {
            if ( commandLineArguments is object )
            {
                foreach ( string argument in commandLineArguments )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( DisplayNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else
                    {
                        Nop = argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase )
                            ? true
                            : throw new ArgumentException( $"Unknown command line argument '{argument}' was detected. Should be Ignored.", nameof( commandLineArguments ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( string commandLineArgs )
        {
            if ( !string.IsNullOrWhiteSpace( commandLineArgs ) )
            {
                IList<string> args = new List<string>( commandLineArgs.Split( ' ' ) );
                ParseCommandLine( args.ToArray() );
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( String[] commandLineArgs )
        {
            var commandLineBuilder = new System.Text.StringBuilder();
            SelectedDisplayOptionName = DisplayOption.ScottChart.ToString();
            if ( commandLineArgs is object )
            {
                foreach ( string argument in commandLineArgs )
                {
                    _ = commandLineBuilder.AppendFormat( "{0} ", argument );
                    if ( argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        Nop = true;
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        DevicesEnabled = argument.Substring( DisplayNameOption.Length ).StartsWith( "n", StringComparison.OrdinalIgnoreCase );
                    }
                    else if ( argument.StartsWith( DisplayNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        SelectedDisplayOptionName = argument.Substring( DisplayNameOption.Length );
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }

            CommandLine = commandLineBuilder.ToString();
        }

        /// <summary>   Parses the command line. </summary>
        /// <remarks>   David, 2020-09-12. </remarks>
        /// <param name="commandLineArgs">  The command line arguments. </param>
        /// <returns>   <c>True</c> if success or false if Exception occurred. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryParseCommandLine( String[] commandLineArgs) 
        {
            (bool Success, string Details) result = ( true, string.Empty);
            string activity = string.Empty;
            if ( commandLineArgs is null || !commandLineArgs.Any() )
            {
                CommandLine = string.Empty;
            }
            else
            {
                try
                {
                    CommandLine = string.Join( ",", commandLineArgs );
                    activity = $"Parsing the commandLine {CommandLine}";
                    // Data.ParseCommandLine(commandLineArgs)
                    ParseCommandLine( commandLineArgs );
                }
                catch ( ArgumentException ex )
                {
                   result = ( false, $"Unknown argument value for '{ex.ParamName}' ignored" );
                }
                catch ( Exception ex )
                {
                    result = (false, $"Failed {activity};. {ex.ToFullBlownString()}" );
                }
            }

            return result;
        }

        /// <summary> Gets the command line. </summary>
        /// <value> The command line. </value>
        public static string CommandLine { get; private set; }

        /// <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
        /// <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
        public static bool? DevicesEnabled { get; set; }

        /// <summary> Gets or sets the no operation option. </summary>
        /// <value> The no-op. </value>
        public static bool Nop { get; set; }

        private static string _SelectedDisplayOptionName = string.Empty;

        /// <summary> Gets or sets the selected display option name. </summary>
        /// <value> The name of the selected display option. </value>
        public static string SelectedDisplayOptionName
        {
            get => _SelectedDisplayOptionName;

            set {
                _SelectedDisplayOptionName = value;
                if ( !Enum.TryParse( value, true, out DisplayOption selectedOption ) )
                {
                    selectedOption = DisplayOption.Unknown;
                }

                SelectedDisplayOption = selectedOption;
            }
        }

        /// <summary>   Gets or sets the selected display option. </summary>
        /// <value> The selected display option. </value>
        public static DisplayOption SelectedDisplayOption { get; set; }

    }

    /// <summary> Values that represent display options. </summary>
    public enum DisplayOption
    {
        /// <summary>   An enum constant representing the unknown option. </summary>
        [System.ComponentModel.Description( "Unknown" )]
        Unknown,
        /// <summary>   An enum constant representing the Microsoft chart option. </summary>
        [System.ComponentModel.Description( "Microsoft Chart" )]
        MicrosoftChart,
        /// <summary>   An enum constant representing the scott chart option. </summary>
        [System.ComponentModel.Description( "Scott Chart" )]
        ScottChart,
        /// <summary>   An enum constant representing the live chart option. </summary>
        [System.ComponentModel.Description( "Live Chart" )]
        LiveChart
    }
}
