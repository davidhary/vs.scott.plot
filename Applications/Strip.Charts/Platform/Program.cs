using System;
using System.IO;
using System.Windows.Forms;

using isr.Strip.Charts.ExceptionExtensions;

using StripCharts;

namespace isr.Strip.Charts
{
    /// <summary>
    /// Entry point to the Coyote tool.
    /// </summary>
    internal class Program
    {
        private static readonly object ConsoleLock = new object();

        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2020-09-12. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        private static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += OnProcessExit;
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

            // Parses the command line options to get the configuration.
            CommandLineInfo.ParseCommandLine( args );

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.Run( new ScottStripChartForm() );
        }

        #region " EVENT CALLBACKS "

        /// <summary>
        /// Callback invoked when the current process terminates.
        /// </summary>
        private static void OnProcessExit(object sender, EventArgs e) => Program.Shutdown();

        /// <summary>
        /// Callback invoked when an unhandled exception occurs.
        /// </summary>
        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            Program.ReportUnhandledException((Exception)args.ExceptionObject);
            Environment.Exit(1);
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary>   Reports unhandled exception. </summary>
        /// <remarks>   David, 2020-09-12. </remarks>
        /// <param name="ex">   The exception. </param>
        private static void ReportUnhandledException(Exception ex)
        {
           Program.ReportException(ex);
        }

        /// <summary>   Reports an exception. </summary>
        /// <remarks>   David, 2020-09-12. </remarks>
        /// <param name="ex">   The exception. </param>
        private static void ReportException(Exception ex)
        {
            lock (ConsoleLock)
            {
                _ = MessageBox.Show( ex.ToFullBlownString(), "Exception Occurred", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }

        /// <summary>
        /// Shutdowns any active monitors.
        /// </summary>
        private static void Shutdown()
        {
            Console.WriteLine( ". Shutting down the strip charting demo..." );
        }

        #endregion

    }
}
