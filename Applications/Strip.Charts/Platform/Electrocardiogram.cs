using System;

namespace isr.Strip.Charts
{
    /// <summary> An electrocardiogram. </summary>
    /// <remarks>
    /// The aim of the ECG simulator is to produce the typical ECG waveforms of different leads and
    /// as many arrhythmias as possible. My ECG simulator is able To produce normal lead II ECG
    /// waveform. The use Of a simulator has many advantages In the simulation Of ECG waveforms.
    /// First one Is saving Of time And another one Is removing the difficulties Of taking real ECG
    /// signals With invasive And noninvasive methods. The ECG simulator enables us To analyze And
    /// study normal And abnormal ECG waveforms without actually Using the ECG machine. One can
    /// simulate any given ECG waveform Using the ECG simulator. The way by which my simulator
    /// differs from other typical ECG simulators Is that i have used the principle Of Fourier
    /// series. The calculations used And other necessary descriptions are included In the file
    /// attached.
    /// https://www.mathworks.com/matlabcentral/fileexchange/10858-ecg-simulation-using-matlab.
    /// </remarks>
    /// <license>
    /// (c) 2019 karthik raviprakash. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    /// <history date="6/16/2019" by="David" revision=""> Created. </history>
    public class Electrocardiogram
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        public Electrocardiogram() : base()
        {
            this.HeartRate = 72d;
        }

        private double _HeartRate = 72d;
        /// <summary>   72 is heart rate. x spans 2 seconds so x / l @. </summary>
        /// <value> The heart rate. </value>
        public double HeartRate
        {
            get => this._HeartRate;

            set {
                this._HeartRate = value;
                this._Period = 60d / value;
            }
        }

        /// <summary>   The period. </summary>
        private double _Period;

        /// <summary>   Gets or sets the period. </summary>
        /// <value> The period. </value>
        public double Period
        {
            get => this._Period;

            set {
                this._Period = value;
                this._HeartRate = 60d / value;
            }
        }

        /// <summary>   The P wave amplitude. </summary>
        /// <value> The P wave amplitude. </value>
        public double PWaveAmplitude { get; set; } = 0.25d;

        /// <summary>   Gets or sets the duration of the wave. </summary>
        /// <value> The p wave duration. </value>
        public double PWaveDuration { get; set; } = 0.09d;
        /// <summary>   Gets or sets the wave P-R interval. </summary>
        /// <value> The P wave pr interval. </value>
        public double PWavePRInterval { get; set; } = 0.16d;

        /// <summary>   Gets or sets the wave amplitude. </summary>
        /// <value> The Q wave amplitude. </value>
        public double QWaveAmplitude { get; set; } = 0.025d;
        /// <summary>   Gets or sets the duration of the qwave. </summary>
        /// <value> The Q wave duration. </value>
        public double QwaveDuration { get; set; } = 0.066d;

        /// <summary>   Gets or sets the Q wave time. </summary>
        /// <value> The Q wave time. </value>
        public double QWaveTime { get; set; } = 0.166d;

        /// <summary>   Gets or sets the QRS wave amplitude. </summary>
        /// <value> The QRS wave amplitude. </value>
        public double QRSWaveAmplitude { get; set; } = 1.6d;

        /// <summary>   Gets or sets the duration of the QR wave. </summary>
        /// <value> The QR wave duration. </value>
        public double QRSwaveDuration { get; set; } = 0.11d;

        /// <summary>   Gets or sets the S wave amplitude. </summary>
        /// <value> The S wave amplitude. </value>
        public double SWaveAmplitude { get; set; } = 0.25d;

        /// <summary>   Gets or sets the duration of the S wave. </summary>
        /// <value> The S wave duration. </value>
        public double SWaveDuration { get; set; } = 0.066d;

        /// <summary>   Gets or sets the S wave time. </summary>
        /// <value> The S wave time. </value>
        public double SWaveTime { get; set; } = 0.09d;

        /// <summary>   Gets or sets the T wave amplitude. </summary>
        /// <value> The T wave amplitude. </value>
        public double TWaveAmplitude { get; set; } = 0.35d;

        /// <summary>   Gets or sets the duration of the T wave. </summary>
        /// <value> The T wave duration. </value>
        public double TWaveDuration { get; set; } = 0.142d;

        /// <summary>   Gets or sets the T wave ST interval. </summary>
        /// <value> The T wave ST interval. </value>
        public double TWaveSTInterval { get; set; } = 0.2d;
        /// <summary>   Gets or sets the wave amplitude. </summary>
        /// <value> The u wave amplitude. </value>
        public double UWaveAmplitude { get; set; } = 0.035d;

        /// <summary>   Gets or sets the duration of the U wave. </summary>
        /// <value> The U wave duration. </value>
        public double UWaveDuration { get; set; } = 0.0476d;

        /// <summary>   Gets or sets the U wave time. </summary>
        /// <value> The U wave time. </value>
        public double UWaveTime { get; set; } = 0.433d;

        /// <summary>   Generates a QRS Wave value. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="x">            The x coordinate. </param>
        /// <param name="amplitude">    The amplitude. </param>
        /// <param name="duration">     The duration. </param>
        /// <param name="period">       The average ECG Period. </param>
        /// <returns>   A Double. </returns>
        public static double QRSwave( double x, double amplitude, double duration, double period )
        {
            double l = 0.5d * period;
            double a = amplitude;
            double b = 2d * l / duration;
            int n = 100;
            double qrs1 = a / (2d * b) * (2d - b);
            double qrs2 = 0d;
            for ( int i = 1, loopTo = n; i <= loopTo; i++ )
            {
                double harm = 2d * b * a / (i * i * Math.PI * Math.PI) * (1d - Math.Cos( i * Math.PI / b )) * Math.Cos( i * Math.PI * x / l );
                qrs2 += harm;
            }

            return qrs1 + qrs2;
        }

        /// <summary>   Generates a P Wave value. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="x">            The x coordinate. </param>
        /// <param name="amplitude">    The amplitude. </param>
        /// <param name="duration">     The duration. </param>
        /// <param name="time">         The time. </param>
        /// <param name="period">       The average ECG Period. </param>
        /// <returns>   A Double. </returns>
        public static double PWave( double x, double amplitude, double duration, double time, double period )
        {
            double l = 0.5d * period;
            double a = amplitude;
            double b = 2d * l / duration;
            x += time;
            int n = 100;
            double p1 = 1d / l;
            double p2 = 0d;
            double harm1;
            for ( int i = 1, loopTo = n; i <= loopTo; i++ )
            {
                harm1 = (Math.Sin( Math.PI / (2d * b) * (b - 2 * i) ) / (b - 2 * i) + Math.Sin( Math.PI / (2d * b) * (b + 2 * i) ) / (b + 2 * i)) * (2d / Math.PI) * Math.Cos( i * Math.PI * x / l );
                p2 += harm1;
            }

            return a * (p1 + p2);
        }

        /// <summary>   Generates a Q Wave value. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="x">            The x coordinate. </param>
        /// <param name="amplitude">    The amplitude. </param>
        /// <param name="duration">     The duration. </param>
        /// <param name="time">         The time. </param>
        /// <param name="period">       The average ECG Period. </param>
        /// <returns>   A Double. </returns>
        public static double QWave( double x, double amplitude, double duration, double time, double period )
        {
            double l = 0.5d * period;
            double a = amplitude;
            double b = 2d * l / duration;
            x += time;
            int n = 100;
            double q1 = a / (2d * b) * (2d - b);
            double q2 = 0d;
            double harm5;
            for ( int i = 1, loopTo = n; i <= loopTo; i++ )
            {
                harm5 = 2d * b * a / (i * i * Math.PI * Math.PI) * (1d - Math.Cos( i * Math.PI / b )) * Math.Cos( i * Math.PI * x / l );
                q2 += harm5;
            }

            return -1 * (q1 + q2);
        }

        /// <summary>   Generates an S Wave value. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="x">            The x coordinate. </param>
        /// <param name="amplitude">    The amplitude. </param>
        /// <param name="duration">     The duration. </param>
        /// <param name="time">         The time. </param>
        /// <param name="period">       The average ECG Period. </param>
        /// <returns>   A Double. </returns>
        public static double SWave( double x, double amplitude, double duration, double time, double period )
        {
            double l = 0.5d * period;
            double a = amplitude;
            double b = 2d * l / duration;
            x -= time;
            // x = x + time
            int n = 100;
            double s1 = a / (2d * b) * (2d - b);
            double s2 = 0d;
            double harm3;
            for ( int i = 1, loopTo = n; i <= loopTo; i++ )
            {
                harm3 = 2d * b * a / (i * i * Math.PI * Math.PI) * (1d - Math.Cos( i * Math.PI / b )) * Math.Cos( i * Math.PI * x / l );
                s2 += harm3;
            }

            return -1 * (s1 + s2);
        }

        /// <summary>   Generates a T Wave value. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="x">            The x coordinate. </param>
        /// <param name="amplitude">    The amplitude. </param>
        /// <param name="duration">     The duration. </param>
        /// <param name="time">         The time. </param>
        /// <param name="period">       The average ECG Period. </param>
        /// <returns>   A Double. </returns>
        public static double TWave( double x, double amplitude, double duration, double time, double period )
        {
            double l = 0.5d * period;
            double a = amplitude;
            double b = 2d * l / duration;
            x = x - time - 0.045d;
            // x = x + time - 0.045
            int n = 100;
            double t1 = 1d / l;
            double t2 = 0d;
            double harm2;
            for ( int i = 1, loopTo = n; i <= loopTo; i++ )
            {
                harm2 = (Math.Sin( Math.PI / (2d * b) * (b - 2 * i) ) / (b - 2 * i) + Math.Sin( Math.PI / (2d * b) * (b + 2 * i) ) / (b + 2 * i)) * (2d / Math.PI) * Math.Cos( i * Math.PI * x / l );
                t2 += harm2;
            }

            return a * (t1 + t2);
        }

        /// <summary>   Generates a U Wave value. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="x">            The x coordinate. </param>
        /// <param name="amplitude">    The amplitude. </param>
        /// <param name="duration">     The duration. </param>
        /// <param name="time">         The time. </param>
        /// <param name="period">       The average ECG Period. </param>
        /// <returns>   A double. </returns>
        public static double UWave( double x, double amplitude, double duration, double time, double period )
        {
            double l = 0.5d * period;
            double a = amplitude;
            double b = 2d * l / duration;
            x -= time;
            // x = x + time
            int n = 100;
            double u1 = 1d / l;
            double u2 = 0d;
            double harm4;
            for ( int i = 1, loopTo = n; i <= loopTo; i++ )
            {
                harm4 = (Math.Sin( Math.PI / (2d * b) * (b - 2 * i) ) / (b - 2 * i) + Math.Sin( Math.PI / (2d * b) * (b + 2 * i) ) / (b + 2 * i)) * (2d / Math.PI) * Math.Cos( i * Math.PI * x / l );
                u2 += harm4;
            }

            return a * (u1 + u2);
        }

        /// <summary>   Simulate electrocardiogram. </summary>
        /// <remarks>   David, 9/11/2020. </remarks>
        /// <param name="totalSeconds"> The total in seconds. </param>
        /// <returns>   A Double. </returns>
        public double SimulateElectrocardiogram( double totalSeconds )
        {
            totalSeconds %= (2d * this.Period);
            double value = -0.9d;
            value += Electrocardiogram.PWave( totalSeconds, this.PWaveAmplitude, this.PWaveDuration, this.PWavePRInterval, this.Period );
            value += Electrocardiogram.QWave( totalSeconds, this.QWaveAmplitude, this.QwaveDuration, this.QWaveTime, this.Period );
            value += Electrocardiogram.QRSwave( totalSeconds, this.QRSWaveAmplitude, this.QRSwaveDuration, this.Period );
            value += Electrocardiogram.SWave( totalSeconds, this.SWaveAmplitude, this.SWaveDuration, this.SWaveTime, this.Period );
            value += Electrocardiogram.TWave( totalSeconds, this.TWaveAmplitude, this.TWaveDuration, this.TWaveSTInterval, this.Period );
            value += Electrocardiogram.UWave( totalSeconds, this.UWaveAmplitude, this.UWaveDuration, this.UWaveTime, this.Period );
            return value;
        }

    }
}
