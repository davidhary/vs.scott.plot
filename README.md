# Scott Plot Project

Python-like plot routines.

Forked from [ScottPlot](https://github.com/swharden/ScottPlot/)

### Projects

The following projects are required for the forked Scott Plot Solution:
* [Scott Plot](https://www.bitbucket.org/davidhary/vs.scott.plot) - Forked Scott Plot Project

```
git clone git@bitbucket.org:davidhary/vs.scott.plot.git
```

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **Scott Harden** - [Harden Technologies](https://tech.swharden.com)
* **David Hary** - *Strip Chart Demo* - [Integrated Scientific Resources](https://www.IntegratedScientificResources.com)

## License

This repository is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.scott.plot/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)

## Revision Changes

* [ScottPlot v3] 8/2019
* [ScottPlot v4] 4.0.39 8/2020
