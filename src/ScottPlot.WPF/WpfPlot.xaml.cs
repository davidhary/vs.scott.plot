﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;

namespace ScottPlot
{
    /// <summary>
    /// Interaction logic for ScottPlotWPF.xaml
    /// </summary>

    [System.ComponentModel.ToolboxItem(true)]
    [System.ComponentModel.DesignTimeVisible(true)]
    public partial class WpfPlot : UserControl
    {
        public Plot plt { get; private set; }
        private Settings settings;
        private bool isDesignerMode;
        public Cursor cursor = Cursors.Arrow;
        private double dpiScale = 1;

        public WpfPlot(Plot plt)
        {
            this.InitializeComponent();
            this.ContextMenu = this.DefaultRightClickMenu();
            this.Reset(plt);
        }

        public WpfPlot()
        {
            this.InitializeComponent();
            this.ContextMenu = this.DefaultRightClickMenu();
            this.Reset(null);
        }

        private ContextMenu DefaultRightClickMenu()
        {
            MenuItem SaveImageMenuItem = new MenuItem() { Header = "Save Image" };
            SaveImageMenuItem.Click += this.SaveImage;
            MenuItem CopyImageMenuItem = new MenuItem() { Header = "Copy Image" };
            CopyImageMenuItem.Click += this.CopyImage;
            MenuItem NewWindowMenuItem = new MenuItem() { Header = "Open in New Window" };
            NewWindowMenuItem.Click += this.OpenInNewWindow;
            MenuItem HelpMenuItem = new MenuItem() { Header = "Help" };
            HelpMenuItem.Click += this.OpenHelp;

            var cm = new ContextMenu();
            cm.Items.Add(SaveImageMenuItem);
            cm.Items.Add(CopyImageMenuItem);
            cm.Items.Add(NewWindowMenuItem);
            cm.Items.Add(HelpMenuItem);

            return cm;
        }

        public void Reset()
        {
            this.Reset(null);
        }

        public void Reset(Plot plt)
        {
            this.plt = (plt is null) ? new Plot() : plt;
            this.InitializeScottPlot();
            this.Render();
        }

        private void InitializeScottPlot()
        {
            this.lblVersion.Content = Tools.GetVersionString();
            this.isDesignerMode = DesignerProperties.GetIsInDesignMode(this);

            this.settings = this.plt.GetSettings(showWarning: false);

            if (this.isDesignerMode)
            {
                // hide the plot
                this.mainGrid.RowDefinitions[1].Height = new GridLength(0);
            }
            else
            {
                // hide the version info
                this.mainGrid.RowDefinitions[0].Height = new GridLength(0);
                this.CanvasPlot_SizeChanged(null, null);
                this.dpiScale = this.settings.gfxFigure.DpiX / 96;
                this.canvasDesigner.Background = Brushes.Transparent;
                this.canvasPlot.Background = Brushes.Transparent;
            }
        }

        private static BitmapImage BmpImageFromBmp(System.Drawing.Bitmap bmp)
        {
            using (var memory = new System.IO.MemoryStream())
            {
                bmp.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        private bool currentlyRendering = false;
        public void Render(bool skipIfCurrentlyRendering = false, bool lowQuality = false, bool recalculateLayout = false)
        {
            if (!this.isDesignerMode)
            {
                if (recalculateLayout)
                    this.plt.TightenLayout();

                if (this.equalAxes)
                    this.plt.AxisEqual();

                if (!(skipIfCurrentlyRendering && this.currentlyRendering))
                {
                    this.currentlyRendering = true;
                    this.imagePlot.Source = BmpImageFromBmp(this.plt.GetBitmap(true, lowQuality || this.lowQualityAlways));
                    this.currentlyRendering = false;
                    Rendered?.Invoke(null, null);
                }
            }
        }

        private void CanvasPlot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.plt?.Resize((int)(this.canvasPlot.ActualWidth * this.dpiScale), (int)(this.canvasPlot.ActualHeight * this.dpiScale));
            this.Render();
        }

        #region user control configuration

        private bool plotContainsHeatmap => this.settings?.plottables.Where(p => p is PlottableHeatmap).Count() > 0;

        private bool enablePanning = true;
        private bool enableZooming = true;
        private bool enableScrollWheelZoom = true;
        private bool lowQualityWhileDragging = true;
        private bool lowQualityAlways = false;
        private bool doubleClickingTogglesBenchmark = true;
        private bool lockVerticalAxis = false;
        private bool lockHorizontalAxis = false;
        private bool equalAxes = false;
        private double middleClickMarginX = .1;
        private double middleClickMarginY = .1;
        private bool? recalculateLayoutOnMouseUp = null;
        public void Configure(
            bool? enablePanning = null,
            bool? enableRightClickZoom = null,
            bool? enableRightClickMenu = null,
            bool? enableScrollWheelZoom = null,
            bool? lowQualityWhileDragging = null,
            bool? lowQualityAlways = null,
            bool? enableDoubleClickBenchmark = null,
            bool? lockVerticalAxis = null,
            bool? lockHorizontalAxis = null,
            bool? equalAxes = null,
            double? middleClickMarginX = null,
            double? middleClickMarginY = null,
            bool? recalculateLayoutOnMouseUp = null
            )
        {
            if (enablePanning != null) this.enablePanning = (bool)enablePanning;
            if (enableRightClickZoom != null) this.enableZooming = (bool)enableRightClickZoom;
            if (enableRightClickMenu != null) this.ContextMenu = (enableRightClickMenu.Value) ? this.DefaultRightClickMenu() : null;
            if (enableScrollWheelZoom != null) this.enableScrollWheelZoom = (bool)enableScrollWheelZoom;
            if (lowQualityWhileDragging != null) this.lowQualityWhileDragging = (bool)lowQualityWhileDragging;
            if (lowQualityAlways != null) this.lowQualityAlways = (bool)lowQualityAlways;
            if (enableDoubleClickBenchmark != null) this.doubleClickingTogglesBenchmark = (bool)enableDoubleClickBenchmark;
            if (lockVerticalAxis != null) this.lockVerticalAxis = (bool)lockVerticalAxis;
            if (lockHorizontalAxis != null) this.lockHorizontalAxis = (bool)lockHorizontalAxis;
            if (equalAxes != null) this.equalAxes = (bool)equalAxes;
            this.middleClickMarginX = middleClickMarginX ?? this.middleClickMarginX;
            this.middleClickMarginY = middleClickMarginY ?? this.middleClickMarginY;
            this.recalculateLayoutOnMouseUp = recalculateLayoutOnMouseUp;
        }

        private bool isAltPressed { get { return Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt); } }
        private bool isShiftPressed { get { return (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift) || (this.lockHorizontalAxis)); } }
        private bool isCtrlPressed { get { return (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || (this.lockVerticalAxis)); } }

        #endregion

        #region mouse tracking

        private Point? mouseLeftDownLocation, mouseRightDownLocation, mouseMiddleDownLocation;

        double[] axisLimitsOnMouseDown;
        private bool isPanningOrZooming
        {
            get
            {
                if (this.axisLimitsOnMouseDown is null) return false;
                if (this.mouseLeftDownLocation != null) return true;
                else if (this.mouseRightDownLocation != null) return true;
                else if (this.mouseMiddleDownLocation != null) return true;
                return false;
            }
        }

        IDraggable plottableBeingDragged = null;
        private bool isMovingDraggable { get { return (this.plottableBeingDragged != null); } }

        private Cursor GetCursor(Config.Cursor scottPlotCursor)
        {
            switch (scottPlotCursor)
            {
                case Config.Cursor.Arrow: return Cursors.Arrow;
                case Config.Cursor.WE: return Cursors.SizeWE;
                case Config.Cursor.NS: return Cursors.SizeNS;
                case Config.Cursor.All: return Cursors.SizeAll;
                default: return Cursors.Help;
            }
        }

        private Point GetPixelPosition(MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(this);
            pos.X *= this.dpiScale;
            pos.Y *= this.dpiScale;
            return pos;
        }

        private Point GetPixelPosition(MouseEventArgs e, bool applyDpiScaling = true)
        {
            Point pos = e.GetPosition(this);
            if (applyDpiScaling)
            {
                pos.X *= this.dpiScale;
                pos.Y *= this.dpiScale;
            }
            return pos;
        }

        private System.Drawing.Point SDPoint(Point pt)
        {
            return new System.Drawing.Point((int)pt.X, (int)pt.Y);
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.CaptureMouse();

            var mousePixel = this.GetPixelPosition(e);
            this.plottableBeingDragged = this.plt.GetDraggableUnderMouse(mousePixel.X, mousePixel.Y);

            if (this.plottableBeingDragged is null)
            {
                // MouseDown event is to start a pan or zoom
                if (e.ChangedButton == MouseButton.Left && this.isAltPressed) this.mouseMiddleDownLocation = this.GetPixelPosition(e);
                else if (e.ChangedButton == MouseButton.Left && this.enablePanning) this.mouseLeftDownLocation = this.GetPixelPosition(e);
                else if (e.ChangedButton == MouseButton.Right && this.enableZooming) this.mouseRightDownLocation = this.GetPixelPosition(e);
                else if (e.ChangedButton == MouseButton.Middle && this.enableScrollWheelZoom) this.mouseMiddleDownLocation = this.GetPixelPosition(e);
                this.axisLimitsOnMouseDown = this.plt.Axis();
            }
            else
            {
                // mouse is being used to drag a plottable
            }
        }

        [Obsolete("use Plot.CoordinateFromPixelX() and Plot.CoordinateFromPixelY()")]
        public Point mouseCoordinates
        {
            get
            {
                var coord = this.plt.CoordinateFromPixel(this.mouseLocation.X, this.mouseLocation.Y);
                return new Point(coord.X, coord.Y);
            }
        }
        Point mouseLocation;
        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            this.mouseLocation = this.GetPixelPosition(e);
            if (this.isPanningOrZooming)
                this.MouseMovedToPanOrZoom(e);
            else if (this.isMovingDraggable)
                this.MouseMovedToMoveDraggable(e);
            else
                this.MouseMovedWithoutInteraction(e);
        }

        private void MouseMovedToPanOrZoom(MouseEventArgs e)
        {
            this.plt.Axis(this.axisLimitsOnMouseDown);
            var mouseLocation = this.GetPixelPosition(e);

            if (this.mouseLeftDownLocation != null)
            {
                // left-click-drag panning
                double deltaX = ((Point)this.mouseLeftDownLocation).X - mouseLocation.X;
                double deltaY = mouseLocation.Y - ((Point)this.mouseLeftDownLocation).Y;

                if (this.isCtrlPressed) deltaY = 0;
                if (this.isShiftPressed) deltaX = 0;

                this.settings.AxesPanPx((int)deltaX, (int)deltaY);
                AxisChanged?.Invoke(null, null);
            }
            else if (this.mouseRightDownLocation != null)
            {
                // right-click-drag zooming
                double deltaX = ((Point)this.mouseRightDownLocation).X - mouseLocation.X;
                double deltaY = mouseLocation.Y - ((Point)this.mouseRightDownLocation).Y;

                if (this.isCtrlPressed == true && this.isShiftPressed == false) deltaY = 0;
                if (this.isShiftPressed == true && this.isCtrlPressed == false) deltaX = 0;

                this.settings.AxesZoomPx(-(int)deltaX, -(int)deltaY, lockRatio: this.isCtrlPressed && this.isShiftPressed);
                AxisChanged?.Invoke(null, null);
            }
            else if (this.mouseMiddleDownLocation != null)
            {
                // middle-click-drag zooming to rectangle
                double x1 = Math.Min(mouseLocation.X, ((Point)this.mouseMiddleDownLocation).X);
                double x2 = Math.Max(mouseLocation.X, ((Point)this.mouseMiddleDownLocation).X);
                double y1 = Math.Min(mouseLocation.Y, ((Point)this.mouseMiddleDownLocation).Y);
                double y2 = Math.Max(mouseLocation.Y, ((Point)this.mouseMiddleDownLocation).Y);

                var origin = new System.Drawing.Point((int)x1 - this.settings.dataOrigin.X, (int)y1 - this.settings.dataOrigin.Y);
                var size = new System.Drawing.Size((int)(x2 - x1), (int)(y2 - y1));

                if (this.lockVerticalAxis)
                {
                    origin.Y = 0;
                    size.Height = this.settings.dataSize.Height - 1;
                }
                if (this.lockHorizontalAxis)
                {
                    origin.X = 0;
                    size.Width = this.settings.dataSize.Width - 1;
                }

                this.settings.mouseMiddleRect = new System.Drawing.Rectangle(origin, size);
            }

            this.Render(true, lowQuality: this.lowQualityWhileDragging);
            return;
        }

        public (double x, double y) GetMouseCoordinates()
        {
            double x = this.plt.CoordinateFromPixelX(this.mouseLocation.X / this.dpiScale);
            double y = this.plt.CoordinateFromPixelY(this.mouseLocation.Y / this.dpiScale);
            return (x, y);
        }

        private void MouseMovedToMoveDraggable(MouseEventArgs e)
        {
            this.plottableBeingDragged.DragTo(
                this.plt.CoordinateFromPixelX(this.GetPixelPosition(e).X), this.plt.CoordinateFromPixelY(this.GetPixelPosition(e).Y),
                this.isShiftPressed, this.isAltPressed, this.isCtrlPressed);
            this.Render(true);
        }

        private void MouseMovedWithoutInteraction(MouseEventArgs e)
        {
            // set the cursor based on what's beneath it
            var draggableUnderCursor = this.plt.GetDraggableUnderMouse(this.GetPixelPosition(e).X, this.GetPixelPosition(e).Y);
            var spCursor = (draggableUnderCursor is null) ? Config.Cursor.Arrow : draggableUnderCursor.DragCursor;
            this.imagePlot.Cursor = this.GetCursor(spCursor);
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.ReleaseMouseCapture();
            var mouseLocation = this.GetPixelPosition(e);

            this.plottableBeingDragged = null;

            if (this.mouseMiddleDownLocation != null)
            {
                double x1 = Math.Min(mouseLocation.X / this.dpiScale, ((Point)this.mouseMiddleDownLocation).X / this.dpiScale);
                double x2 = Math.Max(mouseLocation.X / this.dpiScale, ((Point)this.mouseMiddleDownLocation).X / this.dpiScale);
                double y1 = Math.Min(mouseLocation.Y / this.dpiScale, ((Point)this.mouseMiddleDownLocation).Y / this.dpiScale);
                double y2 = Math.Max(mouseLocation.Y / this.dpiScale, ((Point)this.mouseMiddleDownLocation).Y / this.dpiScale);

                Point topLeft = new Point(x1, y1);
                Size size = new Size(x2 - x1, y2 - y1);
                Point botRight = new Point(topLeft.X + size.Width, topLeft.Y + size.Height);

                if ((size.Width > 2) && (size.Height > 2))
                {
                    // only change axes if suffeciently large square was drawn
                    if (!this.lockHorizontalAxis)
                        this.plt.Axis(
                            x1: this.plt.CoordinateFromPixelX(topLeft.X),
                            x2: this.plt.CoordinateFromPixelX(botRight.X));
                    if (!this.lockVerticalAxis)
                        this.plt.Axis(
                            y1: this.plt.CoordinateFromPixelY(botRight.Y),
                            y2: this.plt.CoordinateFromPixelY(topLeft.Y));
                    AxisChanged?.Invoke(null, null);
                }
                else
                {
                    bool shouldTighten = this.recalculateLayoutOnMouseUp ?? this.plotContainsHeatmap == false;
                    this.plt.AxisAuto(this.middleClickMarginX, this.middleClickMarginY, tightenLayout: shouldTighten);
                    AxisChanged?.Invoke(null, null);
                }
            }

            if (this.mouseRightDownLocation != null)
            {
                double deltaX = Math.Abs(mouseLocation.X - this.mouseRightDownLocation.Value.X);
                double deltaY = Math.Abs(mouseLocation.Y - this.mouseRightDownLocation.Value.Y);
                bool mouseDraggedFar = (deltaX > 3 || deltaY > 3);
                if (this.ContextMenu != null)
                {
                    this.ContextMenu.Visibility = (mouseDraggedFar) ? Visibility.Hidden : Visibility.Visible;
                    this.ContextMenu.IsOpen = (!mouseDraggedFar);
                }
            }
            else
            {
                if (this.ContextMenu != null)
                    this.ContextMenu.IsOpen = false;
            }

            this.mouseLeftDownLocation = null;
            this.mouseRightDownLocation = null;
            this.mouseMiddleDownLocation = null;
            this.axisLimitsOnMouseDown = null;
            this.settings.mouseMiddleRect = null;

            bool shouldRecalculate = this.recalculateLayoutOnMouseUp ?? this.plotContainsHeatmap == false;
            this.Render(recalculateLayout: shouldRecalculate);
        }

        #endregion

        #region mouse clicking

        private void UserControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (this.enableScrollWheelZoom == false)
                return;

            var mousePixel = this.GetPixelPosition(e, applyDpiScaling: false); // DPI-scaling aware

            double xFrac = (e.Delta > 0) ? 1.15 : 0.85;
            double yFrac = (e.Delta > 0) ? 1.15 : 0.85;

            if (this.isCtrlPressed) yFrac = 1;
            if (this.isShiftPressed) xFrac = 1;

            this.plt.AxisZoom(xFrac, yFrac, this.plt.CoordinateFromPixelX(mousePixel.X), this.plt.CoordinateFromPixelY(mousePixel.Y));
            AxisChanged?.Invoke(null, null);
            bool shouldRecalculate = this.recalculateLayoutOnMouseUp ?? this.plotContainsHeatmap == false;
            this.Render(recalculateLayout: shouldRecalculate);
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.doubleClickingTogglesBenchmark)
            {
                this.plt.Benchmark(toggle: true);
                this.Render();
            }
        }

        private void SaveImage(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.FileName = "ScottPlot.png";
            savefile.Filter = "PNG Files (*.png)|*.png;*.png";
            savefile.Filter += "|JPG Files (*.jpg, *.jpeg)|*.jpg;*.jpeg";
            savefile.Filter += "|BMP Files (*.bmp)|*.bmp;*.bmp";
            savefile.Filter += "|TIF files (*.tif, *.tiff)|*.tif;*.tiff";
            savefile.Filter += "|All files (*.*)|*.*";
            if (savefile.ShowDialog() == true)
                this.plt.SaveFig(savefile.FileName);
        }

        private void CopyImage(object sender, RoutedEventArgs e)
        {
            Clipboard.SetImage((BitmapSource)this.imagePlot.Source);
        }

        private void OpenInNewWindow(object sender, RoutedEventArgs e)
        {
            new WpfPlotViewer(this.plt.Copy()).Show();
        }

        private void OpenHelp(object sender, RoutedEventArgs e)
        {
            new WPF.HelpWindow().Show();
        }

        #endregion

        #region event handling

        public event EventHandler Rendered;
        public event EventHandler AxisChanged;

        #endregion
    }
}
