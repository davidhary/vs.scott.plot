﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.VisualTree;
using ScottPlot.Interactive;

using Ava = global::Avalonia;

namespace ScottPlot.Avalonia
{
    internal class AvaPlotBackend : ScottPlot.Interactive.ControlBackend
    {
        private AvaPlot view;
        public AvaPlotBackend(AvaPlot view)
        {
            this.view = view;
        }

        public override void InitializeScottPlot()
        {
            this.view.Find<TextBlock>("lblVersion").Text = Tools.GetVersionString();
            //isDesignerMode = DesignerProperties.GetIsInDesignMode(this);
            this.isDesignerMode = false;

            this.settings = this.plt.GetSettings(showWarning: false);

            var mainGrid = this.view.Find<Ava.Controls.Grid>("mainGrid");

            if (this.isDesignerMode)
            {
                // hide the plot
                mainGrid.RowDefinitions[1].Height = new GridLength(0);
            }
            else
            {
                // hide the version info
                mainGrid.RowDefinitions[0].Height = new GridLength(0);
                //CanvasPlot_SizeChanged(null, null);
                //dpiScaleInput = settings.gfxFigure.DpiX / 96; THIS IS ONLY NECESSARY ON WPF
                this.dpiScaleOutput = this.settings.gfxFigure.DpiX / 96;
                this.view.Find<StackPanel>("canvasDesigner").Background = this.view.transparentBrush;
                this.view.Find<Canvas>("canvasPlot").Background = this.view.transparentBrush;
            }

        }
        public void SetAltPressed(bool value)
        {
            this.isAltPressed = value;
        }

        public void SetCtrlPressed(bool value)
        {
            this.isCtrlPressed = value;
        }

        public void SetShiftPressed(bool value)
        {
            this.isShiftPressed = value;
        }

        public override async void SaveImage()
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.InitialFileName = "ScottPlot.png";

            var filtersPNG = new FileDialogFilter();
            filtersPNG.Name = "PNG Files";
            filtersPNG.Extensions.Add("png");

            var filtersJPEG = new FileDialogFilter();
            filtersJPEG.Name = "JPG Files";
            filtersJPEG.Extensions.Add("jpg");
            filtersJPEG.Extensions.Add("jpeg");

            var filtersBMP = new FileDialogFilter();
            filtersBMP.Name = "BMP Files";
            filtersBMP.Extensions.Add("bmp");

            var filtersTIFF = new FileDialogFilter();
            filtersTIFF.Name = "TIF Files";
            filtersTIFF.Extensions.Add("tif");
            filtersTIFF.Extensions.Add("tiff");

            var filtersGeneric = new FileDialogFilter();
            filtersGeneric.Name = "All Files";
            filtersGeneric.Extensions.Add("*");

            savefile.Filters.Add(filtersPNG);
            savefile.Filters.Add(filtersJPEG);
            savefile.Filters.Add(filtersBMP);
            savefile.Filters.Add(filtersTIFF);
            savefile.Filters.Add(filtersGeneric);


            Task<string> filenameTask = savefile.ShowAsync((Window)this.view.GetVisualRoot());
            await filenameTask;

            if (filenameTask.Exception != null)
            {
                return;
            }

            if ((filenameTask.Result ?? "") != "")
                this.plt.SaveFig(filenameTask.Result);
        }

        public override void SetImagePlot(bool lowQuality)
        {
            this.view.Find<Ava.Controls.Image>("imagePlot").Source = BmpImageFromBmp(this.plt.GetBitmap(true, lowQuality));
        }

        public override void OpenInNewWindow()
        {
            new AvaPlotViewer(this.plt.Copy()).Show();
        }

        public override void OpenHelp()
        {
            new HelpWindow().Show();
        }
        public static Ava.Media.Imaging.Bitmap BmpImageFromBmp(System.Drawing.Bitmap bmp)
        {
            using (var memory = new System.IO.MemoryStream())
            {
                bmp.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new Ava.Media.Imaging.Bitmap(memory);

                return bitmapImage;
            }
        }

        public override List<ContextMenuItem> DefaultRightClickMenu()
        {
            var list = new List<ContextMenuItem>();
            list.Add(new ContextMenuItem()
            {
                itemName = "Save Image",
                onClick = () => this.SaveImage()
            });
            list.Add(new ContextMenuItem()
            {
                itemName = "Open in New Window",
                onClick = () => this.OpenInNewWindow()
            });
            list.Add(new ContextMenuItem()
            {
                itemName = "Help",
                onClick = () => this.OpenHelp()
            });

            return list;
        }

        public override void MouseMovedWithoutInteraction(PointF mouseLocation)
        {
            //nop
        }

        public override void CopyImage()
        {
            //nop
        }
    }

}
