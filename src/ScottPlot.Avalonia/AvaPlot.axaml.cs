﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avalonia.Platform;
using Avalonia.VisualTree;
using ScottPlot.Avalonia;
using ScottPlot.Config;
using ScottPlot.Interactive;
using Ava = Avalonia;

#pragma warning disable IDE1006 // lowercase top-level property

namespace ScottPlot.Avalonia
{
    /// <summary>
    /// Interaction logic for AvaPlot.axaml
    /// </summary>

    [System.ComponentModel.ToolboxItem(true)]
    [System.ComponentModel.DesignTimeVisible(true)]
    public partial class AvaPlot : UserControl
    {
        public Plot plt { get { return this.backend.plt; } }
        internal readonly SolidColorBrush transparentBrush = new SolidColorBrush(Ava.Media.Color.FromUInt32(0), 0);
        internal AvaPlotBackend backend;

        public AvaPlot(Plot plt)
        {
            this.InitializeComponent();
            this.backend = new AvaPlotBackend(this);
            this.SetContextMenu(this.backend.DefaultRightClickMenu());
            this.backend.Reset(plt);
        }

        public AvaPlot()
        {
            this.InitializeComponent();
            this.backend = new AvaPlotBackend(this);
            this.SetContextMenu(this.backend.DefaultRightClickMenu());
            this.backend.Reset(null);
        }

        public void SetContextMenu(List<ContextMenuItem> contextMenuItems)
        {
            this.backend.contextMenuItems = contextMenuItems;
            var cm = new ContextMenu();

            List<MenuItem> menuItems = new List<MenuItem>();
            foreach (var curr in contextMenuItems)
            {
                var menuItem = new MenuItem() { Header = curr.itemName };
                menuItem.Click += (object sender, RoutedEventArgs e) => curr.onClick();
                menuItems.Add(menuItem);
            }
            cm.Items = menuItems;

            this.ContextMenu = cm;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            this.Focusable = true;

            PointerPressed += this.UserControl_MouseDown;
            PointerMoved += this.UserControl_MouseMove;
            PointerReleased += this.UserControl_MouseUp;
            KeyDown += this.OnKeyDown;
            KeyUp += this.OnKeyUp;
            PointerWheelChanged += this.UserControl_MouseWheel;

            PropertyChanged += this.AvaPlot_PropertyChanged;
        }

        private void AvaPlot_PropertyChanged(object sender, AvaloniaPropertyChangedEventArgs e)
        {
            //Debug.WriteLine(e.Property.Name);
            if (e.Property.Name == "Bounds")
            {
                this.plt.Resize((int)((Ava.Rect)e.NewValue).Width, (int)((Ava.Rect)e.NewValue).Height);
                this.Render();
            }

        }

        public void Render()
        {
            this.backend.Render();
        }

        #region user control configuration

        public void Configure(
            bool? enablePanning = null,
            bool? enableRightClickZoom = null,
            bool? enableRightClickMenu = null,
            bool? enableScrollWheelZoom = null,
            bool? lowQualityWhileDragging = null,
            bool? enableDoubleClickBenchmark = null,
            bool? lockVerticalAxis = null,
            bool? lockHorizontalAxis = null,
            bool? equalAxes = null,
            double? middleClickMarginX = null,
            double? middleClickMarginY = null,
            bool? recalculateLayoutOnMouseUp = null
            )
        {
            this.backend.Configure(enablePanning, enableRightClickZoom, enableRightClickMenu, enableScrollWheelZoom, lowQualityWhileDragging, enableDoubleClickBenchmark,
                lockVerticalAxis, lockHorizontalAxis, equalAxes, middleClickMarginX, middleClickMarginY, recalculateLayoutOnMouseUp);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftAlt:
                case Key.RightAlt:
                    this.backend.SetAltPressed(true);
                    break;
                case Key.LeftShift:
                case Key.RightShift:
                    this.backend.SetShiftPressed(true);
                    break;
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    this.backend.SetCtrlPressed(true);
                    break;
            }
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftAlt:
                case Key.RightAlt:
                    this.backend.SetAltPressed(false);
                    break;
                case Key.LeftShift:
                case Key.RightShift:
                    this.backend.SetShiftPressed(false);
                    break;
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    this.backend.SetCtrlPressed(false);
                    break;
            }
        }
        #endregion

        #region mouse tracking

        private Ava.Point GetPixelPosition(PointerEventArgs e)
        {
            Ava.Point pos = e.GetPosition(this);
            Ava.Point dpiCorrectedPos = new Ava.Point(pos.X * this.backend.dpiScaleInput, pos.Y * this.backend.dpiScaleInput);
            return dpiCorrectedPos;
        }

        private System.Drawing.PointF SDPointF(Ava.Point pt)
        {
            return new System.Drawing.PointF((float)pt.X, (float)pt.Y);
        }

        void UserControl_MouseDown(object sender, PointerPressedEventArgs e)
        {
            e.Pointer.Capture(this);

            var mousePixel = this.GetPixelPosition(e);
            MouseButtons button = MouseButtons.Left;
            if (e.GetCurrentPoint(null).Properties.PointerUpdateKind == PointerUpdateKind.LeftButtonPressed) button = MouseButtons.Left;
            else if (e.GetCurrentPoint(null).Properties.PointerUpdateKind == PointerUpdateKind.RightButtonPressed) button = MouseButtons.Right;
            else if (e.GetCurrentPoint(null).Properties.PointerUpdateKind == PointerUpdateKind.MiddleButtonPressed) button = MouseButtons.Middle;


            this.backend.MouseDrag(this.SDPointF(mousePixel), button);
        }

        private void UserControl_MouseMove(object sender, PointerEventArgs e)
        {
            this.backend.MouseMove(this.SDPointF(this.GetPixelPosition(e)));
        }

        public (double x, double y) GetMouseCoordinates() => this.backend.GetMouseCoordinates();

        private void UserControl_MouseUp(object sender, PointerEventArgs e)
        {
            e.Pointer.Capture(null);
            var mouseLocation = this.GetPixelPosition(e);

            if (this.backend.mouseRightDownLocation != null)
            {
                double deltaX = Math.Abs(mouseLocation.X - this.backend.mouseRightDownLocation.Value.X);
                double deltaY = Math.Abs(mouseLocation.Y - this.backend.mouseRightDownLocation.Value.Y);
                bool mouseDraggedFar = (deltaX > 3 || deltaY > 3);
                if (mouseDraggedFar)
                {
                    e.Handled = true; //I wish I was bullshitting you but this is the only way to prevent opening the context menu that works in Avalonia right now
                }
            }
            else
            {
                if (this.ContextMenu != null)
                {
                    this.ContextMenu.Close();
                }
            }

            this.backend.MouseUp();


        }

        #endregion

        #region mouse clicking

        private void UserControl_MouseWheel(object sender, PointerWheelEventArgs e)
        {
            this.backend.MouseWheel(e.Delta.Y);
        }

        #endregion

    }
}
