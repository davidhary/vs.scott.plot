﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ScottPlot.Interactive
{
    public struct ContextMenuItem
    {
        public string itemName;
        public Action onClick;
    }

    public enum MouseButtons
    {
        Left,
        Right,
        Middle
    }

    public abstract class ControlBackend
    {
        public Plot plt { get; protected set; }
        protected Settings settings;
        protected bool isDesignerMode;
        public double dpiScaleInput { get; protected set; } = 1;
        public double dpiScaleOutput { get; protected set; } = 1;
        private bool plotContainsHeatmap => this.settings?.plottables.Where(p => p is PlottableHeatmap).Count() > 0;


        public List<ContextMenuItem> contextMenuItems;

        public virtual List<ContextMenuItem> DefaultRightClickMenu()
        {
            return new List<ContextMenuItem>();
        }

        public void Reset()
        {
            this.Reset(null);
        }

        public void Reset(Plot plt)
        {
            this.plt = (plt is null) ? new Plot() : plt;
            this.InitializeScottPlot();
            this.Render();
        }
        public abstract void InitializeScottPlot();

        protected bool currentlyRendering = false;
        public void Render(bool skipIfCurrentlyRendering = false, bool lowQuality = false, bool recalculateLayout = false)
        {
            if (!this.isDesignerMode)
            {
                if (recalculateLayout)
                    this.plt.TightenLayout();

                if (this.equalAxes)
                    this.plt.AxisEqual();

                if (!(skipIfCurrentlyRendering && this.currentlyRendering))
                {
                    this.currentlyRendering = true;
                    this.SetImagePlot(lowQuality);
                    this.currentlyRendering = false;
                    Rendered?.Invoke(null, null);
                }
            }
        }

        public abstract void SetImagePlot(bool lowQuality);

        public void CanvasSizeChanged(int dpiCorrectedWidth, int dpiCorrectedHeight)
        {
            this.plt?.Resize(dpiCorrectedWidth, dpiCorrectedHeight);
            this.Render();
        }

        #region user control configuration

        private bool enablePanning = true;
        private bool enableZooming = true;
        private bool enableScrollWheelZoom = true;
        private bool lowQualityWhileDragging = true;
        private bool doubleClickingTogglesBenchmark = true;
        private bool lockVerticalAxis = false;
        private bool lockHorizontalAxis = false;
        private bool equalAxes = false;
        private double middleClickMarginX = .1;
        private double middleClickMarginY = .1;
        private bool? recalculateLayoutOnMouseUp = null;
        public void Configure(
            bool? enablePanning = null,
            bool? enableRightClickZoom = null,
            bool? enableRightClickMenu = null,
            bool? enableScrollWheelZoom = null,
            bool? lowQualityWhileDragging = null,
            bool? enableDoubleClickBenchmark = null,
            bool? lockVerticalAxis = null,
            bool? lockHorizontalAxis = null,
            bool? equalAxes = null,
            double? middleClickMarginX = null,
            double? middleClickMarginY = null,
            bool? recalculateLayoutOnMouseUp = null
            )
        {
            if (enablePanning != null) this.enablePanning = (bool)enablePanning;
            if (enableRightClickZoom != null) this.enableZooming = (bool)enableRightClickZoom;
            if (enableRightClickMenu != null) this.contextMenuItems = (enableRightClickMenu.Value) ? this.DefaultRightClickMenu() : null;
            if (enableScrollWheelZoom != null) this.enableScrollWheelZoom = (bool)enableScrollWheelZoom;
            if (lowQualityWhileDragging != null) this.lowQualityWhileDragging = (bool)lowQualityWhileDragging;
            if (enableDoubleClickBenchmark != null) this.doubleClickingTogglesBenchmark = (bool)enableDoubleClickBenchmark;
            if (lockVerticalAxis != null) this.lockVerticalAxis = (bool)lockVerticalAxis;
            if (lockHorizontalAxis != null) this.lockHorizontalAxis = (bool)lockHorizontalAxis;
            if (equalAxes != null) this.equalAxes = (bool)equalAxes;
            this.middleClickMarginX = middleClickMarginX ?? this.middleClickMarginX;
            this.middleClickMarginY = middleClickMarginY ?? this.middleClickMarginY;
            this.recalculateLayoutOnMouseUp = recalculateLayoutOnMouseUp;
        }

        protected bool isAltPressed = false;
        protected bool isShiftPressed = false;
        protected bool isCtrlPressed = false;

        #endregion

        #region mouse tracking

        public System.Drawing.PointF? mouseLeftDownLocation { get; private set; }
        public System.Drawing.PointF? mouseRightDownLocation { get; private set; }
        public System.Drawing.PointF? mouseMiddleDownLocation { get; private set; }

        double[] axisLimitsOnMouseDown;
        private bool isPanningOrZooming
        {
            get
            {
                if (this.axisLimitsOnMouseDown is null) return false;
                if (this.mouseLeftDownLocation != null) return true;
                else if (this.mouseRightDownLocation != null) return true;
                else if (this.mouseMiddleDownLocation != null) return true;
                return false;
            }
        }

        IDraggable plottableBeingDragged = null;
        private bool isMovingDraggable { get { return (this.plottableBeingDragged != null); } }

        public void MouseDrag(PointF mousePosition, MouseButtons button)
        {
            this.plottableBeingDragged = this.plt.GetDraggableUnderMouse(mousePosition.X, mousePosition.Y);

            if (this.plottableBeingDragged is null)
            {
                // MouseDown event is to start a pan or zoom
                if (button == MouseButtons.Left && this.isAltPressed) this.mouseMiddleDownLocation = mousePosition;
                else if (button == MouseButtons.Left && this.enablePanning) this.mouseLeftDownLocation = mousePosition;
                else if (button == MouseButtons.Right && this.enableZooming) this.mouseRightDownLocation = mousePosition;
                else if (button == MouseButtons.Middle && this.enableScrollWheelZoom) this.mouseMiddleDownLocation = mousePosition;
                this.axisLimitsOnMouseDown = this.plt.Axis();
            }
            else
            {
                // mouse is being used to drag a plottable
            }
        }

        protected PointF mouseLocation;
        public void MouseMove(PointF pointerPosition)
        {
            this.mouseLocation = pointerPosition;

            if (this.isPanningOrZooming)
                this.MouseMovedToPanOrZoom();
            else if (this.isMovingDraggable)
                this.MouseMovedToMoveDraggable();
            else
                this.MouseMovedWithoutInteraction(this.mouseLocation);
        }

        private void MouseMovedToPanOrZoom()
        {
            this.plt.Axis(this.axisLimitsOnMouseDown);

            if (this.mouseLeftDownLocation != null)
            {
                // left-click-drag panning
                double deltaX = this.mouseLeftDownLocation.Value.X - this.mouseLocation.X;
                double deltaY = this.mouseLocation.Y - this.mouseLeftDownLocation.Value.Y;

                if (this.isCtrlPressed) deltaY = 0;
                if (this.isShiftPressed) deltaX = 0;

                this.settings.AxesPanPx((int)deltaX, (int)deltaY);
                AxisChanged?.Invoke(null, null);
            }
            else if (this.mouseRightDownLocation != null)
            {
                // right-click-drag zooming
                double deltaX = this.mouseRightDownLocation.Value.X - this.mouseLocation.X;
                double deltaY = this.mouseLocation.Y - this.mouseRightDownLocation.Value.Y;

                if (this.isCtrlPressed == true && this.isShiftPressed == false) deltaY = 0;
                if (this.isShiftPressed == true && this.isCtrlPressed == false) deltaX = 0;

                this.settings.AxesZoomPx(-(int)deltaX, -(int)deltaY, lockRatio: this.isCtrlPressed && this.isShiftPressed);
                AxisChanged?.Invoke(null, null);
            }
            else if (this.mouseMiddleDownLocation != null)
            {
                // middle-click-drag zooming to rectangle
                double x1 = Math.Min(this.mouseLocation.X, this.mouseMiddleDownLocation.Value.X);
                double x2 = Math.Max(this.mouseLocation.X, this.mouseMiddleDownLocation.Value.X);
                double y1 = Math.Min(this.mouseLocation.Y, this.mouseMiddleDownLocation.Value.Y);
                double y2 = Math.Max(this.mouseLocation.Y, this.mouseMiddleDownLocation.Value.Y);

                var origin = new System.Drawing.Point((int)x1 - this.settings.dataOrigin.X, (int)y1 - this.settings.dataOrigin.Y);
                var size = new System.Drawing.Size((int)(x2 - x1), (int)(y2 - y1));

                if (this.lockVerticalAxis)
                {
                    origin.Y = 0;
                    size.Height = this.settings.dataSize.Height - 1;
                }
                if (this.lockHorizontalAxis)
                {
                    origin.X = 0;
                    size.Width = this.settings.dataSize.Width - 1;
                }

                this.settings.mouseMiddleRect = new System.Drawing.Rectangle(origin, size);
            }

            this.Render(true, lowQuality: this.lowQualityWhileDragging);
            return;
        }

        public (double x, double y) GetMouseCoordinates()
        {
            double x = this.plt.CoordinateFromPixelX(this.mouseLocation.X / this.dpiScaleInput);
            double y = this.plt.CoordinateFromPixelY(this.mouseLocation.Y / this.dpiScaleInput);
            return (x, y);
        }

        private void MouseMovedToMoveDraggable()
        {
            this.plottableBeingDragged.DragTo(
                this.plt.CoordinateFromPixelX(this.mouseLocation.X), this.plt.CoordinateFromPixelY(this.mouseLocation.Y),
                this.isShiftPressed, this.isAltPressed, this.isCtrlPressed);
            this.Render(true);
        }

        public abstract void MouseMovedWithoutInteraction(PointF mouseLocation);

        public void MouseUp()
        {
            this.plottableBeingDragged = null;

            if (this.mouseMiddleDownLocation != null)
            {
                double x1 = Math.Min(this.mouseLocation.X, this.mouseMiddleDownLocation.Value.X) / this.dpiScaleOutput;
                double x2 = Math.Max(this.mouseLocation.X, this.mouseMiddleDownLocation.Value.X) / this.dpiScaleOutput;
                double y1 = Math.Min(this.mouseLocation.Y, this.mouseMiddleDownLocation.Value.Y) / this.dpiScaleOutput;
                double y2 = Math.Max(this.mouseLocation.Y, this.mouseMiddleDownLocation.Value.Y) / this.dpiScaleOutput;

                PointF topLeft = new PointF((float)x1, (float)y1);
                SizeF size = new SizeF((float)(x2 - x1), (float)(y2 - y1));
                PointF botRight = new PointF(topLeft.X + size.Width, topLeft.Y + size.Height);

                if ((size.Width > 2) && (size.Height > 2))
                {
                    // only change axes if suffeciently large square was drawn
                    if (!this.lockHorizontalAxis)
                        this.plt.Axis(
                            x1: this.plt.CoordinateFromPixelX(topLeft.X),
                            x2: this.plt.CoordinateFromPixelX(botRight.X));
                    if (!this.lockVerticalAxis)
                        this.plt.Axis(
                            y1: this.plt.CoordinateFromPixelY(botRight.Y),
                            y2: this.plt.CoordinateFromPixelY(topLeft.Y));
                    AxisChanged?.Invoke(null, null);
                }
                else
                {
                    bool shouldTighten = this.recalculateLayoutOnMouseUp ?? !this.plotContainsHeatmap;
                    this.plt.AxisAuto(this.middleClickMarginX, this.middleClickMarginY, tightenLayout: shouldTighten);
                    AxisChanged?.Invoke(null, null);
                }
            }

            this.mouseLeftDownLocation = null;
            this.mouseRightDownLocation = null;
            this.mouseMiddleDownLocation = null;
            this.axisLimitsOnMouseDown = null;
            this.settings.mouseMiddleRect = null;

            bool shouldRecalculate = this.recalculateLayoutOnMouseUp ?? !this.plotContainsHeatmap;
            this.Render(recalculateLayout: shouldRecalculate);
        }

        #endregion

        #region mouse clicking

        public void MouseWheel(double yScroll)
        {
            if (this.enableScrollWheelZoom == false)
                return;

            double xFrac = (yScroll > 0) ? 1.15 : 0.85;
            double yFrac = (yScroll > 0) ? 1.15 : 0.85;

            if (this.isCtrlPressed) yFrac = 1;
            if (this.isShiftPressed) xFrac = 1;

            this.plt.AxisZoom(xFrac, yFrac, this.plt.CoordinateFromPixelX(this.mouseLocation.X), this.plt.CoordinateFromPixelY(this.mouseLocation.Y));
            AxisChanged?.Invoke(null, null);
            bool shouldRecalculate = this.recalculateLayoutOnMouseUp ?? !this.plotContainsHeatmap;
            this.Render(recalculateLayout: shouldRecalculate);
        }

        public void MouseDoubleClick()
        {
            if (this.doubleClickingTogglesBenchmark)
            {
                this.plt.Benchmark(toggle: true);
                this.Render();
            }
        }

        public abstract void SaveImage();

        public abstract void CopyImage();

        public abstract void OpenInNewWindow();

        public abstract void OpenHelp();

        #endregion

        #region event handling

        public event EventHandler Rendered;
        public event EventHandler AxisChanged;

        #endregion


    }
}
