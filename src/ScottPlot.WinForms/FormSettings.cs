using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.UserControls
{
    public partial class FormSettings : Form
    {
        Plot plt;

        // let's pass in a PLT (not a user control) so it's Winforms/WPF-agnostic
        public FormSettings(Plot plt)
        {
            this.plt = plt;
            this.InitializeComponent();
            this.PopualteGuiFromPlot();
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {

        }

        private void PopualteGuiFromPlot()
        {
            // vertical axis
            this.tbYlabel.Text = this.plt.GetSettings().yLabel.text;
            this.tbY2.Text = Math.Round(this.plt.Axis()[3], 4).ToString();
            this.tbY1.Text = Math.Round(this.plt.Axis()[2], 4).ToString();
            this.cbYminor.Checked = this.plt.GetSettings().ticks.displayYminor;
            this.cbYdateTime.Checked = this.plt.GetSettings().ticks.y.dateFormat;

            // horizontal axis
            this.tbXlabel.Text = this.plt.GetSettings().xLabel.text;
            this.tbX2.Text = Math.Round(this.plt.Axis()[1], 4).ToString();
            this.tbX1.Text = Math.Round(this.plt.Axis()[0], 4).ToString();
            this.cbXminor.Checked = this.plt.GetSettings().ticks.displayXminor;
            this.cbXdateTime.Checked = this.plt.GetSettings().ticks.x.dateFormat;

            // tick display options
            this.cbTicksOffset.Checked = this.plt.GetSettings().ticks.useOffsetNotation;
            this.cbTicksMult.Checked = this.plt.GetSettings().ticks.useMultiplierNotation;
            this.cbGrid.Checked = this.plt.GetSettings().HorizontalGridLines.Visible;

            // legend
            this.cbLegend.Checked = this.plt.GetSettings().Legend.Visible;

            // image quality
            this.rbQualityLow.Checked = !this.plt.GetSettings().misc.antiAliasData;
            this.rbQualityHigh.Checked = this.plt.GetSettings().misc.antiAliasData;
            //cbQualityLowWhileDragging.Checked = plt.mouseTracker.lowQualityWhileInteracting;

            // list of plottables
            this.lbPlotObjects.Items.Clear();
            foreach (var plotObject in this.plt.GetPlottables())
                this.lbPlotObjects.Items.Add(plotObject);

            // list of color styles
            this.cbStyle.Items.AddRange(Enum.GetNames(typeof(Style)));
        }

        private void BtnFitDataY_Click(object sender, EventArgs e)
        {
            this.plt.AxisAutoY();
            this.PopualteGuiFromPlot();
        }

        private void BtnFitDataX_Click(object sender, EventArgs e)
        {
            this.plt.AxisAutoX();
            this.PopualteGuiFromPlot();
        }

        private void btnCopyCSV_Click(object sender, EventArgs e)
        {
            int plotObjectIndex = this.lbPlotObjects.SelectedIndex;
            IExportable plottable = (IExportable)this.plt.GetPlottables()[plotObjectIndex];
            Clipboard.SetText(plottable.GetCSV());
        }

        private void BtnExportCSV_Click(object sender, EventArgs e)
        {
            int plotObjectIndex = this.lbPlotObjects.SelectedIndex;
            IExportable plottable = (IExportable)this.plt.GetPlottables()[plotObjectIndex];

            SaveFileDialog savefile = new SaveFileDialog();
            savefile.Title = $"Export CSV data for {plottable}";
            savefile.FileName = "data.csv";
            savefile.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*";
            if (savefile.ShowDialog() == DialogResult.OK)
                plottable.SaveCSV(savefile.FileName);
        }

        private void LbPlotObjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lbPlotObjects.Items.Count > 0 && this.lbPlotObjects.SelectedItem != null)
            {
                int plotObjectIndex = this.lbPlotObjects.SelectedIndex;
                var plottable = this.plt.GetPlottables()[plotObjectIndex];

                this.btnExportCSV.Enabled = plottable is IExportable;
                this.btnCopyCSV.Enabled = plottable is IExportable;

                // don't allow labels to be changed (after new legend system)
                //tbLabel.Enabled = true;
                //tbLabel.Text = plottable.ToString();
                this.tbLabel.Text = "editing disabled";
            }
            else
            {
                this.btnExportCSV.Enabled = false;
                this.btnCopyCSV.Enabled = false;
                this.tbLabel.Enabled = false;
            }
        }

        private void TbLabel_TextChanged(object sender, EventArgs e)
        {
            // don't allow labels to be changed (after new legend system)
            //int plotObjectIndex = lbPlotObjects.SelectedIndex;
            //var plottable = plt.GetPlottables()[plotObjectIndex];
            //plottable.label = tbLabel.ToString;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            var settings = this.plt.GetSettings();

            // vertical axis
            this.plt.YLabel(this.tbYlabel.Text);
            this.plt.Ticks(displayTicksYminor: this.cbYminor.Checked, dateTimeY: this.cbYdateTime.Checked);
            double y1, y2;
            double.TryParse(this.tbY1.Text, out y1);
            double.TryParse(this.tbY2.Text, out y2);
            this.plt.Axis(y1: y1, y2: y2);

            // horizontal axis
            this.plt.XLabel(this.tbXlabel.Text);
            this.plt.Ticks(displayTicksXminor: this.cbXminor.Checked, dateTimeX: this.cbXdateTime.Checked);
            double x1, x2;
            double.TryParse(this.tbX1.Text, out x1);
            double.TryParse(this.tbX2.Text, out x2);
            this.plt.Axis(x1: x1, x2: x2);

            // tick display options
            this.plt.Ticks(useOffsetNotation: this.cbTicksOffset.Checked, useMultiplierNotation: this.cbTicksMult.Checked);

            // image quality
            this.plt.AntiAlias(figure: this.rbQualityHigh.Checked, data: this.rbQualityHigh.Checked);
            //plt.mouseTracker.lowQualityWhileInteracting = cbQualityLowWhileDragging.Checked;

            // misc
            this.plt.Grid(enable: this.cbGrid.Checked);
            this.plt.Legend(enableLegend: this.cbLegend.Checked);
            if (this.cbStyle.Text != "")
            {
                Style newStyle = (Style)Enum.Parse(typeof(Style), this.cbStyle.Text);
                this.plt.Style(newStyle);
            }

            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnTighten_Click(object sender, EventArgs e)
        {
            this.plt.TightenLayout();
        }
    }
}
