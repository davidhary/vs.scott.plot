using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ScottPlot
{

    public partial class FormsPlot : UserControl
    {
        public Plot plt { get; private set; }
        private Settings settings;
        private bool isDesignerMode;
        private static readonly Cursor Arrow = Cursors.Arrow;
#pragma warning disable IDE1006 // Naming Styles
        public Cursor cursor = Arrow;
#pragma warning restore IDE1006 // Naming Styles

        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                base.BackColor = value;
                this.pbPlot.BackColor = value;
            }
        }

        public FormsPlot(Plot plt)
        {
            this.InitializeComponent();
            this.Reset(plt);
        }

        public FormsPlot()
        {
            this.InitializeComponent();
            this.Reset(null);
        }

        public void Reset()
        {
            this.Reset(null);
        }

        public void Reset(Plot plt)
        {
            if (plt is null)
            {
                this.plt = new Plot();
                this.InitializeScottPlot();
            }
            else
            {
                this.plt = plt;
                this.InitializeScottPlot(applyDefaultStyle: false);
            }
            this.Render();
        }

        private void InitializeScottPlot(bool applyDefaultStyle = true)
        {
            this.ContextMenuStrip = this.DefaultRightClickMenu();

            this.pbPlot.MouseWheel += this.PbPlot_MouseWheel;

            this.isDesignerMode = Process.GetCurrentProcess().ProcessName == "devenv";
            this.lblTitle.Visible = this.isDesignerMode;
            this.lblVersion.Visible = this.isDesignerMode;
            this.lblVersion.Text = Tools.GetVersionString();
            this.pbPlot.BackColor = ColorTranslator.FromHtml("#003366");
            this.lblTitle.BackColor = ColorTranslator.FromHtml("#003366");
            this.lblVersion.BackColor = ColorTranslator.FromHtml("#003366");

            if (applyDefaultStyle)
                this.plt.Style(Style.Control);

            this.settings = this.plt.GetSettings(showWarning: false);

            this.PbPlot_MouseUp(null, null);
            this.PbPlot_SizeChanged(null, null);
        }

        private ContextMenuStrip DefaultRightClickMenu()
        {
            var cms = new ContextMenuStrip();
            cms.Items.Add(new ToolStripMenuItem("Save Image", null, new EventHandler(this.SaveImage)));
            cms.Items.Add(new ToolStripMenuItem("Copy Image", null, new EventHandler(this.CopyImage)));
            cms.Items.Add(new ToolStripMenuItem("Open in New Window", null, new EventHandler(this.OpenNewWindow)));
            cms.Items.Add(new ToolStripMenuItem("Settings", null, new EventHandler(this.OpenSettingsWindow)));
            cms.Items.Add(new ToolStripMenuItem("Help", null, new EventHandler(this.OpenHelpWindow)));
            return cms;
        }

        private bool currentlyRendering;
        public void Render(bool skipIfCurrentlyRendering = false, bool lowQuality = false, bool recalculateLayout = false, bool processEvents = false)
        {
            if (this.isDesignerMode)
                return;

            if (recalculateLayout)
                this.plt.TightenLayout();

            if (this.equalAxes)
                this.plt.AxisEqual();

            if (!(skipIfCurrentlyRendering && this.currentlyRendering))
            {
                this.currentlyRendering = true;
                this.pbPlot.Image = this.plt?.GetBitmap(true, lowQuality || this.lowQualityAlways);
                if (this.isPanningOrZooming || this.isMovingDraggable || processEvents)
                    Application.DoEvents();
                this.currentlyRendering = false;
                Rendered?.Invoke(this, EventArgs.Empty);
            }
        }

        private void PbPlot_SizeChanged(object sender, EventArgs e)
        {
            if (this.plt is null)
                return;

            this.plt.Resize(this.Width, this.Height);
            this.Render(skipIfCurrentlyRendering: false);
        }

        #region user control configuration

        private bool plotContainsHeatmap => this.settings?.plottables.Where(p => p is PlottableHeatmap).Count() > 0;

        private bool enablePanning = true;
        private bool enableRightClickZoom = true;
        private bool enableScrollWheelZoom = true;
        private bool lowQualityWhileDragging = true;
        private bool lowQualityAlways = false;
        private bool doubleClickingTogglesBenchmark = true;
        private bool lockVerticalAxis = false;
        private bool lockHorizontalAxis = false;
        private bool equalAxes = false;
        private double middleClickMarginX = .1;
        private double middleClickMarginY = .1;
        private bool? recalculateLayoutOnMouseUp = null;
        private bool showCoordinatesTooltip = false;
        public void Configure(
            bool? enablePanning = null,
            bool? enableZooming = null,
            bool? enableRightClickMenu = null,
            bool? enableScrollWheelZoom = null,
            bool? lowQualityWhileDragging = null,
            bool? lowQualityAlways = null,
            bool? enableDoubleClickBenchmark = null,
            bool? lockVerticalAxis = null,
            bool? lockHorizontalAxis = null,
            bool? equalAxes = null,
            double? middleClickMarginX = null,
            double? middleClickMarginY = null,
            bool? recalculateLayoutOnMouseUp = null,
            bool? showCoordinatesTooltip = null
            )
        {
            if (enablePanning != null) this.enablePanning = (bool)enablePanning;
            if (enableZooming != null) this.enableRightClickZoom = (bool)enableZooming;
            if (enableRightClickMenu != null) this.ContextMenuStrip = (enableRightClickMenu.Value) ? this.DefaultRightClickMenu() : null;
            if (enableScrollWheelZoom != null) this.enableScrollWheelZoom = (bool)enableScrollWheelZoom;
            if (lowQualityWhileDragging != null) this.lowQualityWhileDragging = (bool)lowQualityWhileDragging;
            if (lowQualityAlways != null) this.lowQualityAlways = (bool)lowQualityAlways;
            if (enableDoubleClickBenchmark != null) this.doubleClickingTogglesBenchmark = (bool)enableDoubleClickBenchmark;
            if (lockVerticalAxis != null) this.lockVerticalAxis = (bool)lockVerticalAxis;
            if (lockHorizontalAxis != null) this.lockHorizontalAxis = (bool)lockHorizontalAxis;
            if (equalAxes != null) this.equalAxes = (bool)equalAxes;
            this.middleClickMarginX = middleClickMarginX ?? this.middleClickMarginX;
            this.middleClickMarginY = middleClickMarginY ?? this.middleClickMarginY;
            this.recalculateLayoutOnMouseUp = recalculateLayoutOnMouseUp;
            this.showCoordinatesTooltip = showCoordinatesTooltip ?? this.showCoordinatesTooltip;
        }

        private bool isShiftPressed { get { return (ModifierKeys.HasFlag(Keys.Shift) || (this.lockHorizontalAxis)); } }
        private bool isCtrlPressed { get { return (ModifierKeys.HasFlag(Keys.Control) || (this.lockVerticalAxis)); } }
        private bool isAltPressed { get { return ModifierKeys.HasFlag(Keys.Alt); } }

        #endregion

        #region mouse tracking

        ToolTip tooltip = new ToolTip();
        private Point? mouseLeftDownLocation, mouseRightDownLocation, mouseMiddleDownLocation;
        double[] axisLimitsOnMouseDown;
        private bool isPanningOrZooming
        {
            get
            {
                if (this.axisLimitsOnMouseDown is null) return false;
                if (this.mouseLeftDownLocation != null) return true;
                else if (this.mouseRightDownLocation != null) return true;
                else if (this.mouseMiddleDownLocation != null) return true;
                return false;
            }
        }

        IDraggable plottableBeingDragged = null;
        private bool isMovingDraggable { get { return (this.plottableBeingDragged != null); } }

        private Cursor GetCursor(Config.Cursor scottPlotCursor)
        {
            switch (scottPlotCursor)
            {
                case Config.Cursor.Arrow: return Cursors.Arrow;
                case Config.Cursor.WE: return Cursors.SizeWE;
                case Config.Cursor.NS: return Cursors.SizeNS;
                case Config.Cursor.All: return Cursors.SizeAll;
                default: return Cursors.Help;
            }
        }

        private void PbPlot_MouseDown(object sender, MouseEventArgs e)
        {
            var mousePixel = e.Location;
            this.plottableBeingDragged = this.plt.GetDraggableUnderMouse(mousePixel.X, mousePixel.Y);

            if (this.plottableBeingDragged is null)
            {
                // MouseDown event is to start a pan or zoom
                if (e.Button == MouseButtons.Left && ModifierKeys.HasFlag(Keys.Alt)) this.mouseMiddleDownLocation = e.Location;
                else if (e.Button == MouseButtons.Left && this.enablePanning) this.mouseLeftDownLocation = e.Location;
                else if (e.Button == MouseButtons.Right && this.enableRightClickZoom) this.mouseRightDownLocation = e.Location;
                else if (e.Button == MouseButtons.Middle && this.enableScrollWheelZoom) this.mouseMiddleDownLocation = e.Location;
                this.axisLimitsOnMouseDown = this.plt.Axis();
            }
            else
            {
                // mouse is being used to drag a plottable
                this.OnMouseDownOnPlottable(EventArgs.Empty);
            }

            base.OnMouseDown(e);
        }

        [Obsolete("use Plot.CoordinateFromPixelX() and Plot.CoordinateFromPixelY()")]
        public PointF mouseCoordinates { get { return this.plt.CoordinateFromPixel(this.mouseLocation); } }
        Point mouseLocation;

        private int dpiScale = 1; //Heres hoping that WinForms becomes DPI aware

        private void PbPlot_MouseMove(object sender, MouseEventArgs e)
        {
            this.mouseLocation = e.Location;
            this.OnMouseMoved(e);

            this.tooltip.Hide(this);
            if (this.isPanningOrZooming)
                this.MouseMovedToPanOrZoom(e);
            else if (this.isMovingDraggable)
                this.MouseMovedToMoveDraggable(e);
            else
                this.MouseMovedWithoutInteraction(e);

            base.OnMouseMove(e);
        }


        private void MouseMovedToPanOrZoom(MouseEventArgs e)
        {
            this.plt.Axis(this.axisLimitsOnMouseDown);

            if (this.mouseLeftDownLocation != null)
            {
                // left-click-drag panning
                int deltaX = ((Point)this.mouseLeftDownLocation).X - e.Location.X;
                int deltaY = e.Location.Y - ((Point)this.mouseLeftDownLocation).Y;

                if (this.isCtrlPressed) deltaY = 0;
                if (this.isShiftPressed) deltaX = 0;

                this.settings.AxesPanPx(deltaX, deltaY);
                this.OnAxisChanged();
            }
            else if (this.mouseRightDownLocation != null)
            {
                // right-click-drag zooming
                int deltaX = ((Point)this.mouseRightDownLocation).X - e.Location.X;
                int deltaY = e.Location.Y - ((Point)this.mouseRightDownLocation).Y;

                if (this.isCtrlPressed == true && this.isShiftPressed == false) deltaY = 0;
                if (this.isShiftPressed == true && this.isCtrlPressed == false) deltaX = 0;

                this.settings.AxesZoomPx(-deltaX, -deltaY, lockRatio: this.isCtrlPressed && this.isShiftPressed);
                this.OnAxisChanged();
            }
            else if (this.mouseMiddleDownLocation != null)
            {
                // middle-click-drag zooming to rectangle
                int x1 = Math.Min(e.Location.X, ((Point)this.mouseMiddleDownLocation).X);
                int x2 = Math.Max(e.Location.X, ((Point)this.mouseMiddleDownLocation).X);
                int y1 = Math.Min(e.Location.Y, ((Point)this.mouseMiddleDownLocation).Y);
                int y2 = Math.Max(e.Location.Y, ((Point)this.mouseMiddleDownLocation).Y);

                Point origin = new Point(x1 - this.settings.dataOrigin.X, y1 - this.settings.dataOrigin.Y);
                Size size = new Size(x2 - x1, y2 - y1);

                if (this.lockVerticalAxis)
                {
                    origin.Y = 0;
                    size.Height = this.settings.dataSize.Height - 1;
                }
                if (this.lockHorizontalAxis)
                {
                    origin.X = 0;
                    size.Width = this.settings.dataSize.Width - 1;
                }

                this.settings.mouseMiddleRect = new Rectangle(origin, size);
            }

            this.Render(true, lowQuality: this.lowQualityWhileDragging);
            return;
        }

        public (double x, double y) GetMouseCoordinates()
        {
            double x = this.plt.CoordinateFromPixelX(this.mouseLocation.X / this.dpiScale);
            double y = this.plt.CoordinateFromPixelY(this.mouseLocation.Y / this.dpiScale);
            return (x, y);
        }

        private void MouseMovedToMoveDraggable(MouseEventArgs e)
        {
            this.plottableBeingDragged.DragTo(
                this.plt.CoordinateFromPixelX(e.Location.X),
                this.plt.CoordinateFromPixelY(e.Location.Y),
                this.isShiftPressed, this.isAltPressed, this.isCtrlPressed);
            this.OnMouseDragPlottable(EventArgs.Empty);
            this.Render(true, lowQuality: this.lowQualityWhileDragging);
        }

        private void MouseMovedWithoutInteraction(MouseEventArgs e)
        {
            if (this.showCoordinatesTooltip)
            {
                double coordX = this.plt.CoordinateFromPixelX(e.Location.X);
                double coordY = this.plt.CoordinateFromPixelY(e.Location.Y);
                this.tooltip.Show($"{coordX:N2}, {coordY:N2}", this, e.Location.X + 15, e.Location.Y);
            }

            // set the cursor based on what's beneath it
            var draggableUnderCursor = this.plt.GetDraggableUnderMouse(e.Location.X, e.Location.Y);
            var spCursor = (draggableUnderCursor is null) ? Config.Cursor.Arrow : draggableUnderCursor.DragCursor;
            this.pbPlot.Cursor = this.GetCursor(spCursor);
        }

        private void PbPlot_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.mouseMiddleDownLocation != null)
            {
                int x1 = Math.Min(e.Location.X, ((Point)this.mouseMiddleDownLocation).X);
                int x2 = Math.Max(e.Location.X, ((Point)this.mouseMiddleDownLocation).X);
                int y1 = Math.Min(e.Location.Y, ((Point)this.mouseMiddleDownLocation).Y);
                int y2 = Math.Max(e.Location.Y, ((Point)this.mouseMiddleDownLocation).Y);

                Point topLeft = new Point(x1, y1);
                Size size = new Size(x2 - x1, y2 - y1);
                Point botRight = new Point(topLeft.X + size.Width, topLeft.Y + size.Height);

                if ((size.Width > 2) && (size.Height > 2))
                {
                    // only change axes if suffeciently large square was drawn
                    if (!this.lockHorizontalAxis)
                        this.plt.Axis(
                            x1: this.plt.CoordinateFromPixelX(topLeft.X),
                            x2: this.plt.CoordinateFromPixelX(botRight.X));
                    if (!this.lockVerticalAxis)
                        this.plt.Axis(
                            y1: this.plt.CoordinateFromPixelY(botRight.Y),
                            y2: this.plt.CoordinateFromPixelY(topLeft.Y));
                    this.OnAxisChanged();
                }
                else
                {
                    bool shouldTighten = this.recalculateLayoutOnMouseUp ?? this.plotContainsHeatmap == false;
                    this.plt.AxisAuto(this.middleClickMarginX, this.middleClickMarginY, tightenLayout: shouldTighten);
                    this.OnAxisChanged();
                }
            }

            if (this.mouseRightDownLocation != null)
            {
                int deltaX = Math.Abs(((Point)this.mouseRightDownLocation).X - e.Location.X);
                int deltaY = Math.Abs(((Point)this.mouseRightDownLocation).Y - e.Location.Y);
                bool mouseDraggedFar = (deltaX > 3 || deltaY > 3);

                if (mouseDraggedFar)
                    this.ContextMenuStrip?.Hide();
                else
                    this.OnMenuDeployed();
            }

            if (this.isPanningOrZooming)
            {
                this.OnMouseDragged(EventArgs.Empty);
            }

            if (this.plottableBeingDragged != null)
            {
                this.OnMouseDropPlottable(EventArgs.Empty);
            }

            this.OnMouseClicked(e);
            base.OnMouseUp(e);

            this.mouseLeftDownLocation = null;
            this.mouseRightDownLocation = null;
            this.mouseMiddleDownLocation = null;
            this.axisLimitsOnMouseDown = null;
            this.settings.mouseMiddleRect = null;
            this.plottableBeingDragged = null;

            bool shouldRecalculate = this.recalculateLayoutOnMouseUp ?? this.plotContainsHeatmap == false;
            this.Render(recalculateLayout: shouldRecalculate);
        }

        private void PbPlot_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.OnMouseDoubleClicked(e);
            base.OnMouseDoubleClick(e);
        }

        private void PbPlot_MouseWheel(object sender, MouseEventArgs e)
        {
            if (this.enableScrollWheelZoom == false)
                return;

            double xFrac = (e.Delta > 0) ? 1.15 : 0.85;
            double yFrac = (e.Delta > 0) ? 1.15 : 0.85;

            if (this.isCtrlPressed) yFrac = 1;
            if (this.isShiftPressed) xFrac = 1;

            this.plt.AxisZoom(xFrac, yFrac, this.plt.CoordinateFromPixelX(e.Location.X), this.plt.CoordinateFromPixelY(e.Location.Y));

            bool shouldRecalculate = this.recalculateLayoutOnMouseUp ?? this.plotContainsHeatmap == false;
            this.Render(recalculateLayout: shouldRecalculate);
            this.OnAxisChanged();

            base.OnMouseWheel(e);
        }

        #endregion

        #region menus and forms

        private void SaveImage(object sender, EventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.FileName = "ScottPlot.png";
            savefile.Filter = "PNG Files (*.png)|*.png;*.png";
            savefile.Filter += "|JPG Files (*.jpg, *.jpeg)|*.jpg;*.jpeg";
            savefile.Filter += "|BMP Files (*.bmp)|*.bmp;*.bmp";
            savefile.Filter += "|TIF files (*.tif, *.tiff)|*.tif;*.tiff";
            savefile.Filter += "|All files (*.*)|*.*";
            if (savefile.ShowDialog() == DialogResult.OK)
                this.plt.SaveFig(savefile.FileName);
        }

        private void CopyImage(object sender, EventArgs e)
        {
            Clipboard.SetImage(this.plt.GetBitmap(true));
        }

        private void OpenSettingsWindow(object sender, EventArgs e)
        {
            new UserControls.FormSettings(this.plt).ShowDialog();
            this.Render();
        }

        private void OpenHelpWindow(object sender, EventArgs e)
        {
            new UserControls.FormHelp().ShowDialog();
        }

        private void OpenNewWindow(object sender, EventArgs e)
        {
            new FormsPlotViewer(this.plt.Copy()).Show();
        }

        #endregion

        #region custom events

        public event EventHandler Rendered;
        public event EventHandler MouseDownOnPlottable;
        public event EventHandler MouseDragPlottable;
        public event MouseEventHandler MouseMoved;
        public event EventHandler MouseDragged;
        public event EventHandler MouseDropPlottable;
        public event EventHandler AxesChanged;
        public event MouseEventHandler MouseClicked;
        public event MouseEventHandler MouseDoubleClicked;
        public event MouseEventHandler MenuDeployed;
        protected virtual void OnMouseDownOnPlottable(EventArgs e) { MouseDownOnPlottable?.Invoke(this, e); }
        protected virtual void OnMouseDragPlottable(EventArgs e) { MouseDragPlottable?.Invoke(this, e); }
        protected virtual void OnMouseMoved(MouseEventArgs e) { MouseMoved?.Invoke(this, e); }
        protected virtual void OnMouseDragged(EventArgs e) { MouseDragged?.Invoke(this, e); }
        protected virtual void OnMouseDropPlottable(EventArgs e) { MouseDropPlottable?.Invoke(this, e); }
        protected virtual void OnMouseClicked(MouseEventArgs e) { MouseClicked?.Invoke(this, e); }
        protected virtual void OnAxisChanged() { AxesChanged?.Invoke(this, null); }

        protected virtual void OnMouseDoubleClicked(MouseEventArgs e)
        {
            MouseDoubleClicked?.Invoke(this, e);
            if (this.doubleClickingTogglesBenchmark)
            {
                this.plt.Benchmark(toggle: true);
                this.Render();
            }
        }

        protected virtual void OnMenuDeployed()
        {
            MenuDeployed?.Invoke(this, null);
        }

        #endregion
    }
}
