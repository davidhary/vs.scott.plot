﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ScottPlot.DataStructures
{
    public class SegmentedTree<T> where T : struct, IComparable
    {
        private T[] sourceArray;

        private T[] TreeMin;
        private T[] TreeMax;
        private int n = 0; // size of each Tree
        public bool TreesReady = false;
        // precompiled lambda expressions for fast math on generic
        private static Func<T, T, T> MinExp;
        private static Func<T, T, T> MaxExp;
        private static Func<T, T, bool> EqualExp;
        private static Func<T> MaxValue;
        private static Func<T> MinValue;
        private static Func<T, T, bool> LessThanExp;
        private static Func<T, T, bool> GreaterThanExp;

        public T[] SourceArray
        {
            get => this.sourceArray;
            set
            {
                if (value == null)
                    throw new Exception("Source Array cannot be null");
                this.sourceArray = value;
                this.UpdateTrees();
            }
        }

        public SegmentedTree()
        {
            try // runtime check
            {
                Convert.ToDouble(new T());
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Unsupported data type, provide convertable to double data types");
            }
            this.InitExp();
        }

        public SegmentedTree(T[] data)
        {
            this.UpdateTreesInBackground();
        }

        public async Task SetSourceAsync(T[] data)
        {
            if (data == null)
                throw new Exception("Data cannot be null");
            this.sourceArray = data;
            await Task.Run(() => this.UpdateTreesInBackground());
        }

        private void InitExp()
        {
            ParameterExpression paramA = Expression.Parameter(typeof(T), "a");
            ParameterExpression paramB = Expression.Parameter(typeof(T), "b");
            // add the parameters together
            ConditionalExpression bodyMin = Expression.Condition(Expression.LessThanOrEqual(paramA, paramB), paramA, paramB);
            ConditionalExpression bodyMax = Expression.Condition(Expression.GreaterThanOrEqual(paramA, paramB), paramA, paramB);
            BinaryExpression bodyEqual = Expression.Equal(paramA, paramB);
            MemberExpression bodyMaxValue = Expression.MakeMemberAccess(null, typeof(T).GetField("MaxValue"));
            MemberExpression bodyMinValue = Expression.MakeMemberAccess(null, typeof(T).GetField("MinValue"));
            BinaryExpression bodyLessThan = Expression.LessThan(paramA, paramB);
            BinaryExpression bodyGreaterThan = Expression.GreaterThan(paramA, paramB);
            // compile it
            MinExp = Expression.Lambda<Func<T, T, T>>(bodyMin, paramA, paramB).Compile();
            MaxExp = Expression.Lambda<Func<T, T, T>>(bodyMax, paramA, paramB).Compile();
            EqualExp = Expression.Lambda<Func<T, T, bool>>(bodyEqual, paramA, paramB).Compile();
            MaxValue = Expression.Lambda<Func<T>>(bodyMaxValue).Compile();
            MinValue = Expression.Lambda<Func<T>>(bodyMinValue).Compile();
            LessThanExp = Expression.Lambda<Func<T, T, bool>>(bodyLessThan, paramA, paramB).Compile();
            GreaterThanExp = Expression.Lambda<Func<T, T, bool>>(bodyGreaterThan, paramA, paramB).Compile();
        }

        public void updateElement(int index, T newValue)
        {
            this.sourceArray[index] = newValue;
            // Update Tree, can be optimized            
            if (index == this.sourceArray.Length - 1) // last elem haven't pair
            {
                this.TreeMin[this.n / 2 + index / 2] = this.sourceArray[index];
                this.TreeMax[this.n / 2 + index / 2] = this.sourceArray[index];
            }
            else if (index % 2 == 0) // even elem have right pair
            {
                this.TreeMin[this.n / 2 + index / 2] = MinExp(this.sourceArray[index], this.sourceArray[index + 1]);
                this.TreeMax[this.n / 2 + index / 2] = MaxExp(this.sourceArray[index], this.sourceArray[index + 1]);
            }
            else // odd elem have left pair
            {
                this.TreeMin[this.n / 2 + index / 2] = MinExp(this.sourceArray[index], this.sourceArray[index - 1]);
                this.TreeMax[this.n / 2 + index / 2] = MaxExp(this.sourceArray[index], this.sourceArray[index - 1]);
            }

            T candidate;
            for (int i = (this.n / 2 + index / 2) / 2; i > 0; i /= 2)
            {
                candidate = MinExp(this.TreeMin[i * 2], this.TreeMin[i * 2 + 1]);
                if (EqualExp(this.TreeMin[i], candidate)) // if node same then new value don't need to recalc all upper
                    break;
                this.TreeMin[i] = candidate;
            }
            for (int i = (this.n / 2 + index / 2) / 2; i > 0; i /= 2)
            {
                candidate = MaxExp(this.TreeMax[i * 2], this.TreeMax[i * 2 + 1]);
                if (EqualExp(this.TreeMax[i], candidate)) // if node same then new value don't need to recalc all upper
                    break;
                this.TreeMax[i] = candidate;
            }
        }

        public void updateRange(int from, int to, T[] newData, int fromData = 0) // RangeUpdate
        {
            //update source signal
            for (int i = from; i < to; i++)
            {
                this.sourceArray[i] = newData[i - from + fromData];
            }

            for (int i = this.n / 2 + from / 2; i < this.n / 2 + to / 2; i++)
            {
                this.TreeMin[i] = MinExp(this.sourceArray[i * 2 - this.n], this.sourceArray[i * 2 + 1 - this.n]);
                this.TreeMax[i] = MaxExp(this.sourceArray[i * 2 - this.n], this.sourceArray[i * 2 + 1 - this.n]);
            }
            if (to == this.sourceArray.Length) // last elem haven't pair
            {
                this.TreeMin[this.n / 2 + to / 2] = this.sourceArray[to - 1];
                this.TreeMax[this.n / 2 + to / 2] = this.sourceArray[to - 1];
            }
            else if (to % 2 == 1) //last elem even(to-1) and not last
            {
                this.TreeMin[this.n / 2 + to / 2] = MinExp(this.sourceArray[to - 1], this.sourceArray[to]);
                this.TreeMax[this.n / 2 + to / 2] = MaxExp(this.sourceArray[to - 1], this.sourceArray[to]);
            }

            from = (this.n / 2 + from / 2) / 2;
            to = (this.n / 2 + to / 2) / 2;

            T candidate;
            while (from != 0) // up to root elem, that is [1], [0] - is free elem
            {
                if (from != to)
                {
                    for (int i = from; i <= to; i++) // Recalc all level nodes in range 
                    {
                        this.TreeMin[i] = MinExp(this.TreeMin[i * 2], this.TreeMin[i * 2 + 1]);
                        this.TreeMax[i] = MaxExp(this.TreeMax[i * 2], this.TreeMax[i * 2 + 1]);
                    }
                }
                else
                {
                    // left == rigth, so no need more from to loop
                    for (int i = from; i > 0; i /= 2) // up to root node
                    {
                        candidate = MinExp(this.TreeMin[i * 2], this.TreeMin[i * 2 + 1]);
                        if (EqualExp(this.TreeMin[i], candidate)) // if node same then new value don't need to recalc all upper
                            break;
                        this.TreeMin[i] = candidate;
                    }

                    for (int i = from; i > 0; i /= 2) // up to root node
                    {
                        candidate = MaxExp(this.TreeMax[i * 2], this.TreeMax[i * 2 + 1]);
                        if (EqualExp(this.TreeMax[i], candidate)) // if node same then new value don't need to recalc all upper
                            break;
                        this.TreeMax[i] = candidate;
                    }
                    // all work done exit while loop
                    break;
                }
                // level up
                from = from / 2;
                to = to / 2;
            }
        }

        public void updateData(int from, T[] newData)
        {
            this.updateRange(from, newData.Length, newData);
        }

        public void updateData(T[] newData)
        {
            this.updateRange(0, newData.Length, newData);
        }

        public void UpdateTreesInBackground()
        {
            Task.Run(() => { this.UpdateTrees(); });
        }

        public void UpdateTrees()
        {
            // O(n) to build trees
            this.TreesReady = false;
            try
            {
                if (this.sourceArray.Length == 0)
                    throw new ArgumentOutOfRangeException($"Array cant't be empty");
                // Size up to pow2
                if (this.sourceArray.Length > 0x40_00_00_00) // pow 2 must be more then int.MaxValue
                    throw new ArgumentOutOfRangeException($"Array higher than {0x40_00_00_00} not supported by SignalConst");
                int pow2 = 1;
                while (pow2 < 0x40_00_00_00 && pow2 < this.sourceArray.Length)
                    pow2 <<= 1;
                this.n = pow2;
                this.TreeMin = new T[this.n];
                this.TreeMax = new T[this.n];
                T maxValue = MaxValue();
                T minValue = MinValue();

                // fill bottom layer of tree
                for (int i = 0; i < this.sourceArray.Length / 2; i++) // with source array pairs min/max
                {
                    this.TreeMin[this.n / 2 + i] = MinExp(this.sourceArray[i * 2], this.sourceArray[i * 2 + 1]);
                    this.TreeMax[this.n / 2 + i] = MaxExp(this.sourceArray[i * 2], this.sourceArray[i * 2 + 1]);
                }
                if (this.sourceArray.Length % 2 == 1) // if array size odd, last element haven't pair to compare
                {
                    this.TreeMin[this.n / 2 + this.sourceArray.Length / 2] = this.sourceArray[this.sourceArray.Length - 1];
                    this.TreeMax[this.n / 2 + this.sourceArray.Length / 2] = this.sourceArray[this.sourceArray.Length - 1];
                }
                for (int i = this.n / 2 + (this.sourceArray.Length + 1) / 2; i < this.n; i++) // min/max for pairs of nonexistent elements
                {
                    this.TreeMin[i] = minValue;
                    this.TreeMax[i] = maxValue;
                }
                // fill other layers
                for (int i = this.n / 2 - 1; i > 0; i--)
                {
                    this.TreeMin[i] = MinExp(this.TreeMin[2 * i], this.TreeMin[2 * i + 1]);
                    this.TreeMax[i] = MaxExp(this.TreeMax[2 * i], this.TreeMax[2 * i + 1]);
                }
                this.TreesReady = true;
            }
            catch (OutOfMemoryException)
            {
                this.TreeMin = null;
                this.TreeMax = null;
                this.TreesReady = false;
                return;
            }
        }

        //  O(log(n)) for each range min/max query
        public void MinMaxRangeQuery(int l, int r, out double lowestValue, out double highestValue)
        {
            T lowestValueT;
            T highestValueT;
            // if the tree calculation isn't finished or if it crashed
            if (!this.TreesReady)
            {
                // use the original (slower) min/max calculated method
                lowestValueT = this.sourceArray[l];
                highestValueT = this.sourceArray[l];
                for (int i = l; i < r; i++)
                {
                    if (LessThanExp(this.sourceArray[i], lowestValueT))
                        lowestValueT = this.sourceArray[i];
                    if (GreaterThanExp(this.sourceArray[i], highestValueT))
                        highestValueT = this.sourceArray[i];
                }
                lowestValue = Convert.ToDouble(lowestValueT);
                highestValue = Convert.ToDouble(highestValueT);
                return;
            }

            lowestValueT = MaxValue();
            highestValueT = MinValue();
            if (l == r)
            {
                lowestValue = highestValue = Convert.ToDouble(this.sourceArray[l]);
                return;
            }
            // first iteration on source array that virtualy bottom of tree
            if ((l & 1) != 1) // l is left child
            {
                lowestValueT = MinExp(lowestValueT, this.sourceArray[l]);
                highestValueT = MaxExp(highestValueT, this.sourceArray[l]);
            }
            if ((r & 1) == 1) // r is right child
            {
                lowestValueT = MinExp(lowestValueT, this.sourceArray[r]);
                highestValueT = MaxExp(highestValueT, this.sourceArray[r]);
            }
            // go up from array to bottom of Tree
            l = (l + this.n) / 2;
            r = (r + this.n) / 2;
            // next iterations on tree
            while (l <= r)
            {
                if ((l & 1) == 1) // l is right child
                {
                    lowestValueT = MinExp(lowestValueT, this.TreeMin[l]);
                    highestValueT = MaxExp(highestValueT, this.TreeMax[l]);
                }
                if ((r & 1) != 1) // r is left child
                {
                    lowestValueT = MinExp(lowestValueT, this.TreeMin[r]);
                    highestValueT = MaxExp(highestValueT, this.TreeMax[r]);
                }
                // go up one level
                l = (l + 1) / 2;
                r = (r - 1) / 2;
            }
            lowestValue = Convert.ToDouble(lowestValueT);
            highestValue = Convert.ToDouble(highestValueT);
        }
    }
}
