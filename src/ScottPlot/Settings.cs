﻿using ScottPlot.Config;
using ScottPlot.Renderable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

// TODO: move plottables to their own module
// TODO: move mouse/axes interaction functions into the mouse module somehow

namespace ScottPlot
{
    /// <summary>
    /// 
    /// This class stores settings and data necessary to create a ScottPlot.
    /// It is a data transfer object which is easy to pass but should be inaccessible to end users.
    /// 
    /// If you are passed this object, you have EVERYTHING you need to render an image.
    /// An ultimate goal is for this settings object to be able to be passed to different rendering engines.
    /// 
    /// </summary>
    public class Settings
    {
        // these properties get set at instantiation or after size or axis adjustments
        public Size figureSize { get { return this.layout.plot.Size; } }
        public Point dataOrigin { get { return this.layout.data.Location; } }
        public Size dataSize { get { return this.layout.data.Size; } }

        // Eventually move graphics objects to their own module.
        public Graphics gfxFigure;
        public Graphics gfxData;
        public Bitmap bmpFigure;
        public Bitmap bmpData;

        // Renderables (eventually store these in a List)
        public readonly FigureBackground FigureBackground = new FigureBackground();
        public readonly DataBackground DataBackground = new DataBackground();
        public readonly GridLines HorizontalGridLines = new GridLines() { Orientation = Orientation.Horizontal };
        public readonly GridLines VerticalGridLines = new GridLines() { Orientation = Orientation.Vertical };
        public readonly Benchmark Benchmark = new Benchmark();
        public readonly Legend Legend = new Legend();

        // plottables
        public readonly List<Plottable> plottables = new List<Plottable>();

        // TODO: STRANGLE CONFIG OBJECTS AS PART OF https://github.com/swharden/ScottPlot/pull/511
        public Config.TextLabel title = new Config.TextLabel() { fontSize = 16, bold = true };
        public Config.TextLabel xLabel = new Config.TextLabel() { fontSize = 16 };
        public Config.TextLabel yLabel = new Config.TextLabel() { fontSize = 16 };
        public Config.Misc misc = new Config.Misc();
        //public Config.Benchmark benchmark = new Config.Benchmark();
        //public Config.Grid grid = new Config.Grid();
        public Config.Axes axes = new Config.Axes();
        public readonly Config.Layout layout = new Config.Layout();
        public Config.Ticks ticks = new Config.Ticks();
        public System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.DefaultThreadCurrentCulture;

        // default colorset
        public Drawing.Colorset colorset = Drawing.Colorset.Category10;

        // mouse interaction
        public Rectangle? mouseMiddleRect = null;

        // scales calculations must occur at this level because the axes are unaware of pixel dimensions
        public double xAxisScale { get { return this.dataSize.Width / this.axes.x.span; } } // pixels per unit
        public double yAxisScale { get { return this.dataSize.Height / this.axes.y.span; } } // pixels per unit
        public double xAxisUnitsPerPixel { get { return 1.0 / this.xAxisScale; } }
        public double yAxisUnitsPerPixel { get { return 1.0 / this.yAxisScale; } }

        public static int defaultDPI = 96;
        public static double DPIScale;

        // this has to be here because color module is unaware of plottables list
        public Color GetNextColor() { return this.colorset.GetColor(this.plottables.Count); }

        public void Resize(int width, int height, bool useMeasuredStrings = false)
        {
            if (useMeasuredStrings && this.gfxData != null)
            {
                // this section was added before display scaling issues (pixel-referenced font sizes) were figured out.
                // it is probably not needed...

                string sampleString = "IPjg8.8";
                this.layout.yLabelWidth = (int)Drawing.GDI.MeasureString(this.gfxData, sampleString, this.yLabel.font).Height;
                this.layout.y2LabelWidth = (int)Drawing.GDI.MeasureString(this.gfxData, sampleString, this.yLabel.font).Height; // currently y2 isn't supported
                this.layout.titleHeight = (int)Drawing.GDI.MeasureString(this.gfxData, sampleString, this.title.font).Height;
                this.layout.xLabelHeight = (int)Drawing.GDI.MeasureString(this.gfxData, sampleString, this.xLabel.font).Height;

                var tickSize = Drawing.GDI.MeasureString(this.gfxData, "0.001", this.ticks.font);
                this.layout.yScaleWidth = (int)tickSize.Width;
                this.layout.y2ScaleWidth = (int)tickSize.Height; // currently y2 isn't supported
                this.layout.xScaleHeight = (int)tickSize.Height;
            }

            this.layout.Update(width, height);

            if (this.axes.equalAxes)
            {
                var limits = new Config.AxisLimits2D(this.axes.ToArray());

                double xUnitsPerPixel = limits.xSpan / this.dataSize.Width;
                double yUnitsPerPixel = limits.ySpan / this.dataSize.Height;

                if (yUnitsPerPixel > xUnitsPerPixel)
                    this.axes.Zoom(xUnitsPerPixel / yUnitsPerPixel, 1);
                else
                    this.axes.Zoom(1, yUnitsPerPixel / xUnitsPerPixel);
            }
        }

        public void TightenLayout(int padLeft = 15, int padRight = 15, int padBottom = 15, int padTop = 15)
        {
            this.Resize(this.figureSize.Width, this.figureSize.Height);

            // update the layout with sizes based on configuration in settings

            this.layout.titleHeight = (int)this.title.size.Height + 3;

            // disable y2 label and scale by default
            this.layout.y2LabelWidth = 0;
            this.layout.y2ScaleWidth = 0;

            this.layout.yLabelWidth = (int)this.yLabel.size.Height + 3;
            this.layout.xLabelHeight = (int)this.xLabel.size.Height + 3;

            // automatically set yScale size to tick labels
            int minYtickWidth = 40;
            this.layout.yScaleWidth = Math.Max(minYtickWidth, (int)this.ticks.y.maxLabelSize.Width);

            // automatically set xScale size to high labels
            int minXtickHeight = 10;
            this.layout.xScaleHeight = Math.Max(minXtickHeight, (int)this.ticks.x.maxLabelSize.Height);

            // collapse things that are hidden or empty
            if (!this.ticks.displayXmajor)
                this.layout.xScaleHeight = 0;
            if (!this.ticks.displayYmajor)
                this.layout.yScaleWidth = 0;
            if (this.title.text == "")
                this.layout.titleHeight = 0;
            if (this.yLabel.text == "")
                this.layout.yLabelWidth = 0;
            if (this.xLabel.text == "")
                this.layout.xLabelHeight = 0;

            // eliminate all right-side pixels if right-frame is not drawn
            if (!this.layout.displayFrameByAxis[1])
            {
                this.layout.yLabelWidth = 0;
                this.layout.y2ScaleWidth = 0;
            }

            // expand edges to accomodate argument padding
            this.layout.yLabelWidth = Math.Max(this.layout.yLabelWidth, padLeft);
            this.layout.y2LabelWidth = Math.Max(this.layout.y2LabelWidth, padRight);
            this.layout.xLabelHeight = Math.Max(this.layout.xLabelHeight, padBottom);
            this.layout.titleHeight = Math.Max(this.layout.titleHeight, padTop);

            this.layout.Update(this.figureSize.Width, this.figureSize.Height);
            this.layout.tighteningOccurred = true;
        }

        public void AxesPanPx(int dxPx, int dyPx)
        {
            if (!this.axes.hasBeenSet)
                this.AxisAuto();
            this.axes.x.Pan((double)dxPx / this.xAxisScale);
            this.axes.y.Pan((double)dyPx / this.yAxisScale);
        }

        public void AxesZoomPx(int xPx, int yPx, bool lockRatio = false)
        {
            double dX = (double)xPx / this.xAxisScale;
            double dY = (double)yPx / this.yAxisScale;
            double dXFrac = dX / (Math.Abs(dX) + this.axes.x.span);
            double dYFrac = dY / (Math.Abs(dY) + this.axes.y.span);
            if (this.axes.equalAxes)
            {
                double zoomValue = dX + dY; // NE - max zoomIn, SW - max ZoomOut, NW and SE - 0 zoomValue
                double zoomFrac = zoomValue / (Math.Abs(zoomValue) + this.axes.x.span);
                dXFrac = zoomFrac;
                dYFrac = zoomFrac;
            }
            if (lockRatio)
            {
                double meanFrac = (dXFrac + dYFrac) / 2;
                dXFrac = meanFrac;
                dYFrac = meanFrac;
            }
            this.axes.Zoom(Math.Pow(10, dXFrac), Math.Pow(10, dYFrac));
        }

        public void AxisAuto(
            double horizontalMargin = .1, double verticalMargin = .1,
            bool xExpandOnly = false, bool yExpandOnly = false,
            bool autoX = true, bool autoY = true
            )
        {
            var oldLimits = new Config.AxisLimits2D(this.axes.ToArray());
            var newLimits = new Config.AxisLimits2D();

            foreach (var plottable in this.plottables)
            {
                Config.AxisLimits2D plottableLimits = plottable.GetLimits();
                if (autoX && !yExpandOnly)
                    newLimits.ExpandX(plottableLimits);
                if (autoY && !xExpandOnly)
                    newLimits.ExpandY(plottableLimits);
            }

            newLimits.MakeRational();

            if (this.axes.equalAxes)
            {
                var xUnitsPerPixel = newLimits.xSpan / (this.dataSize.Width * (1 - horizontalMargin));
                var yUnitsPerPixel = newLimits.ySpan / (this.dataSize.Height * (1 - verticalMargin));
                this.axes.Set(newLimits);
                if (yUnitsPerPixel > xUnitsPerPixel)
                    this.axes.Zoom((1 - horizontalMargin) * xUnitsPerPixel / yUnitsPerPixel, 1 - verticalMargin);
                else
                    this.axes.Zoom(1 - horizontalMargin, (1 - verticalMargin) * yUnitsPerPixel / xUnitsPerPixel);
                return;
            }

            if (xExpandOnly)
            {
                oldLimits.ExpandX(newLimits);
                this.axes.Set(oldLimits.x1, oldLimits.x2, null, null);
                this.axes.Zoom(1 - horizontalMargin, 1);
            }

            if (yExpandOnly)
            {
                oldLimits.ExpandY(newLimits);
                this.axes.Set(null, null, oldLimits.y1, oldLimits.y2);
                this.axes.Zoom(1, 1 - verticalMargin);
            }

            if ((!xExpandOnly) && (!yExpandOnly))
            {
                this.axes.Set(newLimits);
                this.axes.Zoom(1 - horizontalMargin, 1 - verticalMargin);
            }

            if (this.plottables.Count == 0)
            {
                this.axes.x.hasBeenSet = false;
                this.axes.y.hasBeenSet = false;
            }

            this.layout.tighteningOccurred = false;
        }

        /// <summary>
        /// Returns the X pixel corresponding to an X axis coordinate
        /// </summary>
        public double GetPixelX(double locationX)
        {
            return (locationX - this.axes.x.min) * this.xAxisScale;
        }

        /// <summary>
        /// Returns the Y pixel corresponding to a Y axis coordinate
        /// </summary>
        public double GetPixelY(double locationY)
        {
            return this.dataSize.Height - (float)((locationY - this.axes.y.min) * this.yAxisScale);
        }

        /// <summary>
        /// Returns the pixel corresponding to axis coordinates
        /// </summary>
        public PointF GetPixel(double locationX, double locationY)
        {
            return new PointF((float)this.GetPixelX(locationX), (float)this.GetPixelY(locationY));
        }

        /// <summary>
        /// Returns the X axis coordinate corresponding to a X pixel on the plot
        /// </summary>
        public double GetLocationX(double pixelX)
        {
            return (pixelX * DPIScale - this.dataOrigin.X) / this.xAxisScale + this.axes.x.min;
        }

        /// <summary>
        /// Returns the Y axis coordinate corresponding to a Y pixel on the plot
        /// </summary>
        public double GetLocationY(double pixelY)
        {
            return this.axes.y.max - (pixelY * DPIScale - this.dataOrigin.Y) / this.yAxisScale;
        }

        /// <summary>
        /// Returns axis coordinates corresponding to a pixel on the plot
        /// </summary>
        public PointF GetLocation(double pixelX, double pixelY)
        {
            // Return the X/Y location corresponding to a pixel position on the figure bitmap.
            // This is useful for converting a mouse position to an X/Y coordinate.
            return new PointF((float)this.GetLocationX(pixelX), (float)this.GetLocationY(pixelY));
        }

        public int GetTotalPointCount()
        {
            int totalPointCount = 0;
            foreach (Plottable plottable in this.plottables)
                totalPointCount += plottable.GetPointCount();
            return totalPointCount;
        }
    }
}
