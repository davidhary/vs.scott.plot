﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

namespace ScottPlot
{
    public class MultiPlot
    {
        public readonly Plot[] subplots;
        public readonly int rows, cols;
        public readonly int width, height;

        private readonly Bitmap bmp;
        private readonly Graphics gfx;

        public int subplotCount { get { return this.rows * this.cols; } }
        public int subplotWidth { get { return this.width / this.cols; } }
        public int subplotHeight { get { return this.height / this.rows; } }

        public MultiPlot(int width = 800, int height = 600, int rows = 1, int cols = 1)
        {
            if (rows < 1 || cols < 1)
                throw new ArgumentException("must have at least 1 row and column");

            this.width = width;
            this.height = height;
            this.rows = rows;
            this.cols = cols;

            this.bmp = new Bitmap(width, height);
            this.gfx = Graphics.FromImage(this.bmp);

            this.subplots = new Plot[this.subplotCount];
            for (int i = 0; i < this.subplotCount; i++)
                this.subplots[i] = new Plot(this.subplotWidth, this.subplotHeight);
        }

        private void Render()
        {
            this.gfx.Clear(Color.White);
            int subplotIndex = 0;
            for (int row = 0; row < this.rows; row++)
            {
                for (int col = 0; col < this.cols; col++)
                {
                    Bitmap subplotBmp = this.subplots[subplotIndex++].GetBitmap();
                    Point pt = new Point(col * this.subplotWidth, row * this.subplotHeight);
                    this.gfx.DrawImage(subplotBmp, pt);
                }
            }
        }

        public Bitmap GetBitmap()
        {
            this.Render();
            return this.bmp;
        }

        public void SaveFig(string filePath)
        {
            filePath = System.IO.Path.GetFullPath(filePath);
            string fileFolder = System.IO.Path.GetDirectoryName(filePath);
            if (!System.IO.Directory.Exists(fileFolder))
                throw new Exception($"ERROR: folder does not exist: {fileFolder}");

            ImageFormat imageFormat;
            string extension = System.IO.Path.GetExtension(filePath).ToUpper();
            if (extension == ".JPG" || extension == ".JPEG")
                imageFormat = ImageFormat.Jpeg; // TODO: use jpgEncoder to set custom compression level
            else if (extension == ".PNG")
                imageFormat = ImageFormat.Png;
            else if (extension == ".TIF" || extension == ".TIFF")
                imageFormat = ImageFormat.Tiff;
            else if (extension == ".BMP")
                imageFormat = ImageFormat.Bmp;
            else
                throw new NotImplementedException("Extension not supported: " + extension);

            this.Render();
            this.bmp.Save(filePath, imageFormat);
        }

        public Plot GetSubplot(int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || rowIndex >= this.rows)
                throw new ArgumentException("invalid row index");

            if (columnIndex < 0 || columnIndex >= this.cols)
                throw new ArgumentException("invalid column index");

            int subplotIndex = rowIndex * this.cols + columnIndex;
            return this.subplots[subplotIndex];
        }
    }
}
