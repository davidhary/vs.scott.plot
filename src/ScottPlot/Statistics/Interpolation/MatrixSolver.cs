﻿/*
 *  Work in this file is derived from code originally written by Hans-Peter Moser:
 *  http://www.mosismath.com/Basics/Basics.html
 *  http://www.mosismath.com/Matrix_Gauss/MatrixGauss.html
 *  It is included in ScottPlot under a MIT license with permission from the original author.
 */

using System;

namespace ScottPlot.Statistics.Interpolation
{
    // Matrix equation solver using the Gaussian elimination algorithm
    public class MatrixSolver
    {
        public readonly Matrix m;
        public readonly int maxOrder;
        public bool calcError;

        public MatrixSolver(int size, Matrix mi)
        {
            this.maxOrder = size;
            this.m = mi;
        }

        void SwitchRows(int n)
        {
            double tempD;
            int i, j;
            for (i = n; i <= this.maxOrder - 2; i++)
            {
                for (j = 0; j <= this.maxOrder - 1; j++)
                {
                    tempD = this.m.a[i, j];
                    this.m.a[i, j] = this.m.a[i + 1, j];
                    this.m.a[i + 1, j] = tempD;
                }
                tempD = this.m.y[i];
                this.m.y[i] = this.m.y[i + 1];
                this.m.y[i + 1] = tempD;
            }
        }

        public bool Eliminate()
        {
            int i, k, l;
            this.calcError = false;
            for (k = 0; k <= this.maxOrder - 2; k++)
            {
                for (i = k; i <= this.maxOrder - 2; i++)
                {
                    if (Math.Abs(this.m.a[i + 1, i]) < 1e-8)
                    {
                        this.SwitchRows(i + 1);
                    }
                    if (this.m.a[i + 1, k] != 0.0)
                    {
                        for (l = k + 1; l <= this.maxOrder - 1; l++)
                        {
                            if (!this.calcError)
                            {
                                this.m.a[i + 1, l] = this.m.a[i + 1, l] * this.m.a[k, k] - this.m.a[k, l] * this.m.a[i + 1, k];
                                if (this.m.a[i + 1, l] > 10E260)
                                {
                                    this.m.a[i + 1, k] = 0;
                                    this.calcError = true;
                                }
                            }
                        }
                        this.m.y[i + 1] = this.m.y[i + 1] * this.m.a[k, k] - this.m.y[k] * this.m.a[i + 1, k];
                        this.m.a[i + 1, k] = 0;
                    }
                }
            }
            return !this.calcError;
        }

        public void Solve()
        {
            int k, l;
            for (k = this.maxOrder - 1; k >= 0; k--)
            {
                for (l = this.maxOrder - 1; l >= k; l--)
                {
                    this.m.y[k] = this.m.y[k] - this.m.x[l] * this.m.a[k, l];
                }
                if (this.m.a[k, k] != 0)
                    this.m.x[k] = this.m.y[k] / this.m.a[k, k];
                else
                    this.m.x[k] = 0;
            }
        }
    }
}
