﻿/*
 *  Work in this file is derived from code originally written by Hans-Peter Moser:
 *  http://www.mosismath.com/PeriodicSplines/PeriodicSplines.html
 *  It is included in ScottPlot under a MIT license with permission from the original author.
 */

using System;

namespace ScottPlot.Statistics.Interpolation
{
    /// <summary>
    /// The Periodic Spline is a Natural Spline whose first and last point slopes are equal
    /// </summary>
    public class PeriodicSpline : SplineInterpolator
    {
        public PeriodicSpline(double[] xs, double[] ys, int resolution = 10) : base(xs, ys, resolution)
        {
            this.m = new Matrix(this.n - 1);
            this.gauss = new MatrixSolver(this.n - 1, this.m);

            this.a = new double[this.n + 1];
            this.b = new double[this.n + 1];
            this.c = new double[this.n + 1];
            this.d = new double[this.n + 1];
            this.h = new double[this.n];

            this.CalcParameters();
            this.Integrate();
            this.Interpolate();
        }

        public void CalcParameters()
        {
            for (int i = 0; i < this.n; i++)
                this.a[i] = this.givenYs[i];

            for (int i = 0; i < this.n - 1; i++)
                this.h[i] = this.givenXs[i + 1] - this.givenXs[i];

            this.a[this.n] = this.givenYs[1];
            this.h[this.n - 1] = this.h[0];

            for (int i = 0; i < this.n - 1; i++)
                for (int k = 0; k < this.n - 1; k++)
                {
                    this.m.a[i, k] = 0.0;
                    this.m.y[i] = 0.0;
                    this.m.x[i] = 0.0;
                }

            for (int i = 0; i < this.n - 1; i++)
            {
                if (i == 0)
                {
                    this.m.a[i, 0] = 2.0 * (this.h[0] + this.h[1]);
                    this.m.a[i, 1] = this.h[1];
                }
                else
                {
                    this.m.a[i, i - 1] = this.h[i];
                    this.m.a[i, i] = 2.0 * (this.h[i] + this.h[i + 1]);
                    if (i < this.n - 2)
                        this.m.a[i, i + 1] = this.h[i + 1];
                }
                if ((this.h[i] != 0.0) && (this.h[i + 1] != 0.0))
                    this.m.y[i] = ((this.a[i + 2] - this.a[i + 1]) / this.h[i + 1] - (this.a[i + 1] - this.a[i]) / this.h[i]) * 3.0;
                else
                    this.m.y[i] = 0.0;
            }

            this.m.a[0, this.n - 2] = this.h[0];
            this.m.a[this.n - 2, 0] = this.h[0];

            if (this.gauss.Eliminate() == false)
                throw new InvalidOperationException();

            this.gauss.Solve();

            for (int i = 1; i < this.n; i++)
                this.c[i] = this.m.x[i - 1];
            this.c[0] = this.c[this.n - 1];

            for (int i = 0; i < this.n; i++)
            {
                if (this.h[i] != 0.0)
                {
                    this.d[i] = 1.0 / 3.0 / this.h[i] * (this.c[i + 1] - this.c[i]);
                    this.b[i] = 1.0 / this.h[i] * (this.a[i + 1] - this.a[i]) - this.h[i] / 3.0 * (this.c[i + 1] + 2 * this.c[i]);
                }
            }
        }
    }
}
