﻿/*
 *  Work in this file is derived from code originally written by Hans-Peter Moser:
 *  http://www.mosismath.com/AngleSplines/EndSlopeSplines.html
 *  It is included in ScottPlot under a MIT license with permission from the original author.
 */
using System;

namespace ScottPlot.Statistics.Interpolation
{
    /// <summary>
    /// The End Slope Spline is a Natural Spline whose first and last point slopes can be defined
    /// </summary>
    public class EndSlopeSpline : SplineInterpolator
    {
        public EndSlopeSpline(double[] xs, double[] ys,
            int resolution = 10, double firstSlopeDegrees = 0, double lastSlopeDegrees = 0) :
            base(xs, ys, resolution)
        {
            this.m = new Matrix(this.n);
            this.gauss = new MatrixSolver(this.n, this.m);

            this.a = new double[this.n];
            this.b = new double[this.n];
            this.c = new double[this.n];
            this.d = new double[this.n];
            this.h = new double[this.n];

            this.CalcParameters(firstSlopeDegrees, lastSlopeDegrees);
            this.Integrate();
            this.Interpolate();
        }

        public void CalcParameters(double alpha, double beta)
        {
            for (int i = 0; i < this.n; i++)
                this.a[i] = this.givenYs[i];

            for (int i = 0; i < this.n - 1; i++)
                this.h[i] = this.givenXs[i + 1] - this.givenXs[i];

            this.m.a[0, 0] = 2.0 * this.h[0];
            this.m.a[0, 1] = this.h[0];
            this.m.y[0] = 3 * ((this.a[1] - this.a[0]) / this.h[0] - Math.Tan(alpha * Math.PI / 180));

            for (int i = 0; i < this.n - 2; i++)
            {
                this.m.a[i + 1, i] = this.h[i];
                this.m.a[i + 1, i + 1] = 2.0 * (this.h[i] + this.h[i + 1]);
                if (i < this.n - 2)
                    this.m.a[i + 1, i + 2] = this.h[i + 1];

                if ((this.h[i] != 0.0) && (this.h[i + 1] != 0.0))
                    this.m.y[i + 1] = ((this.a[i + 2] - this.a[i + 1]) / this.h[i + 1] - (this.a[i + 1] - this.a[i]) / this.h[i]) * 3.0;
                else
                    this.m.y[i + 1] = 0.0;
            }

            this.m.a[this.n - 1, this.n - 2] = this.h[this.n - 2];
            this.m.a[this.n - 1, this.n - 1] = 2.0 * this.h[this.n - 2];
            this.m.y[this.n - 1] = 3.0 * (Math.Tan(beta * Math.PI / 180) - (this.a[this.n - 1] - this.a[this.n - 2]) / this.h[this.n - 2]);

            if (this.gauss.Eliminate() == false)
                throw new InvalidOperationException();

            this.gauss.Solve();

            for (int i = 0; i < this.n; i++)
            {
                this.c[i] = this.m.x[i];
            }
            for (int i = 0; i < this.n; i++)
            {
                if (this.h[i] != 0.0)
                {
                    this.d[i] = 1.0 / 3.0 / this.h[i] * (this.c[i + 1] - this.c[i]);
                    this.b[i] = 1.0 / this.h[i] * (this.a[i + 1] - this.a[i]) - this.h[i] / 3.0 * (this.c[i + 1] + 2 * this.c[i]);
                }
            }
        }
    }
}
