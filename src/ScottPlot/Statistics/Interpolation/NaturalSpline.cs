﻿/*
 *  Work in this file is derived from code originally written by Hans-Peter Moser:
 *  http://www.mosismath.com/NaturalSplines/NaturalSplines.html
 *  It is included in ScottPlot under a MIT license with permission from the original author.
 */

using System;

namespace ScottPlot.Statistics.Interpolation
{
    /// <summary>
    /// Natural Spline data interpolator
    /// </summary>
    public class NaturalSpline : SplineInterpolator
    {
        public NaturalSpline(double[] xs, double[] ys, int resolution = 10) : base(xs, ys, resolution)
        {
            this.m = new Matrix(this.n - 2);
            this.gauss = new MatrixSolver(this.n - 2, this.m);

            this.a = new double[this.n];
            this.b = new double[this.n];
            this.c = new double[this.n];
            this.d = new double[this.n];
            this.h = new double[this.n - 1];

            this.CalcParameters();
            this.Integrate();
            this.Interpolate();
        }

        public void CalcParameters()
        {
            for (int i = 0; i < this.n; i++)
                this.a[i] = this.givenYs[i];

            for (int i = 0; i < this.n - 1; i++)
                this.h[i] = this.givenXs[i + 1] - this.givenXs[i];

            for (int i = 0; i < this.n - 2; i++)
            {
                for (int k = 0; k < this.n - 2; k++)
                {
                    this.m.a[i, k] = 0.0;
                    this.m.y[i] = 0.0;
                    this.m.x[i] = 0.0;
                }
            }

            for (int i = 0; i < this.n - 2; i++)
            {
                if (i == 0)
                {
                    this.m.a[i, 0] = 2.0 * (this.h[0] + this.h[1]);
                    this.m.a[i, 1] = this.h[1];
                }
                else
                {
                    this.m.a[i, i - 1] = this.h[i];
                    this.m.a[i, i] = 2.0 * (this.h[i] + this.h[i + 1]);
                    if (i < this.n - 3)
                        this.m.a[i, i + 1] = this.h[i + 1];
                }

                if ((this.h[i] != 0.0) && (this.h[i + 1] != 0.0))
                    this.m.y[i] = ((this.a[i + 2] - this.a[i + 1]) / this.h[i + 1] - (this.a[i + 1] - this.a[i]) / this.h[i]) * 3.0;
                else
                    this.m.y[i] = 0.0;
            }

            if (this.gauss.Eliminate() == false)
                throw new InvalidOperationException("error in matrix calculation");

            this.gauss.Solve();

            this.c[0] = 0.0;
            this.c[this.n - 1] = 0.0;

            for (int i = 1; i < this.n - 1; i++)
                this.c[i] = this.m.x[i - 1];

            for (int i = 0; i < this.n - 1; i++)
            {
                if (this.h[i] != 0.0)
                {
                    this.d[i] = 1.0 / 3.0 / this.h[i] * (this.c[i + 1] - this.c[i]);
                    this.b[i] = 1.0 / this.h[i] * (this.a[i + 1] - this.a[i]) - this.h[i] / 3.0 * (this.c[i + 1] + 2 * this.c[i]);
                }
            }
        }
    }
}
