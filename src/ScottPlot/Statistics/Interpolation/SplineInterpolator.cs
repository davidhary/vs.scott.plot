﻿/*
 *  Work in this file is derived from code originally written by Hans-Peter Moser:
 *  http://www.mosismath.com/NaturalSplines/NaturalSplines.html
 *  It is included in ScottPlot under a MIT license with permission from the original author.
 */

using System;

namespace ScottPlot.Statistics.Interpolation
{
    public abstract class SplineInterpolator
    {
        public double[] givenYs, givenXs;
        public double[] interpolatedXs, interpolatedYs;

        protected Matrix m;
        protected MatrixSolver gauss;

        protected readonly int n;
        protected double[] a, b, c, d, h;

        public SplineInterpolator(double[] xs, double[] ys, int resolution = 10)
        {
            if (xs is null || ys is null)
                throw new ArgumentException("xs and ys cannot be null");

            if (xs.Length != ys.Length)
                throw new ArgumentException("xs and ys must have the same length");

            if (xs.Length < 4)
                throw new ArgumentException("xs and ys must have a length of 4 or greater");

            if (resolution < 1)
                throw new ArgumentException("resolution must be 1 or greater");

            this.givenXs = xs;
            this.givenYs = ys;
            this.n = xs.Length;

            this.interpolatedXs = new double[this.n * resolution];
            this.interpolatedYs = new double[this.n * resolution];
        }

        public void Interpolate()
        {
            int resolution = this.interpolatedXs.Length / this.n;
            for (int i = 0; i < this.h.Length; i++)
            {
                for (int k = 0; k < resolution; k++)
                {
                    double deltaX = (double)k / resolution * this.h[i];
                    double termA = this.a[i];
                    double termB = this.b[i] * deltaX;
                    double termC = this.c[i] * deltaX * deltaX;
                    double termD = this.d[i] * deltaX * deltaX * deltaX;
                    int interpolatedIndex = i * resolution + k;
                    this.interpolatedXs[interpolatedIndex] = deltaX + this.givenXs[i];
                    this.interpolatedYs[interpolatedIndex] = termA + termB + termC + termD;
                }
            }

            // After interpolation the last several values of the interpolated arrays
            // contain uninitialized data. This section identifies the values which are
            // populated with values and copies just the useful data into new arrays.
            int pointsToKeep = resolution * (this.n - 1) + 1;
            double[] interpolatedXsCopy = new double[pointsToKeep];
            double[] interpolatedYsCopy = new double[pointsToKeep];
            Array.Copy(this.interpolatedXs, 0, interpolatedXsCopy, 0, pointsToKeep - 1);
            Array.Copy(this.interpolatedYs, 0, interpolatedYsCopy, 0, pointsToKeep - 1);
            this.interpolatedXs = interpolatedXsCopy;
            this.interpolatedYs = interpolatedYsCopy;
            this.interpolatedXs[pointsToKeep - 1] = this.givenXs[this.n - 1];
            this.interpolatedYs[pointsToKeep - 1] = this.givenYs[this.n - 1];
        }

        public double Integrate()
        {
            double integral = 0;
            for (int i = 0; i < this.h.Length; i++)
            {
                double termA = this.a[i] * this.h[i];
                double termB = this.b[i] * Math.Pow(this.h[i], 2) / 2.0;
                double termC = this.c[i] * Math.Pow(this.h[i], 3) / 3.0;
                double termD = this.d[i] * Math.Pow(this.h[i], 4) / 4.0;
                integral += termA + termB + termC + termD;
            }
            return integral;
        }
    }
}
