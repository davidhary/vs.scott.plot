﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScottPlot.Statistics
{
    /// <summary>
    /// This module holds an array of values and provides popultation statistics (mean, median, standard deviation, etc)
    /// </summary>
    public class Population
    {
        public double[] values { get; private set; }
        public double[] sortedValues { get; private set; }
        public double min { get; private set; }
        public double max { get; private set; }
        public double median { get; private set; }
        public double sum { get; private set; }
        public int count { get; private set; }
        public double mean { get; private set; }
        public double stDev { get; private set; }
        public double plus3stDev { get; private set; }
        public double minus3stDev { get; private set; }
        public double plus2stDev { get; private set; }
        public double minus2stDev { get; private set; }
        public double stdErr { get; private set; }
        public double Q1 { get; private set; }
        public double Q3 { get; private set; }
        public double IQR { get; private set; }
        public double[] lowOutliers { get; private set; }
        public double[] highOutliers { get; private set; }
        public double maxNonOutlier { get; private set; }
        public double minNonOutlier { get; private set; }
        public int n { get { return this.values.Length; } }
        public double span { get { return this.sortedValues.Last() - this.sortedValues.First(); } }

        /// <summary>
        /// Generate random values with a normal distribution
        /// </summary>
        public Population(Random rand, int pointCount, double mean = .5, double stdDev = .5)
        {
            this.values = DataGen.RandomNormal(rand, pointCount, mean, stdDev);
            this.Recalculate();
        }

        /// <summary>
        /// Calculate population stats from the given array of values
        /// </summary>
        public Population(double[] values)
        {
            if (values is null)
                throw new ArgumentException("values cannot be null");

            this.values = values;
            this.Recalculate();
        }

        [Obsolete("This constructor overload is deprecated. Please remove the fullAnalysis argument.")]
        public Population(double[] values, bool fullAnalysis = true)
        {
            if (values is null)
                throw new ArgumentException("values cannot be null");

            this.values = values;
            if (fullAnalysis)
                this.Recalculate();
        }

        public void Recalculate()
        {
            this.count = this.values.Length;

            int QSize = (int)Math.Floor(this.count / 4.0);

            this.sortedValues = new double[this.count];
            Array.Copy(this.values, 0, this.sortedValues, 0, this.count);
            Array.Sort(this.sortedValues);

            this.min = this.sortedValues.First();
            this.max = this.sortedValues.Last();
            this.median = this.sortedValues[this.count / 2];

            this.Q1 = this.sortedValues[QSize];
            this.Q3 = this.sortedValues[this.sortedValues.Length - QSize - 1];
            this.IQR = this.Q3 - this.Q1;

            double lowerBoundary = this.Q1 - 1.5 * this.IQR;
            double upperBoundary = this.Q3 + 1.5 * this.IQR;

            int minNonOutlierIndex = 0;
            int maxNonOutlierIndex = 0;

            for (int i = 0; i < this.sortedValues.Length; i++)
            {
                if (this.sortedValues[i] < lowerBoundary)
                {

                }
                else
                {
                    minNonOutlierIndex = i;
                    break;
                }
            }

            for (int i = this.sortedValues.Length - 1; i >= 0; i--)
            {
                if (this.sortedValues[i] > upperBoundary)
                {

                }
                else
                {
                    maxNonOutlierIndex = i;
                    break;
                }
            }

            this.lowOutliers = new double[minNonOutlierIndex];
            this.highOutliers = new double[this.sortedValues.Length - maxNonOutlierIndex - 1];
            Array.Copy(this.sortedValues, 0, this.lowOutliers, 0, this.lowOutliers.Length);
            Array.Copy(this.sortedValues, maxNonOutlierIndex + 1, this.highOutliers, 0, this.highOutliers.Length);
            this.minNonOutlier = this.sortedValues[minNonOutlierIndex];
            this.maxNonOutlier = this.sortedValues[maxNonOutlierIndex];

            this.sum = this.values.Sum();
            this.mean = this.sum / this.count;

            double sumVariancesSquared = 0;
            for (int i = 0; i < this.values.Length; i++)
            {
                double pointVariance = Math.Abs(this.mean - this.values[i]);
                double pointVarianceSquared = Math.Pow(pointVariance, 2);
                sumVariancesSquared += pointVarianceSquared;
            }
            double meanVarianceSquared = sumVariancesSquared / this.values.Length;
            this.stDev = Math.Sqrt(meanVarianceSquared);
            this.plus2stDev = this.mean + this.stDev * 2;
            this.minus2stDev = this.mean - this.stDev * 2;
            this.plus3stDev = this.mean + this.stDev * 3;
            this.minus3stDev = this.mean - this.stDev * 3;
            this.stdErr = this.stDev / Math.Sqrt(this.count);
        }

        public double[] GetDistribution(double[] xs, bool normalize = true)
        {
            double[] ys = new double[xs.Length];
            double multiplier = (normalize) ? 1 : 1 / (this.stDev * Math.Sqrt(2 * Math.PI));
            for (int i = 0; i < xs.Length; i++)
                ys[i] = multiplier * Math.Exp(-.5 * Math.Pow((xs[i] - this.mean) / this.stDev, 2));
            return ys;
        }
    }
}
