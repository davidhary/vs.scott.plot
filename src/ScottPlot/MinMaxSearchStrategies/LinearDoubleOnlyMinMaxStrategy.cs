﻿namespace ScottPlot.MinMaxSearchStrategies
{
    public class LinearDoubleOnlyMinMaxStrategy : IMinMaxSearchStrategy<double>
    {
        double[] sourceArray;
        public double[] SourceArray
        {
            get => this.sourceArray;
            set => this.sourceArray = value;
        }

        public void MinMaxRangeQuery(int l, int r, out double lowestValue, out double highestValue)
        {
            lowestValue = this.sourceArray[l];
            highestValue = this.sourceArray[l];
            for (int i = l; i <= r; i++)
            {
                if (this.sourceArray[i] < lowestValue)
                    lowestValue = this.sourceArray[i];
                if (this.sourceArray[i] > highestValue)
                    highestValue = this.sourceArray[i];
            }
        }

        public double SourceElement(int index)
        {
            return this.sourceArray[index];
        }

        public void updateElement(int index, double newValue)
        {
            this.sourceArray[index] = newValue;
        }

        public void updateRange(int from, int to, double[] newData, int fromData = 0)
        {
            for (int i = from; i < to; i++)
            {
                this.sourceArray[i] = newData[i - from + fromData];
            }
        }
    }
}
