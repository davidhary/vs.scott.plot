﻿using System;
using ScottPlot.DataStructures;

namespace ScottPlot.MinMaxSearchStrategies
{
    public class SegmentedTreeMinMaxSearchStrategy<T> : IMinMaxSearchStrategy<T> where T : struct, IComparable
    {
        private SegmentedTree<T> segmentedTree;

        public SegmentedTreeMinMaxSearchStrategy()
        {
            this.segmentedTree = new SegmentedTree<T>();
        }

        public SegmentedTreeMinMaxSearchStrategy(T[] data) : this()
        {
            this.SourceArray = data;
        }

        public T[] SourceArray
        {
            get => this.segmentedTree.SourceArray;
            set => this.segmentedTree.SourceArray = value;
        }

        public void MinMaxRangeQuery(int l, int r, out double lowestValue, out double highestValue)
        {
            this.segmentedTree.MinMaxRangeQuery(l, r, out lowestValue, out highestValue);
        }

        public double SourceElement(int index)
        {
            return Convert.ToDouble(this.SourceArray[index]);
        }

        public void updateElement(int index, T newValue)
        {
            this.segmentedTree.updateElement(index, newValue);
        }

        public void updateRange(int from, int to, T[] newData, int fromData = 0)
        {
            this.segmentedTree.updateRange(from, to, newData, fromData);
        }
    }
}
