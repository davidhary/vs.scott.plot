﻿using System;

namespace ScottPlot.MinMaxSearchStrategies
{
    public class LinearFastDoubleMinMaxSearchStrategy<T> : LinearMinMaxSearchStrategy<T> where T : struct, IComparable
    {
        private double[] sourceArrayDouble;

        public override T[] SourceArray
        {
            get => base.SourceArray;
            set
            {
                this.sourceArrayDouble = value as double[];
                base.SourceArray = value;
            }
        }

        public override void MinMaxRangeQuery(int l, int r, out double lowestValue, out double highestValue)
        {
            if (this.sourceArrayDouble != null)
            {
                lowestValue = this.sourceArrayDouble[l];
                highestValue = this.sourceArrayDouble[l];
                for (int i = l; i <= r; i++)
                {
                    if (this.sourceArrayDouble[i] < lowestValue)
                        lowestValue = this.sourceArrayDouble[i];
                    if (this.sourceArrayDouble[i] > highestValue)
                        highestValue = this.sourceArrayDouble[i];
                }
                return;
            }
            else
            {
                base.MinMaxRangeQuery(l, r, out lowestValue, out highestValue);
            }
        }

        public override double SourceElement(int index)
        {
            if (this.sourceArrayDouble != null)
                return this.sourceArrayDouble[index];
            return Convert.ToDouble(this.SourceArray[index]);
        }
    }
}
