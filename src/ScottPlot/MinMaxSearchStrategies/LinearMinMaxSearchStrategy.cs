﻿using System;
using System.Linq.Expressions;

namespace ScottPlot.MinMaxSearchStrategies
{
    public class LinearMinMaxSearchStrategy<T> : IMinMaxSearchStrategy<T> where T : struct, IComparable
    {
        private T[] sourceArray;
        public virtual T[] SourceArray
        {
            get => this.sourceArray;
            set => this.sourceArray = value;
        }

        // precompiled lambda expressions for fast math on generic
        private static Func<T, T, bool> LessThanExp;
        private static Func<T, T, bool> GreaterThanExp;
        private void InitExp()
        {
            ParameterExpression paramA = Expression.Parameter(typeof(T), "a");
            ParameterExpression paramB = Expression.Parameter(typeof(T), "b");
            // add the parameters together
            BinaryExpression bodyLessThan = Expression.LessThan(paramA, paramB);
            BinaryExpression bodyGreaterThan = Expression.GreaterThan(paramA, paramB);
            // compile it
            LessThanExp = Expression.Lambda<Func<T, T, bool>>(bodyLessThan, paramA, paramB).Compile();
            GreaterThanExp = Expression.Lambda<Func<T, T, bool>>(bodyGreaterThan, paramA, paramB).Compile();
        }

        public LinearMinMaxSearchStrategy()
        {
            this.InitExp();
        }

        public virtual void MinMaxRangeQuery(int l, int r, out double lowestValue, out double highestValue)
        {
            T lowestValueT = this.sourceArray[l];
            T highestValueT = this.sourceArray[l];
            for (int i = l; i <= r; i++)
            {
                if (LessThanExp(this.sourceArray[i], lowestValueT))
                    lowestValueT = this.sourceArray[i];
                if (GreaterThanExp(this.sourceArray[i], highestValueT))
                    highestValueT = this.sourceArray[i];
            }
            lowestValue = Convert.ToDouble(lowestValueT);
            highestValue = Convert.ToDouble(highestValueT);
        }

        public virtual double SourceElement(int index)
        {
            return Convert.ToDouble(this.sourceArray[index]);
        }

        public void updateElement(int index, T newValue)
        {
            this.sourceArray[index] = newValue;
        }

        public void updateRange(int from, int to, T[] newData, int fromData = 0)
        {
            for (int i = from; i < to; i++)
            {
                this.sourceArray[i] = newData[i - from + fromData];
            }
        }
    }
}
