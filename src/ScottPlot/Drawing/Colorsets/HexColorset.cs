﻿using System;
using System.Drawing;

namespace ScottPlot.Drawing.Colorsets
{
    // hex colorsets store web-formatted colors (e.g., '#FFAA66') in a string array.

    public abstract class HexColorset : IColorset
    {
        public (byte r, byte g, byte b) GetRGB(int index)
        {
            index %= this.hexColors.Length;

            string hexColor = this.hexColors[index];
            if (!hexColor.StartsWith("#"))
                hexColor = "#" + hexColor;

            if (hexColor.Length != 7)
                throw new InvalidOperationException("invalid hex color string");

            Color color = ColorTranslator.FromHtml(hexColor);

            return (color.R, color.G, color.B);
        }

        public int Count() => this.hexColors.Length;

        public abstract string[] hexColors { get; }
    }
}
