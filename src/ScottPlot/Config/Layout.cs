﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottPlot.Config
{
    public class Layout
    {
        // users can configure these
        public int yLabelWidth = 20;
        public int yScaleWidth = 40;
        public int y2LabelWidth = 20;
        public int y2ScaleWidth = 40;
        public int titleHeight = 20;
        public int xLabelHeight = 20;
        public int xScaleHeight = 20;

        public bool[] displayFrameByAxis; // TODO: MOVE THIS TO ANOTHER CLASS
        public bool displayAxisFrames = true; // TODO: MOVE THIS TO ANOTHER CLASS

        public bool tighteningOccurred = false;

        public SHRect plot { get; private set; }
        public SHRect data { get; private set; }

        public SHRect yLabel { get; private set; }
        public SHRect yScale { get; private set; }
        public SHRect y2Label { get; private set; }
        public SHRect y2Scale { get; private set; }
        public SHRect title { get; private set; }
        public SHRect xLabel { get; private set; }
        public SHRect xScale { get; private set; }

        public Layout()
        {
            this.displayFrameByAxis = new bool[] { true, true, true, true };
            this.Update(640, 480);
        }

        public Layout(int width, int height)
        {
            this.Update(width, height);
        }

        public void Update(int width, int height)
        {
            this.plot = new SHRect(0, width, height, 0);

            this.title = new SHRect(this.plot);
            this.title.ShrinkTo(top: this.titleHeight);

            this.yLabel = new SHRect(this.plot);
            this.yLabel.ShrinkTo(left: this.yLabelWidth);

            this.yScale = new SHRect(this.plot);
            this.yScale.ShrinkTo(left: this.yScaleWidth);
            this.yScale.Shift(rightward: this.yLabel.Width);

            this.y2Label = new SHRect(this.plot);
            this.y2Label.ShrinkTo(right: this.y2LabelWidth);

            this.y2Scale = new SHRect(this.plot);
            this.y2Scale.ShrinkTo(right: this.y2ScaleWidth);
            this.y2Scale.Shift(rightward: -this.y2Label.Width);

            this.xLabel = new SHRect(this.plot);
            this.xLabel.ShrinkTo(bottom: this.xLabelHeight);

            this.xScale = new SHRect(this.plot);
            this.xScale.ShrinkTo(bottom: this.xScaleHeight);
            this.xScale.Shift(downward: -this.xLabel.Height);

            // the data rectangle is what is left over
            this.data = new SHRect(this.plot);
            this.data.ShrinkBy(top: this.title.Height);
            this.data.ShrinkBy(left: this.yLabel.Width + this.yScale.Width);
            this.data.ShrinkBy(right: this.y2Label.Width + this.y2Scale.Width);
            this.data.ShrinkBy(bottom: this.xLabel.Height + this.xScale.Height);

            // shrink labels and scales to match dataRect
            this.yLabel.MatchVert(this.data);
            this.yScale.MatchVert(this.data);
            this.y2Label.MatchVert(this.data);
            this.y2Scale.MatchVert(this.data);
            this.xLabel.MatchHoriz(this.data);
            this.xScale.MatchHoriz(this.data);

            // shrink the title to align it with the data area
            this.title.MatchHoriz(this.data);
        }
    }
}
