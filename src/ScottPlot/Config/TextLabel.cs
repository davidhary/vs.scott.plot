﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottPlot.Config
{
    public class TextLabel
    {

        public TextLabel(Graphics gfx = null)
        {
            this.gfx = gfx ?? Graphics.FromImage(new Bitmap(1, 1));

            // set things which can't be instantiated at the class level
            this.color = Color.Black;
            this.colorBackground = Color.Magenta;
            this.colorBorder = Color.Black;
            this._fontName = Fonts.GetDefaultFontName();
        }

        private Graphics gfx;

        public Color color;
        public Color colorBackground;
        public Color colorBorder;

        public string text = "";
        public bool visible = true;

        public float fontSize = 12;
        public bool bold = false;

        private string _fontName;
        public string fontName
        {
            get
            {
                return this._fontName;
            }
            set
            {
                this._fontName = Fonts.GetValidFontName(value);
            }
        }


        public Font font
        {
            get
            {
                FontFamily family = new FontFamily(this.fontName);
                FontStyle fontStyle = (this.bold) ? FontStyle.Bold : FontStyle.Regular;
                Font font = new Font(family, this.fontSize, fontStyle, GraphicsUnit.Pixel);
                return font;
            }
        }


        public SizeF size
        {
            get
            {
                return Drawing.GDI.MeasureString(this.gfx, this.text, this.font);
            }
        }

        public float width
        {
            get
            {
                return this.size.Width;
            }
        }

        public float height
        {
            get
            {
                return this.size.Height;
            }
        }
    }
}
