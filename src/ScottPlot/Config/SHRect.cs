﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottPlot.Config
{
    public class SHRect
    {
        public int left { get; private set; } // origin
        public int top { get; private set; } // origin
        public int right { get; private set; }
        public int bottom { get; private set; }

        public int Width { get { return this.right - this.left; } }
        public int Height { get { return this.bottom - this.top; } }
        public Point Location { get { return new Point(this.left, this.top); } }
        public Point Center { get { return new Point(this.CenterX, this.CenterY); } }
        public int CenterX { get { return (int)((this.right + this.left) / 2); } }
        public int CenterY { get { return (int)((this.bottom + this.top) / 2); } }
        public Size Size { get { return new Size(this.Width, this.Height); } }
        public Rectangle Rectangle { get { return new Rectangle(this.Location, this.Size); } }
        public bool isValid { get { return (this.left < this.right) && (this.top < this.bottom); } }

        public SHRect(int left, int right, int bottom, int top)
        {
            this.left = left;
            this.right = right;
            this.bottom = bottom;
            this.top = top;
        }

        public SHRect(SHRect rect)
        {
            this.left = rect.left;
            this.right = rect.right;
            this.bottom = rect.bottom;
            this.top = rect.top;
        }

        public override string ToString()
        {
            string validString = (this.isValid) ? "valid" : "invalid";
            return $"{validString} rectangle [{this.Width}, {this.Height}] at ({this.left}, {this.top})";
        }

        public void ShrinkTo(int? left = null, int? right = null, int? bottom = null, int? top = null)
        {
            if (top != null)
                this.bottom = this.top + (int)top;
            if (bottom != null)
                this.top = this.bottom - (int)bottom;
            if (left != null)
                this.right = this.left + (int)left;
            if (right != null)
                this.left = this.right - (int)right;
        }

        public void ShrinkBy(int? left = null, int? right = null, int? bottom = null, int? top = null)
        {
            if (top != null)
                this.top += (int)top;
            if (bottom != null)
                this.bottom -= (int)bottom;
            if (left != null)
                this.left += (int)left;
            if (right != null)
                this.right -= (int)right;
        }

        public void Shift(int rightward = 0, int downward = 0)
        {
            this.left += rightward;
            this.right += rightward;
            this.top += downward;
            this.bottom += downward;
        }

        public void MatchVert(SHRect source)
        {
            this.top = source.top;
            this.bottom = source.bottom;
        }

        public void MatchHoriz(SHRect source)
        {
            this.left = source.left;
            this.right = source.right;
        }
    }
}
