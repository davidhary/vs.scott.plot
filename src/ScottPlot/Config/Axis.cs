﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottPlot.Config
{
    public class Axis
    {
        public bool hasBeenSet = false;
        public double boundMin = double.NegativeInfinity;
        public double boundMax = double.PositiveInfinity;

        private double _min;
        public double min
        {
            get
            {
                return this._min;
            }
            set
            {
                this._min = value;
                this.hasBeenSet = true;
            }
        }

        private double _max;
        public double max
        {
            get
            {
                return this._max;
            }
            set
            {
                this._max = value;
                this.hasBeenSet = true;
            }
        }

        public double span
        {
            get
            {
                return this.max - this.min;
            }
        }

        public double center
        {
            get
            {
                return (this.max + this.min) / 2;
            }
        }

        public void Pan(double delta)
        {
            if (delta == 0)
                return;

            if ((delta < 0) && (this.min + delta < this.boundMin))
            {
                var originalSpan = this.span;
                this.min = this.boundMin;
                this.max = this.min + originalSpan;
                return;
            }

            if ((delta > 0) && (this.max + delta > this.boundMax))
            {
                var originalSpan = this.span;
                this.max = this.boundMax;
                this.min = this.max - originalSpan;
                return;
            }

            this.min += delta;
            this.max += delta;
        }

        public void Zoom(double frac = 1, double? zoomTo = null)
        {
            zoomTo = zoomTo ?? this.center;
            double spanLeft = (double)zoomTo - this.min;
            double spanRight = this.max - (double)zoomTo;
            this.min = (double)zoomTo - spanLeft / frac;
            this.max = (double)zoomTo + spanRight / frac;
            this.ApplyBounds();
        }

        public override string ToString()
        {
            return $"axis [{this.min} to {this.max}]";
        }

        public void ApplyBounds()
        {
            this.min = Math.Max(this.min, this.boundMin);
            this.max = Math.Min(this.max, this.boundMax);
        }
    }
}
