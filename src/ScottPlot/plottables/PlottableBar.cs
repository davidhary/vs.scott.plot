﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottableBar : Plottable
    {
        public double[] xs;
        public double[] ys;
        public double[] yErr;
        public double xOffset;

        public LineStyle lineStyle;
        public Color fillColor;
        public Color negativeColor;
        public double[] yOffsets;
        public string label;

        private double errorCapSize;

        private double barWidth;
        private double valueBase = 0;

        public bool fill;
        private Brush fillBrush;
        private Pen errorPen;
        private Pen outlinePen;

        public bool verticalBars;
        public bool showValues;

        public Font valueTextFont;
        public Brush valueTextBrush;

        public PlottableBar(double[] xs, double[] ys, string label,
            double barWidth, double xOffset,
            bool fill, Color fillColor,
            double outlineWidth, Color outlineColor,
            double[] yErr, double errorLineWidth, double errorCapSize, Color errorColor,
            bool horizontal, bool showValues, Color valueColor, double[] yOffsets, Color negativeColor
            )
        {
            if (ys is null || ys.Length == 0)
                throw new ArgumentException("ys must contain data values");

            if (xs is null)
                xs = DataGen.Consecutive(ys.Length);

            if (xs.Length != ys.Length)
                throw new ArgumentException("xs and ys must have same number of elements");

            if (yErr is null)
                yErr = DataGen.Zeros(ys.Length);

            if (yErr.Length != ys.Length)
                throw new ArgumentException("yErr and ys must have same number of elements");

            if (yOffsets is null)
                yOffsets = DataGen.Zeros(ys.Length);


            this.xs = xs;
            this.ys = ys;
            this.yErr = yErr;
            this.xOffset = xOffset;
            this.label = label;
            this.verticalBars = !horizontal;
            this.showValues = showValues;

            this.barWidth = barWidth;
            this.errorCapSize = errorCapSize;

            this.fill = fill;
            this.fillColor = fillColor;
            this.negativeColor = negativeColor;

            this.yOffsets = yOffsets;

            this.fillBrush = new SolidBrush(fillColor);
            this.outlinePen = new Pen(outlineColor, (float)outlineWidth);
            this.errorPen = new Pen(errorColor, (float)errorLineWidth);

            this.valueTextFont = new Font(Fonts.GetDefaultFontName(), 12);
            this.valueTextBrush = new SolidBrush(valueColor);
        }

        public override AxisLimits2D GetLimits()
        {
            double valueMin = double.PositiveInfinity;
            double valueMax = double.NegativeInfinity;
            double positionMin = double.PositiveInfinity;
            double positionMax = double.NegativeInfinity;

            for (int i = 0; i < this.xs.Length; i++)
            {
                valueMin = Math.Min(valueMin, this.ys[i] - this.yErr[i] + this.yOffsets[i]);
                valueMax = Math.Max(valueMax, this.ys[i] + this.yErr[i] + this.yOffsets[i]);
                positionMin = Math.Min(positionMin, this.xs[i]);
                positionMax = Math.Max(positionMax, this.xs[i]);
            }

            valueMin = Math.Min(valueMin, this.valueBase);
            valueMax = Math.Max(valueMax, this.valueBase);

            if (this.showValues)
                valueMax += (valueMax - valueMin) * .1; // increase by 10% to accomodate label

            positionMin -= this.barWidth / 2;
            positionMax += this.barWidth / 2;

            positionMin += this.xOffset;
            positionMax += this.xOffset;

            if (this.verticalBars)
                return new AxisLimits2D(positionMin, positionMax, valueMin, valueMax);
            else
                return new AxisLimits2D(valueMin, valueMax, positionMin, positionMax);
        }

        public override void Render(Settings settings)
        {
            for (int i = 0; i < this.ys.Length; i++)
            {
                if (this.verticalBars)
                    this.RenderBarVertical(settings, this.xs[i] + this.xOffset, this.ys[i], this.yErr[i], this.yOffsets[i]);
                else
                    this.RenderBarHorizontal(settings, this.xs[i] + this.xOffset, this.ys[i], this.yErr[i], this.yOffsets[i]);
            }
        }

        private void RenderBarVertical(Settings settings, double position, double value, double valueError, double yOffset)
        {
            float centerPx = (float)settings.GetPixelX(position);
            double edge1 = position - this.barWidth / 2;
            double value1 = Math.Min(this.valueBase, value) + yOffset;
            double value2 = Math.Max(this.valueBase, value) + yOffset;
            double valueSpan = value2 - value1;

            ((SolidBrush)this.fillBrush).Color = (value < 0) ? this.negativeColor : this.fillColor;

            var rect = new RectangleF(
                x: (float)settings.GetPixelX(edge1),
                y: (float)settings.GetPixelY(value2),
                width: (float)(this.barWidth * settings.xAxisScale),
                height: (float)(valueSpan * settings.yAxisScale));

            settings.gfxData.FillRectangle(this.fillBrush, rect.X, rect.Y, rect.Width, rect.Height);

            if (this.outlinePen.Width > 0)
                settings.gfxData.DrawRectangle(this.outlinePen, rect.X, rect.Y, rect.Width, rect.Height);

            if (this.errorPen.Width > 0 && valueError > 0)
            {
                double error1 = value > 0 ? value2 - Math.Abs(valueError) : value1 - Math.Abs(valueError);
                double error2 = value > 0 ? value2 + Math.Abs(valueError) : value1 + Math.Abs(valueError);

                float capPx1 = (float)settings.GetPixelX(position - this.errorCapSize * this.barWidth / 2);
                float capPx2 = (float)settings.GetPixelX(position + this.errorCapSize * this.barWidth / 2);
                float errorPx2 = (float)settings.GetPixelY(error2);
                float errorPx1 = (float)settings.GetPixelY(error1);

                settings.gfxData.DrawLine(this.errorPen, centerPx, errorPx1, centerPx, errorPx2);
                settings.gfxData.DrawLine(this.errorPen, capPx1, errorPx1, capPx2, errorPx1);
                settings.gfxData.DrawLine(this.errorPen, capPx1, errorPx2, capPx2, errorPx2);
            }

            if (this.showValues)
                settings.gfxData.DrawString(value.ToString(), this.valueTextFont, this.valueTextBrush, centerPx, rect.Y, settings.misc.sfSouth);
        }

        private void RenderBarHorizontal(Settings settings, double position, double value, double valueError, double yOffset)
        {
            float centerPx = (float)settings.GetPixelY(position);
            double edge2 = position + this.barWidth / 2;
            double value1 = Math.Min(this.valueBase, value) + yOffset;
            double value2 = Math.Max(this.valueBase, value) + yOffset;
            double valueSpan = value2 - value1;

            ((SolidBrush)this.fillBrush).Color = (value < 0) ? this.negativeColor : this.fillColor;

            var rect = new RectangleF(
                x: (float)settings.GetPixelX(value1),
                y: (float)settings.GetPixelY(edge2),
                height: (float)(this.barWidth * settings.yAxisScale),
                width: (float)(valueSpan * settings.xAxisScale));

            settings.gfxData.FillRectangle(this.fillBrush, rect.X, rect.Y, rect.Width, rect.Height);

            if (this.outlinePen.Width > 0)
                settings.gfxData.DrawRectangle(this.outlinePen, rect.X, rect.Y, rect.Width, rect.Height);

            if (this.errorPen.Width > 0 && valueError > 0)
            {
                double error1 = value > 0 ? value2 - Math.Abs(valueError) : value1 - Math.Abs(valueError);
                double error2 = value > 0 ? value2 + Math.Abs(valueError) : value1 + Math.Abs(valueError);

                float capPx1 = (float)settings.GetPixelY(position - this.errorCapSize * this.barWidth / 2);
                float capPx2 = (float)settings.GetPixelY(position + this.errorCapSize * this.barWidth / 2);
                float errorPx2 = (float)settings.GetPixelX(error2);
                float errorPx1 = (float)settings.GetPixelX(error1);

                settings.gfxData.DrawLine(this.errorPen, errorPx1, centerPx, errorPx2, centerPx);
                settings.gfxData.DrawLine(this.errorPen, errorPx1, capPx2, errorPx1, capPx1);
                settings.gfxData.DrawLine(this.errorPen, errorPx2, capPx2, errorPx2, capPx1);
            }

            if (this.showValues)
                settings.gfxData.DrawString(value.ToString(), this.valueTextFont, this.valueTextBrush, rect.X + rect.Width, centerPx, settings.misc.sfWest);
        }

        public override string ToString()
        {
            string label = string.IsNullOrWhiteSpace(this.label) ? "" : $" ({this.label})";
            return $"PlottableBar{label} with {this.GetPointCount()} points";
        }

        public override int GetPointCount()
        {
            return this.ys.Length;
        }

        public override LegendItem[] GetLegendItems()
        {
            var singleLegendItem = new LegendItem(this.label, this.fillColor, lineWidth: 10, markerShape: MarkerShape.none);
            return new LegendItem[] { singleLegendItem };
        }
    }
}
