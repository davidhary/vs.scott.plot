﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using ScottPlot.Config;
using ScottPlot.Drawing;

namespace ScottPlot
{
    public class PlottableErrorBars : Plottable
    {
        private readonly double[] xs;
        private readonly double[] ys;
        private readonly double[] xPositiveError;
        private readonly double[] xNegativeError;
        private readonly double[] yPositiveError;
        private readonly double[] yNegativeError;
        private readonly float capSize;
        private readonly Pen penLine;

        public string label;
        public Color color;
        public LineStyle lineStyle = LineStyle.Solid;

        public PlottableErrorBars(double[] xs, double[] ys, double[] xPositiveError, double[] xNegativeError,
            double[] yPositiveError, double[] yNegativeError, Color color, double lineWidth, double capSize, string label)
        {
            //check input
            if (xs.Length != ys.Length)
                throw new ArgumentException("X and Y arrays must have the same length");

            //save properties
            this.xs = xs;
            this.ys = ys;
            this.xPositiveError = this.SanitizeErrors(xPositiveError, xs.Length);
            this.xNegativeError = this.SanitizeErrors(xNegativeError, xs.Length);
            this.yPositiveError = this.SanitizeErrors(yPositiveError, xs.Length);
            this.yNegativeError = this.SanitizeErrors(yNegativeError, xs.Length);
            this.capSize = (float)capSize;
            this.color = color;
            this.label = label;

            this.penLine = GDI.Pen(this.color, (float)lineWidth, this.lineStyle, true);
        }

        private double[] SanitizeErrors(double[] errorArray, int expectedLength)
        {
            if (errorArray is null)
                return null;

            if (errorArray.Length != expectedLength)
                throw new ArgumentException("Point arrays and error arrays must have the same length");

            for (int i = 0; i < errorArray.Length; i++)
                if (errorArray[i] < 0)
                    errorArray[i] = -errorArray[i];

            return errorArray;
        }

        public override string ToString()
        {
            string label = string.IsNullOrWhiteSpace(this.label) ? "" : $" ({this.label})";
            return $"PlottableErrorBars{label} with {this.GetPointCount()} points";
        }

        public override Config.AxisLimits2D GetLimits()
        {
            double xMin = double.PositiveInfinity;
            double yMin = double.PositiveInfinity;
            double xMax = double.NegativeInfinity;
            double yMax = double.NegativeInfinity;

            if (this.xNegativeError is null)
            {
                xMin = this.xs.Min();
            }
            else
            {
                for (int i = 0; i < this.xs.Length; i++)
                    xMin = Math.Min(xMin, this.xs[i] - this.xNegativeError[i]);
            }

            if (this.xPositiveError is null)
            {
                xMax = this.xs.Max();
            }
            else
            {
                for (int i = 0; i < this.xs.Length; i++)
                    xMax = Math.Max(xMax, this.xs[i] + this.xPositiveError[i]);
            }

            if (this.yNegativeError is null)
            {
                yMin = this.ys.Min();
            }
            else
            {
                for (int i = 0; i < this.xs.Length; i++)
                    yMin = Math.Min(yMin, this.ys[i] - this.yNegativeError[i]);
            }

            if (this.yPositiveError is null)
            {
                yMax = this.ys.Max();
            }
            else
            {
                for (int i = 0; i < this.xs.Length; i++)
                    yMax = Math.Max(yMax, this.ys[i] + this.yPositiveError[i]);
            }

            return new Config.AxisLimits2D(new double[] { xMin, xMax, yMin, yMax });
        }


        public override void Render(Settings settings)
        {
            this.DrawErrorBar(settings, this.xPositiveError, true, true);
            this.DrawErrorBar(settings, this.xNegativeError, true, false);
            this.DrawErrorBar(settings, this.yPositiveError, false, true);
            this.DrawErrorBar(settings, this.yNegativeError, false, false);
        }

        public void DrawErrorBar(Settings settings, double[] errorArray, bool xError, bool positiveError)
        {
            if (errorArray is null)
                return;

            float slightPixelOffset = 0.01f; // to fix GDI bug that happens when small straight lines are drawn with anti-aliasing on

            for (int i = 0; i < this.xs.Length; i++)
            {
                PointF centerPixel = settings.GetPixel(this.xs[i], this.ys[i]);
                float errorSize = positiveError ? (float)errorArray[i] : -(float)errorArray[i];
                if (xError)
                {
                    float xWithError = (float)settings.GetPixelX(this.xs[i] + errorSize);
                    settings.gfxData.DrawLine(this.penLine, centerPixel.X, centerPixel.Y, xWithError, centerPixel.Y);
                    settings.gfxData.DrawLine(this.penLine, xWithError, centerPixel.Y - this.capSize, xWithError + slightPixelOffset, centerPixel.Y + this.capSize);
                }
                else
                {
                    float yWithError = (float)settings.GetPixelY(this.ys[i] + errorSize);
                    settings.gfxData.DrawLine(this.penLine, centerPixel.X, centerPixel.Y, centerPixel.X, yWithError);
                    settings.gfxData.DrawLine(this.penLine, centerPixel.X - this.capSize, yWithError, centerPixel.X + this.capSize, yWithError + slightPixelOffset);
                }
            }
        }

        public override int GetPointCount()
        {
            return this.ys.Length;
        }

        public override LegendItem[] GetLegendItems()
        {
            var singleLegendItem = new Config.LegendItem(this.label, this.color, markerShape: MarkerShape.none);
            return new LegendItem[] { singleLegendItem };
        }
    }
}
