﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottPlot
{
    public class OHLC
    {
        public double open;
        public double high;
        public double low;
        public double close;
        public double time;
        public double timeSpan = 1;

        public double highestOpenClose;
        public double lowestOpenClose;
        public bool closedHigher;

        public OHLC(double open, double high, double low, double close, DateTime dateTime, double timeSpan = 1)
        {
            this.open = open;
            this.high = high;
            this.low = low;
            this.close = close;
            this.time = dateTime.ToOADate();
            this.timeSpan = timeSpan;

            this.highestOpenClose = Math.Max(open, close);
            this.lowestOpenClose = Math.Min(open, close);
            this.closedHigher = (close > open) ? true : false;
        }

        public OHLC(double open, double high, double low, double close, double time, double timeSpan = 1)
        {
            this.open = open;
            this.high = high;
            this.low = low;
            this.close = close;
            this.time = time;
            this.timeSpan = timeSpan;

            this.highestOpenClose = Math.Max(open, close);
            this.lowestOpenClose = Math.Min(open, close);
            this.closedHigher = (close > open) ? true : false;
        }

        public override string ToString()
        {
            return $"OHLC: open={this.open}, high={this.high}, low={this.low}, close={this.close}, timestamp={this.time}";
        }
    }
}
