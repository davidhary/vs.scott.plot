﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottablePie : Plottable
    {
        public double[] values;
        public string label;
        public string[] groupNames;
        public Color[] colors;
        bool explodedChart;
        bool showValues;
        bool showPercentages;
        bool showLabels;

        private SolidBrush brush = new SolidBrush(Color.Black);
        private Pen pen = new Pen(Color.Black);

        public PlottablePie(double[] values, string[] groupNames, Color[] colors, bool explodedChart, bool showValues, bool showPercentages, bool showLabels, string label)
        {
            this.values = values;
            this.label = label;
            this.groupNames = groupNames;
            this.colors = colors;
            this.explodedChart = explodedChart;
            this.showValues = showValues;
            this.showPercentages = showPercentages;
            this.showLabels = (groupNames is null) ? false : showLabels;
        }

        public override LegendItem[] GetLegendItems()
        {
            if (this.groupNames is null)
                return null;

            var items = new LegendItem[this.values.Length];
            for (int i = 0; i < this.values.Length; i++)
            {
                items[i] = new LegendItem(this.groupNames[i], this.colors[i], lineWidth: 10);
            }
            return items;
        }

        public override AxisLimits2D GetLimits()
        {
            return new AxisLimits2D(-0.5, 0.5, -1, 1);
        }

        public override int GetPointCount()
        {
            return this.values.Length;
        }

        public override void Render(Settings settings)
        {
            double[] proportions = this.values.Select(x => x / this.values.Sum()).ToArray();

            int outlineWidth = 1;
            int sliceOutlineWidth = 0;
            if (this.explodedChart)
            {
                this.pen.Color = settings.DataBackground.Color; // TODO: will fail if data background is transparent
                outlineWidth = 20;
                sliceOutlineWidth = 1;
            }

            AxisLimits2D limits = this.GetLimits();
            double centreX = limits.xCenter;
            double centreY = limits.yCenter;
            float diameterPixels = .9f * Math.Min(settings.dataSize.Width, settings.dataSize.Height);
            string fontName = Config.Fonts.GetSansFontName();
            float fontSize = 12;

            // record label details and draw them after slices to prevent cover-ups
            double[] labelXs = new double[this.values.Length];
            double[] labelYs = new double[this.values.Length];
            string[] labelStrings = new string[this.values.Length];

            RectangleF boundingRectangle = new RectangleF((float)settings.GetPixelX(centreX) - diameterPixels / 2, (float)settings.GetPixelY(centreY) - diameterPixels / 2, diameterPixels, diameterPixels);

            double start = -90;
            for (int i = 0; i < this.values.Length; i++)
            {
                // determine where the slice is to be drawn
                double sweep = proportions[i] * 360;
                double sweepOffset = this.explodedChart ? -1 : 0;
                double angle = (Math.PI / 180) * ((sweep + 2 * start) / 2);
                double xOffset = this.explodedChart ? 3 * Math.Cos(angle) : 0;
                double yOffset = this.explodedChart ? 3 * Math.Sin(angle) : 0;

                // record where and what to label the slice
                double sliceLabelR = 0.35 * diameterPixels;
                labelXs[i] = (boundingRectangle.X + diameterPixels / 2 + xOffset + Math.Cos(angle) * sliceLabelR);
                labelYs[i] = (boundingRectangle.Y + diameterPixels / 2 + yOffset + Math.Sin(angle) * sliceLabelR);
                string sliceLabelValue = (this.showValues) ? $"{this.values[i]}" : "";
                string sliceLabelPercentage = (this.showPercentages) ? $"{proportions[i] * 100:f1}%" : "";
                string sliceLabelName = (this.showLabels) ? this.groupNames[i] : "";
                labelStrings[i] = $"{sliceLabelValue}\n{sliceLabelPercentage}\n{sliceLabelName}".Trim();

                this.brush.Color = this.colors[i];
                settings.gfxData.FillPie(this.brush, (int)(boundingRectangle.X + xOffset), (int)(boundingRectangle.Y + yOffset), boundingRectangle.Width, boundingRectangle.Height, (float)start, (float)(sweep + sweepOffset));

                if (this.explodedChart)
                {
                    this.pen.Width = sliceOutlineWidth;
                    settings.gfxData.DrawPie(this.pen, (int)(boundingRectangle.X + xOffset), (int)(boundingRectangle.Y + yOffset), boundingRectangle.Width, boundingRectangle.Height, (float)start, (float)(sweep + sweepOffset));
                }
                start += sweep;

            }

            this.brush.Color = Color.White;
            var font = new Font(fontName, fontSize);
            for (int i = 0; i < this.values.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(labelStrings[i]))
                {
                    settings.gfxData.DrawString(labelStrings[i], font, this.brush,
                        (float)labelXs[i], (float)labelYs[i], settings.misc.sfCenterCenter);
                }
            }

            this.pen.Width = outlineWidth;
            settings.gfxData.DrawEllipse(this.pen, boundingRectangle.X, boundingRectangle.Y, boundingRectangle.Width, boundingRectangle.Height);
        }

        public override string ToString()
        {
            string label = string.IsNullOrWhiteSpace(this.label) ? "" : $" ({this.label})";
            return $"PlottablePie{label} with {this.GetPointCount()} points";
        }
    }
}
