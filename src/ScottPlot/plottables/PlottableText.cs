﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottableText : Plottable
    {
        public double x;
        public double y;
        public double rotation;
        public string text;
        public Brush brush, frameBrush;
        public Font font;
        public TextAlignment alignment;
        public bool frame;
        public Color frameColor;
        public string label;

        public PlottableText(string text, double x, double y, Color color, string fontName, double fontSize, bool bold, string label, TextAlignment alignment, double rotation, bool frame, Color frameColor)
        {
            this.text = text ?? throw new Exception("Text cannot be null");
            this.x = x;
            this.y = y;
            this.rotation = rotation;
            this.label = label;
            this.alignment = alignment;
            this.frame = frame;
            this.frameColor = frameColor;

            this.brush = new SolidBrush(color);
            this.frameBrush = new SolidBrush(frameColor);

            FontStyle fontStyle = (bold == true) ? FontStyle.Bold : FontStyle.Regular;
            this.font = new Font(fontName, (float)fontSize, fontStyle, GraphicsUnit.Pixel);
        }

        public override string ToString()
        {
            return $"PlottableText \"{this.text}\" at ({this.x}, {this.y})";
        }

        public override Config.AxisLimits2D GetLimits()
        {
            double[] limits = { this.x, this.x, this.y, this.y };

            // TODO: use features of 2d axis
            return new Config.AxisLimits2D(limits);
        }

        public override void Render(Settings settings)
        {

            PointF defaultPoint = settings.GetPixel(this.x, this.y);
            PointF textLocationPoint = new PointF();
            SizeF stringSize = Drawing.GDI.MeasureString(settings.gfxData, this.text, this.font);

            if (this.rotation == 0)
            {
                switch (this.alignment)
                {
                    case TextAlignment.lowerCenter:
                        textLocationPoint.Y = defaultPoint.Y - stringSize.Height;
                        textLocationPoint.X = defaultPoint.X - stringSize.Width / 2;
                        break;
                    case TextAlignment.lowerLeft:
                        textLocationPoint.Y = defaultPoint.Y - stringSize.Height;
                        textLocationPoint.X = defaultPoint.X;
                        break;
                    case TextAlignment.lowerRight:
                        textLocationPoint.Y = defaultPoint.Y - stringSize.Height;
                        textLocationPoint.X = defaultPoint.X - stringSize.Width;
                        break;
                    case TextAlignment.middleLeft:
                        textLocationPoint.Y = defaultPoint.Y - stringSize.Height / 2;
                        textLocationPoint.X = defaultPoint.X;
                        break;
                    case TextAlignment.middleRight:
                        textLocationPoint.Y = defaultPoint.Y - stringSize.Height / 2;
                        textLocationPoint.X = defaultPoint.X - stringSize.Width;
                        break;
                    case TextAlignment.upperCenter:
                        textLocationPoint.Y = defaultPoint.Y;
                        textLocationPoint.X = defaultPoint.X - stringSize.Width / 2;
                        break;
                    case TextAlignment.upperLeft:
                        textLocationPoint = defaultPoint;
                        break;
                    case TextAlignment.upperRight:
                        textLocationPoint.Y = defaultPoint.Y;
                        textLocationPoint.X = defaultPoint.X - stringSize.Width;
                        break;
                    case TextAlignment.middleCenter:
                        textLocationPoint.Y = defaultPoint.Y - stringSize.Height / 2;
                        textLocationPoint.X = defaultPoint.X - stringSize.Width / 2;
                        break;
                }
            }
            else
            {
                // ignore alignment if rotation is used
                textLocationPoint = new PointF(defaultPoint.X, defaultPoint.Y);
            }

            settings.gfxData.TranslateTransform((int)textLocationPoint.X, (int)textLocationPoint.Y);
            settings.gfxData.RotateTransform((float)this.rotation);
            if (this.frame)
            {
                Rectangle stringRect = new Rectangle(0, 0, (int)stringSize.Width, (int)stringSize.Height);
                settings.gfxData.FillRectangle(this.frameBrush, stringRect);
            }
            settings.gfxData.DrawString(this.text, this.font, this.brush, new PointF(0, 0));
            settings.gfxData.ResetTransform();
        }

        public override int GetPointCount()
        {
            return 1;
        }

        public override LegendItem[] GetLegendItems()
        {
            return null; // don't show this in the legend
        }
    }
}
