﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottableVSpan : Plottable, IDraggable
    {
        public double position1;
        public double position2;
        public Brush brush;
        public Color color;
        public string label;

        public PlottableVSpan(double position1, double position2, Color color, double alpha, string label,
            bool draggable, bool dragFixedSize, double dragLimitLower, double dragLimitUpper)
        {
            this.position1 = position1;
            this.position2 = position2;
            this.color = Color.FromArgb((int)(alpha * 255), color.R, color.G, color.B);
            this.label = label;
            this.brush = new SolidBrush(this.color);

            this.DragEnabled = draggable;
            this.DragFixedSize = dragFixedSize;

            this.SetLimits(x1: double.NegativeInfinity, x2: double.PositiveInfinity, y1: dragLimitLower, y2: dragLimitUpper);
        }

        public override string ToString()
        {
            string label = string.IsNullOrWhiteSpace(this.label) ? "" : $" ({this.label})";
            return $"PlottableVSpan{label} from Y={this.position1} to Y={this.position2}";
        }

        public override AxisLimits2D GetLimits()
        {
            // TODO: use real numbers (and double.NaN)
            return new AxisLimits2D();
        }

        public override void Render(Settings settings)
        {
            PointF topLeft, lowerRight;

            double positionMin = Math.Min(this.position1, this.position2);
            double positionMax = Math.Max(this.position1, this.position2);

            topLeft = settings.GetPixel(settings.axes.x.min, positionMin);
            lowerRight = settings.GetPixel(settings.axes.x.max, positionMax);
            if (topLeft.Y > settings.bmpData.Height)
                topLeft.Y = settings.bmpData.Height;
            if (lowerRight.Y < 0)
                lowerRight.Y = 0;

            float width = lowerRight.X - topLeft.X + 1;
            float height = topLeft.Y - lowerRight.Y + 1;
            float x = topLeft.X - 1;
            float y = lowerRight.Y - 1;

            settings.gfxData.FillRectangle(this.brush, x, y, width, height);
        }

        public bool DragEnabled { get; set; }
        public bool DragFixedSize { get; set; }

        private double dragLimitY1 = double.NegativeInfinity;
        private double dragLimitY2 = double.PositiveInfinity;
        public void SetLimits(double? x1, double? x2, double? y1, double? y2)
        {
            if (y1 != null) this.dragLimitY1 = (double)y1;
            if (y2 != null) this.dragLimitY2 = (double)y2;
        }

        private enum Edge { Edge1, Edge2, Neither };
        Edge edgeUnderMouse = Edge.Neither;
        public bool IsUnderMouse(double coordinateX, double coordinateY, double snapX, double snapY)
        {
            if (Math.Abs(this.position1 - coordinateY) <= snapY)
                this.edgeUnderMouse = Edge.Edge1;
            else if (Math.Abs(this.position2 - coordinateY) <= snapY)
                this.edgeUnderMouse = Edge.Edge2;
            else
                this.edgeUnderMouse = Edge.Neither;

            return (this.edgeUnderMouse == Edge.Neither) ? false : true;
        }

        public void DragTo(double coordinateX, double coordinateY, bool isShiftDown = false, bool isAltDown = false, bool isCtrlDown = false)
        {
            if (this.DragEnabled)
            {
                if (coordinateY < this.dragLimitY1) coordinateY = this.dragLimitY1;
                if (coordinateY > this.dragLimitY2) coordinateY = this.dragLimitY2;

                double sizeBeforeDrag = this.position2 - this.position1;
                if (this.edgeUnderMouse == Edge.Edge1)
                {
                    this.position1 = coordinateY;
                    if (this.DragFixedSize || isShiftDown)
                        this.position2 = this.position1 + sizeBeforeDrag;
                }
                else if (this.edgeUnderMouse == Edge.Edge2)
                {
                    this.position2 = coordinateY;
                    if (this.DragFixedSize || isShiftDown)
                        this.position1 = this.position2 - sizeBeforeDrag;
                }
                else
                {
                    Debug.WriteLine("DragTo() called but no side selected. Call IsUnderMouse() to select a side.");
                }
            }
        }

        public override int GetPointCount()
        {
            return 1;
        }

        public Cursor DragCursor => Cursor.NS;

        public override LegendItem[] GetLegendItems()
        {
            var singleLegendItem = new Config.LegendItem(this.label, this.color, markerSize: 0, lineWidth: 10);
            return new LegendItem[] { singleLegendItem };
        }
    }
}
