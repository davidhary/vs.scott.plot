﻿using ScottPlot.Config;
using ScottPlot.Drawing;
using ScottPlot.MinMaxSearchStrategies;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ScottPlot
{
    public class PlottableSignalBase<T> : Plottable, IExportable where T : struct, IComparable
    {
        protected IMinMaxSearchStrategy<T> minmaxSearchStrategy;
        // Any changes must be sync with PlottableSignal
        public T[] ys;
        public double sampleRate;
        public double samplePeriod;
        public float markerSize;
        public double xOffset;
        public double yOffset;
        public double lineWidth;
        public Pen penHD;
        public Pen penLD;
        public Brush brush;
        public int minRenderIndex;
        public int maxRenderIndex;
        private Pen[] penByDensity;
        private int densityLevelCount = 0;
        public string label;
        public Color color;
        public LineStyle lineStyle;
        public bool useParallel = true;

        public bool TreesReady => true;
        public PlottableSignalBase(T[] ys, double sampleRate, double xOffset, double yOffset, Color color,
            double lineWidth, double markerSize, string label, Color[] colorByDensity,
            int minRenderIndex, int maxRenderIndex, LineStyle lineStyle, bool useParallel,
            IMinMaxSearchStrategy<T> minMaxSearchStrategy = null)
        {
            if (ys == null)
                throw new Exception("Y data cannot be null");

            this.ys = ys;
            this.sampleRate = sampleRate;
            this.samplePeriod = 1.0 / sampleRate;
            this.markerSize = (float)markerSize;
            this.xOffset = xOffset;
            this.label = label;
            this.color = color;
            this.lineWidth = lineWidth;
            this.yOffset = yOffset;
            if (minRenderIndex < 0 || minRenderIndex > maxRenderIndex)
                throw new ArgumentException("minRenderIndex must be between 0 and maxRenderIndex");
            this.minRenderIndex = minRenderIndex;
            if ((maxRenderIndex > ys.Length - 1) || maxRenderIndex < 0)
                throw new ArgumentException("maxRenderIndex must be a valid index for ys[]");
            this.maxRenderIndex = maxRenderIndex;
            this.lineStyle = lineStyle;
            this.useParallel = useParallel;
            this.brush = new SolidBrush(color);
            this.penLD = GDI.Pen(color, (float)lineWidth, lineStyle, true);
            this.penHD = GDI.Pen(color, (float)lineWidth, LineStyle.Solid, true);

            if (colorByDensity != null)
            {
                // turn the ramp into a pen triangle
                this.densityLevelCount = colorByDensity.Length * 2 - 1;
                this.penByDensity = new Pen[this.densityLevelCount];
                for (int i = 0; i < colorByDensity.Length; i++)
                {
                    this.penByDensity[i] = new Pen(colorByDensity[i]);
                    this.penByDensity[this.densityLevelCount - 1 - i] = new Pen(colorByDensity[i]);
                }
            }
            if (minMaxSearchStrategy == null)
                this.minmaxSearchStrategy = new SegmentedTreeMinMaxSearchStrategy<T>();
            else
                this.minmaxSearchStrategy = minMaxSearchStrategy;
            this.minmaxSearchStrategy.SourceArray = ys;
        }

        public void updateData(int index, T newValue)
        {
            this.minmaxSearchStrategy.updateElement(index, newValue);
        }

        public void updateData(int from, int to, T[] newData, int fromData = 0) // RangeUpdate
        {
            this.minmaxSearchStrategy.updateRange(from, to, newData, fromData);
        }

        public void updateData(int from, T[] newData)
        {
            this.updateData(from, newData.Length, newData);
        }

        public void updateData(T[] newData)
        {
            this.updateData(0, newData.Length, newData);
        }

        public override Config.AxisLimits2D GetLimits()
        {
            double[] limits = new double[4];
            limits[0] = this.minRenderIndex + this.xOffset;
            limits[1] = this.samplePeriod * this.maxRenderIndex + this.xOffset;
            this.minmaxSearchStrategy.MinMaxRangeQuery(this.minRenderIndex, this.maxRenderIndex, out limits[2], out limits[3]);
            limits[2] += this.yOffset;
            limits[3] += this.yOffset;

            // TODO: use features of 2d axis
            return new Config.AxisLimits2D(limits);
        }

        private void RenderSingleLine(Settings settings)
        {
            // this function is for when the graph is zoomed so far out its entire display is a single vertical pixel column
            double yMin, yMax;
            this.minmaxSearchStrategy.MinMaxRangeQuery(this.minRenderIndex, this.maxRenderIndex, out yMin, out yMax);
            PointF point1 = settings.GetPixel(this.xOffset, yMin + this.yOffset);
            PointF point2 = settings.GetPixel(this.xOffset, yMax + this.yOffset);
            settings.gfxData.DrawLine(this.penHD, point1, point2);
        }

        private bool markersAreVisible = false;
        private void RenderLowDensity(Settings settings, int visibleIndex1, int visibleIndex2)
        {
            // this function is for when the graph is zoomed in so individual data points can be seen

            List<PointF> linePoints = new List<PointF>(visibleIndex2 - visibleIndex1 + 2);
            if (visibleIndex2 > this.ys.Length - 2)
                visibleIndex2 = this.ys.Length - 2;
            if (visibleIndex2 > this.maxRenderIndex)
                visibleIndex2 = this.maxRenderIndex - 1;
            if (visibleIndex1 < 0)
                visibleIndex1 = 0;
            if (visibleIndex1 < this.minRenderIndex)
                visibleIndex1 = this.minRenderIndex;

            for (int i = visibleIndex1; i <= visibleIndex2 + 1; i++)
                linePoints.Add(settings.GetPixel(this.samplePeriod * i + this.xOffset, this.minmaxSearchStrategy.SourceElement(i) + this.yOffset));

            if (linePoints.Count > 1)
            {
                if (this.penLD.Width > 0)
                    settings.gfxData.DrawLines(this.penHD, linePoints.ToArray());

                if (this.markerSize > 0)
                {
                    // make markers transition away smoothly by making them smaller as the user zooms out
                    float pixelsBetweenPoints = (float)(this.samplePeriod * settings.xAxisScale);
                    float zoomTransitionScale = Math.Min(1, pixelsBetweenPoints / 10);
                    float markerPxDiameter = this.markerSize * zoomTransitionScale;
                    float markerPxRadius = markerPxDiameter / 2;
                    if (markerPxRadius > .25)
                    {
                        this.markersAreVisible = true;

                        // adjust marker offset to improve rendering on Linux and MacOS
                        float markerOffsetX = (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) ? 0 : 1;

                        foreach (PointF point in linePoints)
                            settings.gfxData.FillEllipse(brush: this.brush,
                                x: point.X - markerPxRadius + markerOffsetX, y: point.Y - markerPxRadius,
                                width: markerPxDiameter, height: markerPxDiameter);
                    }
                }
            }
        }

        private class IntervalMinMax
        {
            public float x;
            public float Min;
            public float Max;
            public IntervalMinMax(float x, float Min, float Max)
            {
                this.x = x;
                this.Min = Min;
                this.Max = Max;
            }
            public IEnumerable<PointF> GetPoints()
            {
                yield return new PointF(this.x, this.Min);
                yield return new PointF(this.x, this.Max);
            }
        }

        private IntervalMinMax CalcInterval(int xPx, double offsetPoints, double columnPointCount, Settings settings)
        {
            int index1 = (int)(offsetPoints + columnPointCount * xPx);
            int index2 = (int)(offsetPoints + columnPointCount * (xPx + 1));

            if (index1 < 0)
                index1 = 0;
            if (index1 < this.minRenderIndex)
                index1 = this.minRenderIndex;

            if (index2 > this.ys.Length - 1)
                index2 = this.ys.Length - 1;
            if (index2 > this.maxRenderIndex)
                index2 = this.maxRenderIndex;

            // get the min and max value for this column                
            this.minmaxSearchStrategy.MinMaxRangeQuery(index1, index2, out double lowestValue, out double highestValue);
            float yPxHigh = (float)settings.GetPixelY(lowestValue + this.yOffset);
            float yPxLow = (float)settings.GetPixelY(highestValue + this.yOffset);
            return new IntervalMinMax(xPx, yPxLow, yPxHigh);
        }

        private void RenderHighDensity(Settings settings, double offsetPoints, double columnPointCount)
        {
            int xPxStart = (int)Math.Ceiling((-1 - offsetPoints + this.minRenderIndex) / columnPointCount - 1);
            int xPxEnd = (int)Math.Ceiling((this.maxRenderIndex - offsetPoints) / columnPointCount);
            xPxStart = Math.Max(0, xPxStart);
            xPxEnd = Math.Min(settings.dataSize.Width, xPxEnd);
            if (xPxStart >= xPxEnd)
                return;

            var columns = Enumerable.Range(xPxStart, xPxEnd - xPxStart);

            IEnumerable<IntervalMinMax> intervals;
            if (this.useParallel)
            {
                intervals = columns
                    .AsParallel()
                    .AsOrdered()
                    .Select(xPx => this.CalcInterval(xPx, offsetPoints, columnPointCount, settings))
                    .AsSequential();
            }
            else
            {
                intervals = columns
                    .Select(xPx => this.CalcInterval(xPx, offsetPoints, columnPointCount, settings));
            }

            PointF[] linePoints = intervals
                .SelectMany(c => c.GetPoints())
                .ToArray();

            // adjust order of points to enhance anti-aliasing
            PointF buf;
            for (int i = 1; i < linePoints.Length / 2; i++)
            {
                if (linePoints[i * 2].Y >= linePoints[i * 2 - 1].Y)
                {
                    buf = linePoints[i * 2];
                    linePoints[i * 2] = linePoints[i * 2 + 1];
                    linePoints[i * 2 + 1] = buf;
                }
            }

            if (linePoints.Length > 0)
                settings.gfxData.DrawLines(this.penHD, linePoints);
        }

        private void RenderHighDensityDistributionParallel(Settings settings, double offsetPoints, double columnPointCount)
        {
            int xPxStart = (int)Math.Ceiling((-1 - offsetPoints) / columnPointCount - 1);
            int xPxEnd = (int)Math.Ceiling((this.ys.Length - offsetPoints) / columnPointCount);
            xPxStart = Math.Max(0, xPxStart);
            xPxEnd = Math.Min(settings.dataSize.Width, xPxEnd);
            if (xPxStart >= xPxEnd)
                return;
            List<PointF> linePoints = new List<PointF>((xPxEnd - xPxStart) * 2 + 1);

            var levelValues = Enumerable.Range(xPxStart, xPxEnd - xPxStart)
                .AsParallel()
                .AsOrdered()
                .Select(xPx =>
                {
                    // determine data indexes for this pixel column
                    int index1 = (int)(offsetPoints + columnPointCount * xPx);
                    int index2 = (int)(offsetPoints + columnPointCount * (xPx + 1));

                    if (index1 < 0)
                        index1 = 0;
                    if (index1 > this.ys.Length - 1)
                        index1 = this.ys.Length - 1;
                    if (index2 > this.ys.Length - 1)
                        index2 = this.ys.Length - 1;

                    var indexes = Enumerable.Range(0, this.densityLevelCount + 1).Select(x => x * (index2 - index1 - 1) / (this.densityLevelCount));

                    var levelsValues = new ArraySegment<T>(this.ys, index1, index2 - index1)
                        .OrderBy(x => x)
                        .Where((y, i) => indexes.Contains(i)).ToArray();
                    return (xPx, levelsValues);
                })
                .ToArray();

            List<PointF[]> linePointsLevels = levelValues
                .Select(x => x.levelsValues
                                .Select(y => new PointF(x.xPx, (float)settings.GetPixelY(Convert.ToDouble(y) + this.yOffset)))
                                .ToArray())
                .ToList();

            for (int i = 0; i < this.densityLevelCount; i++)
            {
                linePoints.Clear();
                for (int j = 0; j < linePointsLevels.Count; j++)
                {
                    if (i + 1 < linePointsLevels[j].Length)
                    {
                        linePoints.Add(linePointsLevels[j][i]);
                        linePoints.Add(linePointsLevels[j][i + 1]);
                    }
                }
                settings.gfxData.DrawLines(this.penByDensity[i], linePoints.ToArray());
            }
        }

        public override void Render(Settings settings)
        {
            this.brush = new SolidBrush(this.color);

            double dataSpanUnits = this.ys.Length * this.samplePeriod;
            double columnSpanUnits = settings.axes.x.span / settings.dataSize.Width;
            double columnPointCount = (columnSpanUnits / dataSpanUnits) * this.ys.Length;
            double offsetUnits = settings.axes.x.min - this.xOffset;
            double offsetPoints = offsetUnits / this.samplePeriod;
            int visibleIndex1 = (int)(offsetPoints);
            int visibleIndex2 = (int)(offsetPoints + columnPointCount * (settings.dataSize.Width + 1));
            int visiblePointCount = visibleIndex2 - visibleIndex1;
            double pointsPerPixelColumn = visiblePointCount / settings.dataSize.Width;
            double dataWidthPx2 = visibleIndex2 - visibleIndex1 + 2;

            PointF firstPoint = settings.GetPixel(this.xOffset, this.minmaxSearchStrategy.SourceElement(0) + this.yOffset); ;
            PointF lastPoint = settings.GetPixel(this.samplePeriod * (this.ys.Length - 1) + this.xOffset, this.minmaxSearchStrategy.SourceElement(this.ys.Length - 1) + this.yOffset);
            double dataWidthPx = lastPoint.X - firstPoint.X;

            // set this now, and let the render function change it if it happens
            this.markersAreVisible = false;

            // use different rendering methods based on how dense the data is on screen
            if ((dataWidthPx <= 1) || (dataWidthPx2 <= 1))
            {
                this.RenderSingleLine(settings);
            }
            else if (pointsPerPixelColumn > 1)
            {
                if (this.densityLevelCount > 0 && pointsPerPixelColumn > this.densityLevelCount)
                    this.RenderHighDensityDistributionParallel(settings, offsetPoints, columnPointCount);
                else
                    this.RenderHighDensity(settings, offsetPoints, columnPointCount);
            }
            else
            {
                this.RenderLowDensity(settings, visibleIndex1, visibleIndex2);
            }
        }

        public override string ToString()
        {
            string label = string.IsNullOrWhiteSpace(this.label) ? "" : $" ({this.label})";
            return $"PlottableSignalBase{label} with {this.GetPointCount()} points ({typeof(T).Name})";
        }

        public void SaveCSV(string filePath, string delimiter = ", ", string separator = "\n")
        {
            System.IO.File.WriteAllText(filePath, this.GetCSV(delimiter, separator));
        }

        public string GetCSV(string delimiter = ", ", string separator = "\n")
        {
            StringBuilder csv = new StringBuilder();
            for (int i = 0; i < this.ys.Length; i++)
                csv.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}{2}{3}", this.xOffset + i * this.samplePeriod, delimiter, this.minmaxSearchStrategy.SourceElement(i) + this.yOffset, separator);
            return csv.ToString();
        }

        public override int GetPointCount()
        {
            return this.ys.Length;
        }

        public override LegendItem[] GetLegendItems()
        {
            var singleLegendItem = new Config.LegendItem(this.label, this.color)
            {
                lineStyle = lineStyle,
                lineWidth = lineWidth,
                markerShape = (this.markersAreVisible) ? MarkerShape.filledCircle : MarkerShape.none,
                markerSize = (this.markersAreVisible) ? this.markerSize : 0
            };
            return new LegendItem[] { singleLegendItem };
        }
    }
}
