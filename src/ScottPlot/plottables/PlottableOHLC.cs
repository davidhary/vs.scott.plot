﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottableOHLC : Plottable
    {
        public OHLC[] ohlcs;
        bool candleFormat;
        bool autoWidth;
        bool sequential;
        Pen penUp;
        Pen penDown;
        Brush brushUp;
        Brush brushDown;

        public PlottableOHLC(OHLC[] ohlcs, bool candleFormat, bool autoWidth, Color colorUp, Color colorDown, bool sequential)
        {
            this.ohlcs = ohlcs;
            this.candleFormat = candleFormat;
            this.autoWidth = autoWidth;
            this.sequential = sequential;

            this.penUp = new Pen(colorUp);
            this.penDown = new Pen(colorDown);
            this.brushUp = new SolidBrush(colorUp);
            this.brushDown = new SolidBrush(colorDown);
        }

        public override string ToString()
        {
            return $"PlottableOHLC with {this.GetPointCount()} points";
        }

        public override Config.AxisLimits2D GetLimits()
        {
            double[] limits = new double[4];
            limits[0] = this.ohlcs[0].time;
            limits[1] = this.ohlcs[0].time;
            limits[2] = this.ohlcs[0].low;
            limits[3] = this.ohlcs[0].high;

            for (int i = 1; i < this.ohlcs.Length; i++)
            {
                if (this.ohlcs[i].time < limits[0])
                    limits[0] = this.ohlcs[i].time;
                if (this.ohlcs[i].time > limits[1])
                    limits[1] = this.ohlcs[i].time;
                if (this.ohlcs[i].low < limits[2])
                    limits[2] = this.ohlcs[i].low;
                if (this.ohlcs[i].high > limits[3])
                    limits[3] = this.ohlcs[i].high;
            }

            if (this.sequential)
            {
                limits[0] = 0;
                limits[1] = this.ohlcs.Length - 1;
            }

            return new Config.AxisLimits2D(limits);
        }

        public override void Render(Settings settings)
        {
            if (this.candleFormat)
                this.RenderCandles(settings);
            else
                this.RenderOhlc(settings);
        }

        private double GetSmallestSpacing()
        {
            double smallestSpacing = double.PositiveInfinity;
            for (int i = 1; i < this.ohlcs.Length; i++)
                smallestSpacing = Math.Min(this.ohlcs[i].time - this.ohlcs[i - 1].time, smallestSpacing);
            return smallestSpacing;
        }

        public void RenderCandles(Settings settings)
        {
            double fractionalTickWidth = .7;
            float boxWidth = 10;

            if (this.autoWidth)
            {
                double spacingPx = this.GetSmallestSpacing() * settings.xAxisScale;
                boxWidth = (float)(spacingPx / 2 * fractionalTickWidth);
            }

            for (int i = 0; i < this.ohlcs.Length; i++)
            {
                var ohlc = this.ohlcs[i];
                var ohlcTime = (this.sequential) ? i : ohlc.time;

                if (this.autoWidth == false)
                    boxWidth = (float)(ohlc.timeSpan * settings.xAxisScale / 2 * fractionalTickWidth);

                Pen pen = (ohlc.closedHigher) ? this.penUp : this.penDown;
                Brush brush = (ohlc.closedHigher) ? this.brushUp : this.brushDown;
                pen.Width = (boxWidth >= 2) ? 2 : 1;

                // the wick below the box
                PointF wickLowBot = settings.GetPixel(ohlcTime, ohlc.low);
                PointF wickLowTop = settings.GetPixel(ohlcTime, ohlc.lowestOpenClose);
                settings.gfxData.DrawLine(pen, wickLowBot, wickLowTop);

                // the wick above the box
                PointF wickHighBot = settings.GetPixel(ohlcTime, ohlc.highestOpenClose);
                PointF wickHighTop = settings.GetPixel(ohlcTime, ohlc.high);
                settings.gfxData.DrawLine(pen, wickHighBot, wickHighTop);

                // the candle
                PointF boxLowerLeft = settings.GetPixel(ohlcTime, ohlc.lowestOpenClose);
                PointF boxUpperRight = settings.GetPixel(ohlcTime, ohlc.highestOpenClose);
                settings.gfxData.FillRectangle(brush, boxLowerLeft.X - boxWidth, boxUpperRight.Y, boxWidth * 2, boxLowerLeft.Y - boxUpperRight.Y);
            }
        }

        public void RenderOhlc(Settings settings)
        {
            double fractionalTickWidth = .7;
            float boxWidth = 10;

            if (this.autoWidth)
            {
                double spacingPx = this.GetSmallestSpacing() * settings.xAxisScale;
                boxWidth = (float)(spacingPx / 2 * fractionalTickWidth);
            }

            for (int i = 0; i < this.ohlcs.Length; i++)
            {
                var ohlc = this.ohlcs[i];
                var ohlcTime = (this.sequential) ? i : ohlc.time;

                if (this.autoWidth == false)
                    boxWidth = (float)(ohlc.timeSpan * settings.xAxisScale / 2 * fractionalTickWidth);

                Pen pen = (ohlc.closedHigher) ? this.penUp : this.penDown;
                pen.Width = (boxWidth >= 2) ? 2 : 1;

                // the main line
                PointF wickTop = settings.GetPixel(ohlcTime, ohlc.low);
                PointF wickBot = settings.GetPixel(ohlcTime, ohlc.high);
                settings.gfxData.DrawLine(pen, wickBot, wickTop);

                // open and close lines
                float xPx = wickTop.X;
                float yPxOpen = (float)settings.GetPixel(0, ohlc.open).Y;
                float yPxClose = (float)settings.GetPixel(0, ohlc.close).Y;
                settings.gfxData.DrawLine(pen, xPx - boxWidth, yPxOpen, xPx, yPxOpen);
                settings.gfxData.DrawLine(pen, xPx + boxWidth, yPxClose, xPx, yPxClose);
            }
        }

        public override int GetPointCount()
        {
            return this.ohlcs.Length;
        }

        public override LegendItem[] GetLegendItems()
        {
            return null; // don't show this in the legend
        }
    }
}
