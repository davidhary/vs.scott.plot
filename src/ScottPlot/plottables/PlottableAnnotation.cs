﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottableAnnotation : Plottable
    {
        public double xPixel;
        public double yPixel;
        public string label;

        Font font;
        Brush fontBrush;
        bool fill;
        Brush fillBrush;
        Brush shadowBrush;
        Pen pen;
        bool shadow;


        public PlottableAnnotation(double xPixel, double yPixel, string label,
            double fontSize, string fontName, Color fontColor,
            bool fill, Color fillColor,
            double lineWidth, Color lineColor,
            bool shadow)
        {
            this.xPixel = xPixel;
            this.yPixel = yPixel;
            this.label = label;
            this.shadow = shadow;

            this.font = new Font(Fonts.GetValidFontName(fontName), (float)fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
            this.fontBrush = new SolidBrush(fontColor);

            this.fill = fill;
            this.fillBrush = new SolidBrush(fillColor);
            this.shadowBrush = new SolidBrush(Color.FromArgb(50, 0, 0, 0));

            this.pen = new Pen(lineColor, (float)lineWidth);
        }

        public override string ToString()
        {
            return $"PlottableAnnotation at ({this.xPixel} px, {this.yPixel} px)";
        }

        public override int GetPointCount()
        {
            return 1;
        }

        public override AxisLimits2D GetLimits()
        {
            return new AxisLimits2D();
        }

        public override LegendItem[] GetLegendItems()
        {
            return null;
        }

        public override void Render(Settings settings)
        {
            if (this.label is null)
                return;

            SizeF size = Drawing.GDI.MeasureString(settings.gfxData, this.label, this.font);

            double x = (this.xPixel >= 0) ? this.xPixel : settings.bmpData.Width + this.xPixel - size.Width;
            double y = (this.yPixel >= 0) ? this.yPixel : settings.bmpData.Height + this.yPixel - size.Height;

            PointF location = new PointF((float)x, (float)y);

            if (this.fill && this.shadow)
                settings.gfxData.FillRectangle(this.shadowBrush, location.X + 5, location.Y + 5, size.Width, size.Height);

            if (this.fill)
                settings.gfxData.FillRectangle(this.fillBrush, location.X, location.Y, size.Width, size.Height);

            if (this.pen.Width > 0)
                settings.gfxData.DrawRectangle(this.pen, location.X, location.Y, size.Width, size.Height);

            settings.gfxData.DrawString(this.label, this.font, this.fontBrush, location);
        }
    }
}
