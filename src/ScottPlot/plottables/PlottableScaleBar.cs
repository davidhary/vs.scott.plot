﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using ScottPlot.Config;

namespace ScottPlot
{
    public class PlottableScaleBar : Plottable
    {
        readonly double sizeX;
        readonly double sizeY;
        readonly string labelX;
        readonly string labelY;
        readonly double padPx;
        readonly double thickness;
        readonly Color color;
        readonly string fontName;
        readonly double fontSize;
        readonly FontStyle fontStyle = FontStyle.Regular;

        public PlottableScaleBar(double sizeX, double sizeY, string labelX, string labelY,
            double thickness, double fontSize, Color color, double padPx)
        {
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.labelX = (labelX is null) ? sizeX.ToString() : labelX;
            this.labelY = (labelY is null) ? sizeY.ToString() : labelY;
            this.thickness = thickness;
            this.fontSize = fontSize;
            this.color = color;
            this.padPx = padPx;
            this.fontName = Fonts.GetDefaultFontName();
        }

        public override string ToString()
        {
            return $"PlottableScaleBar ({this.labelX}, {this.labelY})";
        }

        public override LegendItem[] GetLegendItems()
        {
            return null;
        }

        public override AxisLimits2D GetLimits()
        {
            return new AxisLimits2D();
        }

        public override int GetPointCount()
        {
            return 1;
        }

        public override void Render(Settings settings)
        {
            float widthPx = (float)(this.sizeX * settings.xAxisScale);
            float heightPx = (float)(this.sizeY * settings.yAxisScale);

            using (var font = new Font(this.fontName, (float)this.fontSize, this.fontStyle))
            using (var brush = new SolidBrush(this.color))
            using (var pen = new Pen(this.color, (float)this.thickness))
            {
                PointF cornerPoint = settings.GetPixel(settings.axes.x.max, settings.axes.y.min);
                cornerPoint.X -= (float)this.padPx;
                cornerPoint.Y -= (float)this.padPx;

                var xLabelSize = Drawing.GDI.MeasureString(settings.gfxData, this.labelX, font);
                var yLabelSize = Drawing.GDI.MeasureString(settings.gfxData, this.labelY, font);
                cornerPoint.X -= yLabelSize.Width * 1.2f;
                cornerPoint.Y -= yLabelSize.Height;

                PointF horizPoint = new PointF(cornerPoint.X - widthPx, cornerPoint.Y);
                PointF vertPoint = new PointF(cornerPoint.X, cornerPoint.Y - heightPx);
                PointF horizMidPoint = new PointF((cornerPoint.X + horizPoint.X) / 2, cornerPoint.Y);
                PointF vertMidPoint = new PointF(cornerPoint.X, (cornerPoint.Y + vertPoint.Y) / 2);

                settings.gfxData.DrawLines(pen, new PointF[] { horizPoint, cornerPoint, vertPoint });
                settings.gfxData.DrawString(this.labelX, font, brush, horizMidPoint.X, cornerPoint.Y, settings.misc.sfNorth);
                settings.gfxData.DrawString(this.labelY, font, brush, cornerPoint.X, vertMidPoint.Y, settings.misc.sfWest);
            }
        }
    }
}
