﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms
{
    public partial class FormCookbook : Form
    {
        public FormCookbook()
        {
            this.InitializeComponent();
            this.Icon = new Icon("icon.ico");
            this.pictureBox1.Dock = DockStyle.Fill;
            this.LoadTreeWithDemos();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void LoadTreeWithDemos()
        {
            foreach (DemoNodeItem majorItem in Reflection.GetPlotNodeItems())
            {
                var majorNode = new TreeNode(majorItem.Header);
                this.treeView1.Nodes.Add(majorNode);
                foreach (DemoNodeItem minorItem in majorItem.Items)
                {
                    var minorNode = new TreeNode(minorItem.Header);
                    majorNode.Nodes.Add(minorNode);
                    foreach (DemoNodeItem plotItem in minorItem.Items)
                    {
                        var plotNode = new TreeNode(plotItem.Header);
                        plotNode.Tag = plotItem.Tag;
                        minorNode.Nodes.Add(plotNode);
                    }
                }
            }
            this.treeView1.Nodes[1].Expand();
            this.treeView1.Nodes[2].Expand();
            this.treeView1.SelectedNode = this.treeView1.Nodes[0].Nodes[0].Nodes[0];
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string tag = this.treeView1.SelectedNode?.Tag?.ToString();
            if (tag != null)
                this.LoadDemo(tag);
        }

        private void LoadDemo(string objectPath)
        {
            var demoPlot = Reflection.GetPlot(objectPath);

            this.DemoNameLabel.Text = demoPlot.name;
            this.sourceCodeGroupbox.Text = demoPlot.classPath.Replace("+", ".");
            this.DescriptionTextbox.Text = (demoPlot.description is null) ? "no descriton provided..." : demoPlot.description;
            this.sourceCodeTextbox.Text = demoPlot.GetSourceCode("../../../../src/ScottPlot.Demo/");

            this.formsPlot1.Reset();

            if (demoPlot is IBitmapDemo bmpPlot)
            {
                this.formsPlot1.Visible = false;
                this.pictureBox1.Visible = true;
                this.pictureBox1.Image = bmpPlot.Render(800, 600);
                this.formsPlot1_Rendered(null, null);
            }
            else
            {
                this.formsPlot1.Visible = true;
                this.pictureBox1.Visible = false;

                demoPlot.Render(this.formsPlot1.plt);
                this.formsPlot1.Render();
            }

        }

        private void formsPlot1_Rendered(object sender, EventArgs e)
        {
            if (this.formsPlot1.Visible)
                this.tbBenchmark.Text = this.formsPlot1.plt.GetSettings(false).Benchmark.ToString();
            else
                this.tbBenchmark.Text = "This plot is a non-interactive Bitmap";
        }
    }
}
