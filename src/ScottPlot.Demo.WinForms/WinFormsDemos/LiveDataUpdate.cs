﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class LiveDataUpdate : Form
    {
        Random rand = new Random();
        double[] liveData = DataGen.Sin(100, oscillations: 2, mult: 20);

        public LiveDataUpdate()
        {
            this.InitializeComponent();
            this.formsPlot1.Configure(middleClickMarginX: 0);

            // plot the data array only once
            this.formsPlot1.plt.PlotSignal(this.liveData);
            this.formsPlot1.plt.Axis(y1: -50, y2: 50);
            this.formsPlot1.Render();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // modify the underlying data
            for (int i = 0; i < this.liveData.Length; i++)
                this.liveData[i] += this.rand.NextDouble() - .5;

            // then ask for a render to redraw the new data
            this.formsPlot1.Render(skipIfCurrentlyRendering: true);
        }
    }
}
