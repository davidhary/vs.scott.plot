﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class PlotsInScrollViewer : Form
    {
        Random rand = new Random();

        public PlotsInScrollViewer()
        {
            this.InitializeComponent();

            FormsPlot[] formsPlots = { this.formsPlot1, this.formsPlot2, this.formsPlot3 };

            foreach (FormsPlot formsPlot in formsPlots)
            {
                for (int i = 0; i < 3; i++)
                    formsPlot.plt.PlotSignal(DataGen.RandomWalk(this.rand, 100));

                formsPlot.Configure(enableScrollWheelZoom: false, enableRightClickMenu: false);

                formsPlot.Render();
            }
        }
    }
}
