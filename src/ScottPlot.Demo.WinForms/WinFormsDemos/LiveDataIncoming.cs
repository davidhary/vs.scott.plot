﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class LiveDataIncoming : Form
    {
        public double[] data = new double[100_000];
        int nextDataIndex = 1;
        PlottableSignal signalPlot;
        Random rand = new Random(0);

        public LiveDataIncoming()
        {
            this.InitializeComponent();
            this.signalPlot = this.formsPlot1.plt.PlotSignal(this.data);
            this.formsPlot1.plt.YLabel("Value");
            this.formsPlot1.plt.XLabel("Sample Number");
        }

        private void LiveDataIncoming_Load(object sender, EventArgs e)
        {

        }

        private void dataTimer_Tick(object sender, EventArgs e)
        {
            if (this.nextDataIndex >= this.data.Length)
            {
                throw new OverflowException("data array isn't long enough to accomodate new data");
                // in this situation the solution would be:
                //   1. clear the plot
                //   2. create a new larger array
                //   3. copy the old data into the start of the larger array
                //   4. plot the new (larger) array
                //   5. continue to update the new array
            }

            double randomValue = Math.Round(this.rand.NextDouble() - .5, 3);
            double latestValue = this.data[this.nextDataIndex - 1] + randomValue;
            this.data[this.nextDataIndex] = latestValue;
            this.tbLastValue.Text = (latestValue > 0) ? "+" + latestValue.ToString() : latestValue.ToString();
            this.tbLatestValue.Text = this.nextDataIndex.ToString();

            this.signalPlot.maxRenderIndex = this.nextDataIndex;

            this.nextDataIndex += 1;
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            if (this.cbAutoAxis.Checked)
                this.formsPlot1.plt.AxisAuto();
            this.formsPlot1.Render();
        }

        private void cbAutoAxis_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cbAutoAxis.Checked == false)
            {
                double[] autoAxisLimits = this.formsPlot1.plt.AxisAuto(verticalMargin: .5);
                double oldX2 = autoAxisLimits[1];
                this.formsPlot1.plt.Axis(x2: oldX2 + 1000);
            }
        }
    }
}
