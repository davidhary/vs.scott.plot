﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class AxisLimits : Form
    {
        public AxisLimits()
        {
            this.InitializeComponent();
            this.formsPlot1.plt.PlotSignal(DataGen.Sin(51));
            this.formsPlot1.plt.PlotSignal(DataGen.Cos(51));
            this.formsPlot1.plt.AxisAuto();
            this.formsPlot1.plt.AxisBounds(0, 50, -1, 1);
            this.formsPlot1.Render();
        }
    }
}
