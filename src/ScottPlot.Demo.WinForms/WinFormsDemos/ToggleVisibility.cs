﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class ToggleVisibility : Form
    {
        public ToggleVisibility()
        {
            this.InitializeComponent();
        }

        PlottableScatter sinPlot, cosPlot;
        PlottableVLine vline1, vline2;

        private void ToggleVisibility_Load(object sender, EventArgs e)
        {
            int pointCount = 51;
            double[] dataXs = DataGen.Consecutive(pointCount);
            double[] dataSin = DataGen.Sin(pointCount);
            double[] dataCos = DataGen.Cos(pointCount);

            this.sinPlot = this.formsPlot1.plt.PlotScatter(dataXs, dataSin);
            this.cosPlot = this.formsPlot1.plt.PlotScatter(dataXs, dataCos);
            this.vline1 = this.formsPlot1.plt.PlotVLine(0);
            this.vline2 = this.formsPlot1.plt.PlotVLine(50);

            this.formsPlot1.Render();
        }

        private void cbSin_CheckedChanged(object sender, EventArgs e)
        {
            this.sinPlot.visible = this.cbSin.Checked;
            this.formsPlot1.Render();
        }

        private void cbCos_CheckedChanged(object sender, EventArgs e)
        {
            this.cosPlot.visible = this.cbCos.Checked;
            this.formsPlot1.Render();
        }

        private void cbLines_CheckedChanged(object sender, EventArgs e)
        {
            this.vline1.visible = this.cbLines.Checked;
            this.vline2.visible = this.cbLines.Checked;
            this.formsPlot1.Render();
        }
    }
}
