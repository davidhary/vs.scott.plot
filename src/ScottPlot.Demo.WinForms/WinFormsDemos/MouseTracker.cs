﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class MouseTracker : Form
    {
        PlottableVLine vLine;
        PlottableHLine hLine;

        public MouseTracker()
        {
            this.InitializeComponent();
            this.formsPlot1.plt.PlotSignal(DataGen.RandomWalk(null, 100));

            this.vLine = this.formsPlot1.plt.PlotVLine(0, color: Color.Red, lineStyle: LineStyle.Dash);
            this.hLine = this.formsPlot1.plt.PlotHLine(0, color: Color.Red, lineStyle: LineStyle.Dash);

            this.formsPlot1.Render();
        }

        private void formsPlot1_MouseMoved_1(object sender, MouseEventArgs e)
        {
            int pixelX = e.X;
            int pixelY = e.Y;

            (double coordinateX, double coordinateY) = this.formsPlot1.GetMouseCoordinates();

            this.XPixelLabel.Text = $"{e.X:0.000}";
            this.YPixelLabel.Text = $"{e.X:0.000}";

            this.XCoordinateLabel.Text = $"{coordinateX:0.00000000}";
            this.YCoordinateLabel.Text = $"{coordinateY:0.00000000}";

            this.vLine.position = coordinateX;
            this.hLine.position = coordinateY;

            this.formsPlot1.Render(skipIfCurrentlyRendering: true);
        }
    }
}
