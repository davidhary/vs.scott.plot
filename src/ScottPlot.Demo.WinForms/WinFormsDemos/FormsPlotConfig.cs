﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class FormsPlotConfig : Form
    {
        public FormsPlotConfig()
        {
            this.InitializeComponent();
        }

        private void FormsPlotConfig_Load(object sender, EventArgs e)
        {
            int pointCount = 51;
            double[] dataXs = DataGen.Consecutive(pointCount);
            double[] dataSin = DataGen.Sin(pointCount);
            double[] dataCos = DataGen.Cos(pointCount);

            this.formsPlot1.plt.PlotScatter(dataXs, dataSin);
            this.formsPlot1.plt.PlotScatter(dataXs, dataCos);

            this.formsPlot1.Render();
        }

        private void cbPannable_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(enablePanning: this.cbPannable.Checked);
        }

        private void cbZoomable_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(enableZooming: this.cbZoomable.Checked, enableScrollWheelZoom: this.cbZoomable.Checked);
        }

        private void cbLowQualWhileDragging_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(lowQualityWhileDragging: this.cbLowQualWhileDragging.Checked);
        }

        private void cbLockVertical_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(lockVerticalAxis: this.cbLockVertical.Checked);
        }

        private void cbLockHorizontal_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(lockHorizontalAxis: this.cbLockHorizontal.Checked);
        }

        private void cbEqualAxes_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(equalAxes: this.cbEqualAxes.Checked);
            this.formsPlot1.Render();
        }

        private void cbRightClickMenu_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(enableRightClickMenu: this.cbRightClickMenu.Checked);
        }

        private void cbDoubleClickBenchmark_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cbDoubleClickBenchmark.Checked is false)
            {
                this.formsPlot1.plt.Benchmark(show: false);
                this.formsPlot1.Render();
            }

            this.formsPlot1.Configure(enableDoubleClickBenchmark: this.cbDoubleClickBenchmark.Checked);
        }

        private void cbCustomRightClick_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cbCustomRightClick.Checked)
            {
                this.cbRightClickMenu.Checked = false;
                this.formsPlot1.Configure(enableRightClickMenu: false);
            }
        }

        private void formsPlot1_MouseClicked(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && this.cbCustomRightClick.Checked)
            {
                MessageBox.Show("This is a custom right-click action");
            }
        }

        private void cbTooltip_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.Configure(showCoordinatesTooltip: this.cbTooltip.Checked);
        }
    }
}
