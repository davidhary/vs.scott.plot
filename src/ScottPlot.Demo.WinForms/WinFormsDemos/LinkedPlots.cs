﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScottPlot.Demo.WinForms.WinFormsDemos
{
    public partial class LinkedPlots : Form
    {
        public LinkedPlots()
        {
            this.InitializeComponent();
        }

        private void LinkedPlots_Load(object sender, EventArgs e)
        {
            Random rand = new Random(0);
            int pointCount = 5000;
            double[] dataXs = DataGen.Consecutive(pointCount);
            double[] dataSin = DataGen.NoisySin(rand, pointCount);
            double[] dataCos = DataGen.NoisySin(rand, pointCount);

            this.formsPlot1.plt.PlotScatter(dataXs, dataSin);
            this.formsPlot1.Render();

            this.formsPlot2.plt.PlotScatter(dataXs, dataCos);
            this.formsPlot2.Render();
        }

        private void formsPlot1_AxesChanged(object sender, EventArgs e)
        {
            this.formsPlot2.plt.MatchAxis(this.formsPlot1.plt);
            this.formsPlot2.Render(skipIfCurrentlyRendering: true, processEvents: this.cbProcessEvents.Checked);
        }

        private void formsPlot2_AxesChanged(object sender, EventArgs e)
        {
            this.formsPlot1.plt.MatchAxis(this.formsPlot2.plt);
            this.formsPlot1.Render(skipIfCurrentlyRendering: true, processEvents: this.cbProcessEvents.Checked);
        }
    }
}
