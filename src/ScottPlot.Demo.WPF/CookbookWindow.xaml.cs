﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScottPlot.Demo.WPF
{
    /// <summary>
    /// Interaction logic for DemoNavigator.xaml
    /// </summary>
    public partial class CookbookWindow : Window
    {
        public CookbookWindow()
        {
            this.InitializeComponent();
            this.LoadTreeWithDemos();
        }

        private void DemoSelected(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var selectedDemoItem = (DemoNodeItem)this.DemoTreeview.SelectedItem;
            if (selectedDemoItem.Tag != null)
            {
                this.DemoPlotControl1.Visibility = Visibility.Visible;
                this.AboutControl.Visibility = Visibility.Hidden;
                this.DemoPlotControl1.LoadDemo(selectedDemoItem.Tag);
            }
            else
            {
                this.DemoPlotControl1.Visibility = Visibility.Hidden;
                this.AboutControl.Visibility = Visibility.Visible;
            }
        }

        private void LoadTreeWithDemos()
        {
            this.DemoTreeview.ItemsSource = Reflection.GetPlotNodeItems();
            this.DemoTreeview.Focus();
        }
    }
}
