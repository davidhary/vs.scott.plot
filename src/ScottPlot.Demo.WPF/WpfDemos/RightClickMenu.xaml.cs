﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScottPlot.Demo.WPF.WpfDemos
{
    /// <summary>
    /// Interaction logic for RightClickMenu.xaml
    /// </summary>
    public partial class RightClickMenu : Window
    {
        public RightClickMenu()
        {
            this.InitializeComponent();

            this.wpfPlot1.plt.PlotSignal(DataGen.Sin(51));
            this.wpfPlot1.plt.PlotSignal(DataGen.Cos(51));
            this.wpfPlot1.Render();

            MenuItem addSinMenuItem = new MenuItem() { Header = "Add Sine Wave" };
            addSinMenuItem.Click += this.AddSine;
            MenuItem clearPlotMenuItem = new MenuItem() { Header = "Clear Plot" };
            clearPlotMenuItem.Click += this.ClearPlot;

            ContextMenu rightClickMenu = new ContextMenu();
            rightClickMenu.Items.Add(addSinMenuItem);
            rightClickMenu.Items.Add(clearPlotMenuItem);

            this.wpfPlot1.ContextMenu = rightClickMenu;
        }

        private void AddSine(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            this.wpfPlot1.plt.PlotSignal(DataGen.Sin(51, phase: rand.NextDouble() * 1000));
            this.wpfPlot1.plt.AxisAuto();
            this.wpfPlot1.Render();
        }

        private void ClearPlot(object sender, RoutedEventArgs e)
        {
            this.wpfPlot1.plt.Clear();
            this.wpfPlot1.plt.AxisAuto();
            this.wpfPlot1.Render();
        }
    }
}
