﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScottPlot.Demo.WPF.WpfDemos
{
    /// <summary>
    /// Interaction logic for LinkedPlots.xaml
    /// </summary>
    public partial class LinkedPlots : Window
    {
        public LinkedPlots()
        {
            this.InitializeComponent();

            int pointCount = 51;
            double[] dataXs = DataGen.Consecutive(pointCount);
            double[] dataSin = DataGen.Sin(pointCount);
            double[] dataCos = DataGen.Cos(pointCount);

            this.wpfPlot1.plt.PlotScatter(dataXs, dataSin);
            this.wpfPlot1.Render();

            this.wpfPlot2.plt.PlotScatter(dataXs, dataCos);
            this.wpfPlot2.Render();
        }

        private void axisChanged1(object sender, EventArgs e)
        {
            this.wpfPlot2.plt.MatchAxis(this.wpfPlot1.plt);
            this.wpfPlot2.Render();
        }

        private void axisChanged2(object sender, EventArgs e)
        {
            this.wpfPlot1.plt.MatchAxis(this.wpfPlot2.plt);
            this.wpfPlot1.Render();
        }
    }
}
