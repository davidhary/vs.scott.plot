﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ScottPlot.Demo.WPF.WpfDemos
{
    /// <summary>
    /// Interaction logic for LiveDataFixed.xaml
    /// </summary>
    public partial class LiveDataFixed : Window
    {
        Random rand = new Random();
        double[] liveData = DataGen.Sin(100, oscillations: 2, mult: 20);

        public LiveDataFixed()
        {
            this.InitializeComponent();
            this.wpfPlot1.Configure(middleClickMarginX: 0);

            // plot the data array only once
            this.wpfPlot1.plt.PlotSignal(this.liveData);
            this.wpfPlot1.plt.Axis(y1: -50, y2: 50);
            this.wpfPlot1.Render();

            // create a timer to modify the data
            DispatcherTimer updateDataTimer = new DispatcherTimer();
            updateDataTimer.Interval = TimeSpan.FromMilliseconds(1);
            updateDataTimer.Tick += this.UpdateData;
            updateDataTimer.Start();

            // create a timer to update the GUI
            DispatcherTimer renderTimer = new DispatcherTimer();
            renderTimer.Interval = TimeSpan.FromMilliseconds(20);
            renderTimer.Tick += this.Render;
            renderTimer.Start();
        }

        void UpdateData(object sender, EventArgs e)
        {
            for (int i = 0; i < this.liveData.Length; i++)
                this.liveData[i] += this.rand.NextDouble() - .5;
        }

        void Render(object sender, EventArgs e)
        {
            this.wpfPlot1.Render(skipIfCurrentlyRendering: true);
        }
    }
}
