﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScottPlot.Demo.WPF.WpfDemos
{
    /// <summary>
    /// Interaction logic for ToggleVisibility.xaml
    /// </summary>
    public partial class ToggleVisibility : Window
    {
        PlottableScatter sinPlot, cosPlot;
        PlottableVLine vline1, vline2;

        public ToggleVisibility()
        {
            this.InitializeComponent();

            int pointCount = 51;
            double[] dataXs = DataGen.Consecutive(pointCount);
            double[] dataSin = DataGen.Sin(pointCount);
            double[] dataCos = DataGen.Cos(pointCount);

            this.sinPlot = this.wpfPlot1.plt.PlotScatter(dataXs, dataSin);
            this.cosPlot = this.wpfPlot1.plt.PlotScatter(dataXs, dataCos);
            this.vline1 = this.wpfPlot1.plt.PlotVLine(0);
            this.vline2 = this.wpfPlot1.plt.PlotVLine(50);

            this.wpfPlot1.Render();
        }

        private void SinHide(object sender, RoutedEventArgs e)
        {
            if (this.wpfPlot1 is null) return;
            this.sinPlot.visible = false;
            this.wpfPlot1.Render();
        }

        private void SinShow(object sender, RoutedEventArgs e)
        {
            if (this.wpfPlot1 is null) return;
            this.sinPlot.visible = true;
            this.wpfPlot1.Render();
        }

        private void CosShow(object sender, RoutedEventArgs e)
        {
            if (this.wpfPlot1 is null) return;
            this.cosPlot.visible = true;
            this.wpfPlot1.Render();
        }

        private void CosHide(object sender, RoutedEventArgs e)
        {
            if (this.wpfPlot1 is null) return;
            this.cosPlot.visible = false;
            this.wpfPlot1.Render();
        }

        private void LinesShow(object sender, RoutedEventArgs e)
        {
            if (this.wpfPlot1 is null) return;
            this.vline1.visible = true;
            this.vline2.visible = true;
            this.wpfPlot1.Render();
        }

        private void LinesHide(object sender, RoutedEventArgs e)
        {
            if (this.wpfPlot1 is null) return;
            this.vline1.visible = false;
            this.vline2.visible = false;
            this.wpfPlot1.Render();
        }
    }
}
