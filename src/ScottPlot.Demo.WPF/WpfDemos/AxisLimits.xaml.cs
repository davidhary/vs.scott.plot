﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScottPlot.Demo.WPF.WpfDemos
{
    /// <summary>
    /// Interaction logic for AxisLimits.xaml
    /// </summary>
    public partial class AxisLimits : Window
    {
        public AxisLimits()
        {
            this.InitializeComponent();
            this.wpfPlot1.plt.PlotSignal(DataGen.Sin(51));
            this.wpfPlot1.plt.PlotSignal(DataGen.Cos(51));
            this.wpfPlot1.plt.AxisAuto();
            this.wpfPlot1.plt.AxisBounds(0, 50, -1, 1);
            this.wpfPlot1.Render();
        }
    }
}
