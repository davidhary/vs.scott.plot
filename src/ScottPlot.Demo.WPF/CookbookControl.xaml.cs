﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScottPlot.Demo.WPF
{
    /// <summary>
    /// Interaction logic for DemoPlotControl.xaml
    /// </summary>
    public partial class CookbookControl : UserControl
    {
        string sourceCodeFolder = Reflection.FindDemoSourceFolder();

        public CookbookControl()
        {
            this.InitializeComponent();

            if (this.sourceCodeFolder is null)
                throw new ArgumentException("cannot locate source code");
        }

        private void WpfPlot1_Rendered(object sender, EventArgs e)
        {
            if (this.wpfPlot1.Visibility == Visibility.Visible)
                this.BenchmarkLabel.Content = this.wpfPlot1.plt.GetSettings(false).Benchmark.ToString();
            else
                this.BenchmarkLabel.Content = "This plot is a non-interactive Bitmap";
        }

        private BitmapImage BmpImageFromBmp(System.Drawing.Bitmap bmp)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Png); // use PNG to support transparency
            BitmapImage bmpImage = new BitmapImage();
            bmpImage.BeginInit();
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            bmpImage.StreamSource = stream;
            bmpImage.EndInit();
            return bmpImage;
        }

        public void LoadDemo(string objectPath)
        {
            var demoPlot = Reflection.GetPlot(objectPath);

            this.DemoNameLabel.Content = demoPlot.name;
            this.SourceCodeLabel.Content = $"{demoPlot.sourceFile} ({demoPlot.categoryClass})";
            this.DescriptionTextbox.Text = (demoPlot.description is null) ? "no descriton provided..." : demoPlot.description;
            string sourceCode = demoPlot.GetSourceCode(this.sourceCodeFolder);

            this.wpfPlot1.Reset();

            if (demoPlot is IBitmapDemo bmpPlot)
            {
                this.imagePlot.Visibility = Visibility.Visible;
                this.wpfPlot1.Visibility = Visibility.Hidden;
                this.imagePlot.Source = this.BmpImageFromBmp(bmpPlot.Render(600, 400));
                this.WpfPlot1_Rendered(null, null);
            }
            else
            {
                this.imagePlot.Visibility = Visibility.Hidden;
                this.wpfPlot1.Visibility = Visibility.Visible;

                demoPlot.Render(this.wpfPlot1.plt);
                this.wpfPlot1.Render();
            }

            this.SourceTextBox.Text = sourceCode;
        }
    }
}
