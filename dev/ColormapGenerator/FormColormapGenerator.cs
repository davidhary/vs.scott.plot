﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ColormapGenerator
{
    public partial class FormColormapGenerator : Form
    {
        public FormColormapGenerator()
        {
            this.InitializeComponent();
            this.btnRandomize_Click(null, null);
            this.richTextBox1.ForeColor = ColorTranslator.FromHtml("#d1cfce");
            this.richTextBox1.BackColor = ColorTranslator.FromHtml("#1e1e1e");
        }

        private void FormColormapGenerator_Load(object sender, EventArgs e) => this.Replot();

        private void tb1_Scroll(object sender, EventArgs e) => this.Replot();
        private void tb2_Scroll(object sender, EventArgs e) => this.Replot();
        private void tb3_Scroll(object sender, EventArgs e) => this.Replot();
        private void tb4_Scroll(object sender, EventArgs e) => this.Replot();
        private void tb5_Scroll(object sender, EventArgs e) => this.Replot();
        private void tb6_Scroll(object sender, EventArgs e) => this.Replot();

        private void tbGreen1_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbGreen2_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbGreen3_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbGreen4_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbGreen5_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbGreen6_Scroll(object sender, EventArgs e) => this.Replot();

        private void tbBlue1_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbBlue2_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbBlue3_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbBlue4_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbBlue5_Scroll(object sender, EventArgs e) => this.Replot();
        private void tbBlue6_Scroll(object sender, EventArgs e) => this.Replot();

        int[] colormap = new int[256];
        private void Replot()
        {
            this.formsPlot1.plt.Clear();

            double[] xs = { 0, 51, 102, 153, 204, 255 };
            double[] redYs = { this.tbRed1.Value, this.tbRed2.Value, this.tbRed3.Value, this.tbRed4.Value, this.tbRed5.Value, this.tbRed6.Value };
            double[] greenYs = { this.tbGreen1.Value, this.tbGreen2.Value, this.tbGreen3.Value, this.tbGreen4.Value, this.tbGreen5.Value, this.tbGreen6.Value };
            double[] blueYs = { this.tbBlue1.Value, this.tbBlue2.Value, this.tbBlue3.Value, this.tbBlue4.Value, this.tbBlue5.Value, this.tbBlue6.Value };

            int resolution = 51;
            var redInterp = new ScottPlot.Statistics.Interpolation.NaturalSpline(xs, redYs, resolution);
            var greenInterp = new ScottPlot.Statistics.Interpolation.NaturalSpline(xs, greenYs, resolution);
            var blueInterp = new ScottPlot.Statistics.Interpolation.NaturalSpline(xs, blueYs, resolution);

            double[] xsCurve = redInterp.interpolatedXs;
            double[] redCurve = redInterp.interpolatedYs;
            double[] greenCurve = greenInterp.interpolatedYs;
            double[] blueCurve = blueInterp.interpolatedYs;

            double[] meanCurve = new double[redCurve.Length];
            for (int i = 0; i < 256; i++)
            {
                byte redByte = (byte)Math.Max(Math.Min(redCurve[i], 255), 0);
                byte greenByte = (byte)Math.Max(Math.Min(greenCurve[i], 255), 0);
                byte blueByte = (byte)Math.Max(Math.Min(blueCurve[i], 255), 0);
                byte alphaByte = 255;
                byte[] bytes = { blueByte, greenByte, redByte, alphaByte };
                this.colormap[i] = BitConverter.ToInt32(bytes, 0);
                meanCurve[i] = (redByte + greenByte + blueByte) / 3.0;
            }

            this.formsPlot1.plt.PlotScatter(xs, redYs, Color.Red, 0);
            this.formsPlot1.plt.PlotScatter(xs, greenYs, Color.Green, 0);
            this.formsPlot1.plt.PlotScatter(xs, blueYs, Color.Blue, 0);

            this.formsPlot1.plt.PlotScatter(xsCurve, redCurve, color: Color.Red, markerSize: 0);
            this.formsPlot1.plt.PlotScatter(xsCurve, greenCurve, color: Color.Green, markerSize: 0);
            this.formsPlot1.plt.PlotScatter(xsCurve, blueCurve, color: Color.Blue, markerSize: 0);
            this.formsPlot1.plt.PlotScatter(xsCurve, meanCurve, color: Color.Black, markerSize: 0, lineStyle: ScottPlot.LineStyle.Dash);

            //formsPlot1.plt.Frame(false);
            //formsPlot1.plt.Ticks(false, false);
            //formsPlot1.plt.Axis(0, 255, 0, 255);
            this.formsPlot1.plt.PlotVLine(0, Color.Black, lineStyle: ScottPlot.LineStyle.Dot);
            this.formsPlot1.plt.PlotVLine(255, Color.Black, lineStyle: ScottPlot.LineStyle.Dot);
            this.formsPlot1.plt.PlotHLine(0, Color.Black, lineStyle: ScottPlot.LineStyle.Dot);
            this.formsPlot1.plt.PlotHLine(255, Color.Black, lineStyle: ScottPlot.LineStyle.Dot);
            this.formsPlot1.Render();

            Size cmapSize = this.pbColorbar.Size;
            Bitmap bmp = new Bitmap(cmapSize.Width, cmapSize.Height);
            using (var gfx = Graphics.FromImage(bmp))
            using (var pen = new Pen(Color.White))
            {
                for (int x = 0; x < cmapSize.Width; x++)
                {
                    double intensityFrac = (double)x / cmapSize.Width;
                    int intensityValue = (int)(255.0 * intensityFrac);
                    pen.Color = Color.FromArgb( this.colormap[intensityValue]);
                    gfx.DrawLine(pen, x, 0, x, cmapSize.Height);
                }
            }

            this.pbColorbar.Image?.Dispose();
            this.pbColorbar.Image = bmp;
            this.newValues = true;
        }

        private void btnRandomize_Click(object sender, EventArgs e)
        {
            List<TrackBar> trackbars = this.Controls.OfType<TrackBar>().Cast<TrackBar>().ToList();
            Random rand = new Random();
            foreach (var tb in trackbars)
                tb.Value = rand.Next(255);
            this.Replot();
        }

        private void btnZero_Click(object sender, EventArgs e)
        {
            List<TrackBar> trackbars = this.Controls.OfType<TrackBar>().Cast<TrackBar>().ToList();
            foreach (var tb in trackbars)
                tb.Value = 0;
            this.Replot();
        }

        private bool newValues = true;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if ( this.newValues == false)
                return;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("private readonly int[] argb =");
            sb.AppendLine("{");
            sb.Append("    ");
            for (int i = 0; i < this.colormap.Length; i++)
            {
                sb.Append($"{this.colormap[i]:D8}, ");
                if (i % 8 == 7)
                    sb.Append(Environment.NewLine + "    ");
            }
            this.richTextBox1.Text = sb.ToString().Trim() + "\n};";
            this.newValues = false;
        }

    }
}
