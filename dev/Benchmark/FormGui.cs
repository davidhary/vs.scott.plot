﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Benchmark
{
    public partial class FormGui : Form
    {
        ScottPlot.PlottableScatter scat;
        ScottPlot.PlottableSignal sig;
        ScottPlot.PlottableSignalConst<double> sigConst;

        public FormGui()
        {
            this.InitializeComponent();

            Random rand = new Random(0);

            this.sig = this.formsPlot1.plt.PlotSignal(
                ys: ScottPlot.DataGen.RandomWalk(rand, 10_000_000),
                label: "Signal (10M)");

            this.sigConst = this.formsPlot1.plt.PlotSignalConst(
                ys: ScottPlot.DataGen.RandomWalk(rand, 10_000_000),
                label: "SignalConst (10M)");

            this.scat = this.formsPlot1.plt.PlotScatter(
                xs: ScottPlot.DataGen.Consecutive(1_000, spacing: 10_000),
                ys: ScottPlot.DataGen.RandomWalk(rand, 1_000, 20),
                label: "Scatter (1k)"); ;

            this.formsPlot1.plt.Legend();
            this.formsPlot1.plt.Axis();
            this.formsPlot1.Render();
        }

        private double[] AxisMidWay(double frac)
        {
            double x1A = 185735;
            double x1B = -500_000;
            double x1 = x1A + (x1B - x1A) * frac;

            double x2A = 185748;
            double x2B = 5_500_000;
            double x2 = x2A + (x2B - x2A) * frac;

            double y1A = -11;
            double y1B = -2000;
            double y1 = y1A + (y1B - y1A) * frac;

            double y2A = -6;
            double y2B = 2000;
            double y2 = y2A + (y2B - y2A) * frac;

            return new double[] { x1, x2, y1, y2 };
        }

        int renders = 0;
        Stopwatch sw;
        private void timer1_Tick(object sender, EventArgs e)
        {
            int slowness = 50;
            int rendersPerCycle = (int)(slowness * Math.PI * 2);
            double pos = (double) this.renders++ / slowness;
            double frac = (Math.Sin(pos) + 1);
            this.progressBar1.Maximum = rendersPerCycle;
            this.progressBar1.Value = this.renders % rendersPerCycle;

            frac = frac * frac;
            this.formsPlot1.plt.Axis( this.AxisMidWay(frac));
            this.formsPlot1.Render();

            if ( this.renders % rendersPerCycle == 1)
            {
                if ( this.sw is null)
                {
                    this.sw = Stopwatch.StartNew();
                }
                else
                {
                    double elapsedSec = (double) this.sw.ElapsedTicks / Stopwatch.Frequency;
                    double fps = rendersPerCycle / elapsedSec;
                    this.TimeLabel.Text = $"{fps:0.00} FPS";
                    this.sw.Restart();
                }
            }
        }

        private void RunCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            this.timer1.Enabled = this.RunCheckbox.Checked;
        }

        private void AntiAliasCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.plt.AntiAlias(
                figure: this.AntiAliasCheckbox.Checked,
                data: this.AntiAliasCheckbox.Checked,
                legend: this.AntiAliasCheckbox.Checked);
        }
    }
}
