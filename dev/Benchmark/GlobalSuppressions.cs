﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormBmp.RunBenchmark(System.String,System.Int32)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormBmp.TestScatter_Click(System.Object,System.EventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormBmp.TestSignal_Click(System.Object,System.EventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormBmp.TestSignalConst_Click(System.Object,System.EventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormGui.#ctor" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormGui.timer1_Tick(System.Object,System.EventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormMain.BitmapButton_Click(System.Object,System.EventArgs)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:Benchmark.FormMain.ControlButton_Click(System.Object,System.EventArgs)" )]
