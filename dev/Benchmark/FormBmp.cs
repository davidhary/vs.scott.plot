﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Benchmark
{
    public partial class FormBmp : Form
    {
        private readonly ScottPlot.Plot plt = new ScottPlot.Plot();

        public FormBmp()
        {
            this.InitializeComponent();
            this.formsPlot1.Reset();
        }

        private void RunBenchmark(string label = null, int rendersPerSize = 10)
        {
            double[] widths = ScottPlot.DataGen.Consecutive(20, 100, 200);
            double[] mean = new double[widths.Length];
            double[] err = new double[widths.Length];

            this.progressBar1.Maximum = rendersPerSize * widths.Length;

            for (int i = 0; i < widths.Length; i++)
            {
                int width = (int)widths[i];
                int height = width * 3 / 4;
                this.plt.Resize(width, height);
                this.plt.GetBitmap();

                double[] times = new double[rendersPerSize];
                for (int j = 0; j < rendersPerSize; j++)
                {
                    this.progressBar1.Value = rendersPerSize * i + j + 1;
                    this.plt.GetBitmap(renderFirst: true);
                    times[j] = this.plt.GetSettings(false).Benchmark.msec;
                }

                mean[i] = ScottPlot.Statistics.Common.Mean(times);
                err[i] = ScottPlot.Statistics.Common.StDev(times);

                this.pictureBox1.Image?.Dispose();
                this.pictureBox1.Image = (Bitmap) this.plt.GetBitmap().Clone();
                Application.DoEvents();
            }

            this.formsPlot1.plt.PlotScatter(widths, mean, errorY: err, label: label);
            this.formsPlot1.plt.AxisAuto();
            this.formsPlot1.plt.Title("Benchmark (4:3 aspect)");
            this.formsPlot1.plt.YLabel("Render Time (ms)");
            this.formsPlot1.plt.XLabel("Plot Width (px)");
            this.formsPlot1.plt.Legend(location: ScottPlot.legendLocation.upperLeft);
            this.formsPlot1.Render();
        }

        private void TestEmpty_Click(object sender, EventArgs e)
        {
            this.formsPlot1.plt.Clear();

            this.plt.Clear();
            this.RunBenchmark("Baseline");
        }

        private void TestScatter_Click(object sender, EventArgs e)
        {
            Random rand = new Random(0);

            this.formsPlot1.plt.Clear();
            this.RunBenchmark("Baseline");

            this.plt.Clear();
            this.plt.PlotScatter(
                ScottPlot.DataGen.RandomWalk(rand, 100),
                ScottPlot.DataGen.RandomWalk(rand, 100));
            this.plt.AxisAuto();
            this.RunBenchmark("Scatter (100 pts)");

            this.plt.Clear();
            this.plt.PlotScatter(
                ScottPlot.DataGen.RandomWalk(rand, 1000),
                ScottPlot.DataGen.RandomWalk(rand, 1000));
            this.plt.AxisAuto();
            this.RunBenchmark("Scatter (1k pts)");
        }

        private void TestSignal_Click(object sender, EventArgs e)
        {
            Random rand = new Random(0);

            this.formsPlot1.plt.Clear();

            this.plt.Clear();
            this.RunBenchmark("Baseline");

            this.plt.Clear();
            this.plt.PlotSignal(ScottPlot.DataGen.RandomWalk(rand, 10_000));
            this.plt.AxisAuto();
            this.RunBenchmark("Signal (10k pts)");

            this.plt.Clear();
            this.plt.PlotSignal(ScottPlot.DataGen.RandomWalk(rand, 1_000_000));
            this.plt.AxisAuto();
            this.RunBenchmark("Signal (1M pts)");
        }

        private void TestSignalConst_Click(object sender, EventArgs e)
        {
            Random rand = new Random(0);

            this.formsPlot1.plt.Clear();

            this.plt.Clear();
            this.RunBenchmark("Baseline");

            this.plt.Clear();
            this.plt.PlotSignal(ScottPlot.DataGen.RandomWalk(rand, 1_000_000));
            this.plt.AxisAuto();
            this.RunBenchmark("Signal (1M pts)");

            this.plt.Clear();
            this.plt.PlotSignalConst(ScottPlot.DataGen.RandomWalk(rand, 1_000_000));
            this.plt.AxisAuto();
            this.RunBenchmark("SignalConst (1M pts)");
        }
    }
}
