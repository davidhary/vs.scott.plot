﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.BuildProject(System.String)" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.CleanProject(System.String)" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.CopySource(System.String,System.String)" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.IncrimentVersion" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.MakeDemo" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.ProcessArguments(System.String[])" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.RunCommand(System.String)" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.ProjectFileVersion.#ctor(System.String)" )]
[assembly: SuppressMessage( "Style", "IDE0011:Add braces", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.ProjectFileVersion.Save" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.CopySource(System.String,System.String)" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.MakeCookbook" )]
[assembly: SuppressMessage( "Style", "IDE0058:Expression value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:ScottPlotDevTools.Program.MakeDemo" )]
