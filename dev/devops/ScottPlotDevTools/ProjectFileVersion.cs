﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ScottPlotDevTools
{
    public class ProjectFileVersion
    {
        public readonly string path;
        public Version version;
        public readonly string name;

        public ProjectFileVersion(string projectFilePath)
        {
            if (!projectFilePath.EndsWith(".csproj"))
                throw new ArgumentException("Project files must end with .csproj");

            projectFilePath = System.IO.Path.GetFullPath(projectFilePath);
            if (!System.IO.File.Exists(projectFilePath))
                throw new ArgumentException("Project file does not exist: " + projectFilePath);

            this.path = projectFilePath;
            this.name = System.IO.Path.GetFileNameWithoutExtension(projectFilePath);
            this.version = this.Read();
        }

        public override string ToString()
        {
            return $"{this.name} {this.version.Major}.{this.version.Minor}.{this.version.Build}";
        }

        private Version Read()
        {
            string[] sourceLines = System.IO.File.ReadAllLines( this.path );
            foreach (string line in sourceLines)
            {
                string[] parts = line.Split("Version>");
                if (parts.Length == 3)
                {
                    string versionString = parts[1].Split("<")[0];
                    //versionString += ".0";
                    return new Version(versionString);
                }
            }

            throw new ArgumentException("file does not contain version information: " + this.path );
        }

        public void Incriment()
        {
            this.version = new Version( this.version.Major, this.version.Minor, this.version.Build + 1);
        }

        public void Save()
        {
            Version fileVersion = this.Read();
            string fileVersionString = $"{fileVersion.Major}.{fileVersion.Minor}.{fileVersion.Build}";
            string newVersionShort = $"{this.version.Major}.{this.version.Minor}.{this.version.Build}";

            string[] lines = System.IO.File.ReadAllText( this.path ).Split("\n");
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Contains($">{fileVersionString}"))
                    lines[i] = line.Replace(fileVersionString, newVersionShort);
            }
            System.IO.File.WriteAllText( this.path, string.Join("\n", lines));
        }
    }
}
