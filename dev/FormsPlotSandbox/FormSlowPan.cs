﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsPlotSandbox
{
    public partial class FormSlowPan : Form
    {
        public FormSlowPan()
        {
            this.InitializeComponent();
            this.formsPlot1.plt.Grid(color: Color.Gray);
            this.checkBox2.Checked = this.formsPlot1.plt.GetSettings(false).HorizontalGridLines.SnapToNearestPixel;
            this.checkBox3.Checked = this.formsPlot1.plt.GetSettings(false).ticks.snapToNearestPixel;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.timer1.Enabled = this.checkBox1.Checked;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.formsPlot1.plt.AxisPan(.001, .001);
            this.formsPlot1.Render();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.plt.Grid(snapToNearestPixel: this.checkBox2.Checked);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.plt.Ticks(snapToNearestPixel: this.checkBox3.Checked);
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.plt.Ticks(rulerModeX: this.checkBox4.Checked);
            this.formsPlot1.plt.Ticks(rulerModeY: this.checkBox4.Checked);
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            this.formsPlot1.plt.AntiAlias(
                figure: this.checkBox5.Checked,
                data: this.checkBox5.Checked);
        }
    }
}
